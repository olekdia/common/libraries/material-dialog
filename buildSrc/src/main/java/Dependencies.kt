object Versions {
    const val kotlin = "2.0.20"
    const val junit = "4.13.2"
    const val robolectric = "4.13"
    const val mockk = "1.13.13"
    const val androidGradle = "8.6.0"

    object Olekdia {
        const val common = "0.6.1"
        const val commonAndroid = "3.7.4"
    }

    object Sdk {
        const val min = 21
        const val target = 35
        const val compile = 35
    }
    const val buildTools = "34.0.0"

    object Androidx {
        const val annotations = "1.9.1"
        const val appcompat = "1.7.0"
        const val material = "1.12.0"
        const val fragment = "1.8.5"
        const val swiperefreshlayout = "1.1.0"


        const val testCore = "1.6.1"
        const val testRunner = "1.6.1"
        const val testRules = "1.6.1"
        const val fragmentTesting = "1.8.5"
    }
}


object Libs {
    val junit = "junit:junit:${Versions.junit}"
    val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"
    val mockkJvm = "io.mockk:mockk:${Versions.mockk}"

    object kotlin {
        val reflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"
    }
    object olekdia {
        private val commonPrefix = "com.olekdia:multiplatform-common"
        val common = "$commonPrefix:${Versions.Olekdia.common}"
        val commonJvm = "$commonPrefix-jvm:${Versions.Olekdia.common}"
        val commonJs = "$commonPrefix-js:${Versions.Olekdia.common}"
        val commonNative = "$commonPrefix-native:${Versions.Olekdia.common}"
        val commonAndroid = "com.olekdia:android-common:${Versions.Olekdia.commonAndroid}"
    }

    object Androidx {
        val annotations = "androidx.annotation:annotation:${Versions.Androidx.annotations}"
        val appcompat = "androidx.appcompat:appcompat:${Versions.Androidx.appcompat}"
        val material = "com.google.android.material:material:${Versions.Androidx.material}"
        val fragment = "androidx.fragment:fragment:${Versions.Androidx.fragment}"
        val swiperefreshlayout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.Androidx.swiperefreshlayout}"


        val testCore = "androidx.test:core:${Versions.Androidx.testCore}"
        val testRunner = "androidx.test:runner:${Versions.Androidx.testRunner}"
        val testRules = "androidx.test:rules:${Versions.Androidx.testRules}"
        val fragmentTesting = "androidx.fragment:fragment-testing:${Versions.Androidx.fragmentTesting}"
    }

    object Plugin {
        val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
        val androidGradle = "com.android.tools.build:gradle:${Versions.androidGradle}"
    }
}
