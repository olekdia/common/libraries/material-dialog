package com.olekdia.materialdialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.common.extensions.withAlpha
import com.olekdia.materialdialog.R
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MaterialDialog_GridTest {

    private lateinit var context: Context
    private lateinit var itemsCallback: MaterialDialog.ItemsCallback
    private lateinit var itemsCallbackSingleChoice: MaterialDialog.ItemsCallbackSingleChoice
    private lateinit var itemsCallbackMultiChoice: MaterialDialog.ItemsCallbackMultiChoice

    private lateinit var dialog: MaterialDialog

    private lateinit var cities: Array<CharSequence>
    private lateinit var citiesIcons: Array<Drawable>

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context

        itemsCallback = mockk(relaxed = true, relaxUnitFun = true)
        every {
            itemsCallback.onSelection(any(), any(), any(), any())
        } answers {
            dialog.isItemEnabled(args[2] as Int)
        }

        itemsCallbackSingleChoice = mockk(relaxed = true, relaxUnitFun = true)
        every {
            itemsCallbackSingleChoice.onSelection(any(), any(), any(), any())
        } answers { dialog.isItemEnabled(args[2] as Int) }

        itemsCallbackMultiChoice = mockk(relaxed = true, relaxUnitFun = true)
        every {
            itemsCallbackMultiChoice.onSelection(any(), any(), any())
        } answers {
            (args[1] as IntArray).let { arr ->
                arr.all { dialog.isItemEnabled(it) }
            }
        }

        cities = arrayOf(
            "Nizhyn", "Kyiv", "New-York", "London", "Beijing",
            "Dubai", "Hong Kong", "Paris", "Shanghai", "Singapore",
            "Sydney", "Tokyo", "Los Angeles", "Boston", "Chicago",
            "San Francisco", "Houston", "Sydney", "San Jose", "Melbourne",
            "Seattle", "Atlanta", "Washington", "Toronto", "Dallas",
            "San Diego", "Baltimore", "Shenzhen", "Miami", "Austin",
            "Philadelphia", "Phoenix", "Munich", "Brisbane", "Moscow",
            "Seoul", "Milan", "San Jose", "Detroit", "Madrid",
            "Rome", "Qingdao", "Austin", "Pune", "Dayton",
            "Dhaka", "Frankfurt"
        )

        val city1: Drawable = ContextCompat.getDrawable(context, R.drawable.city_1)!!
        val city2: Drawable = ContextCompat.getDrawable(context, R.drawable.city_2)!!

        citiesIcons = Array(cities.size) { i -> if (i % 2 == 0) city1 else city2 }
    }

    private fun performGridItemClick(position: Int) {
        dialog.gridView!!.let { grid ->
            grid.performItemClick(
                grid.adapter.getView(position, null, grid),
                position,
                grid.getItemIdAtPosition(position)
            )
        }
    }

    private fun assertItemSelected(position: Int) {
        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertEquals(
                if (i == position) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer!!.background as ColorDrawable).color
            )
        }
    }

    private fun assertItemSelected(position1: Int, position2: Int) {
        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertEquals(
                if (i == position1 || i == position2) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer!!.background as ColorDrawable).color
            )
        }
    }

//--------------------------------------------------------------------------------------------------
//  Regular
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder regular, set items empty res - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(R.array.cities_empty)
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder regular, set items res - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(R.array.cities)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )
        }
    }

    @Test
    fun `builder regular, set items empty array - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(arrayOf<CharSequence>())
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder regular, set items array - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )
        }
    }

    @Test
    fun `builder regular, set items empty array, itemsIcons - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(arrayOf<CharSequence>())
            .itemsIcons(citiesIcons)
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder regular, set items array, itemsIcons - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities)
            .itemsIcons(citiesIcons)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is ImageView)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                citiesIcons[i],
                (itemContainer.getChildAt(0) as ImageView).drawable
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )
        }
    }

    @Test
    fun `builder regular, set items array, empty disablePositions, disableTail - all items look like enable`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(), "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(1.0F, itemContainer.alpha)
        }
    }

    @Test
    fun `builder regular, set items array, disablePositions, disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " disable tail" else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder regular, set items array, disablePositions, empty disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " " else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder regular, set items array, disablePositions, null disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, null)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    //todo gravity

    @Test
    fun `builder regular, set items array, disablePositions, itemsCallback, click enable item - callback triggered, dialog dismissed`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .itemsCallback(itemsCallback)
            .autoDismiss(true)
            .build()

        dialog.show()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        verify(exactly = 1) {
            itemsCallback.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }

        assertFalse(dialog.isShowing)
    }

    @Test
    fun `builder regular, set items array, disablePositions, itemsCallback, click disable item - callback triggered, dialog not dismissed`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .itemsCallback(itemsCallback)
            .autoDismiss(true)
            .build()

        dialog.show()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        verify(exactly = 1) {
            itemsCallback.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }

        assertTrue(dialog.isShowing)
    }

//--------------------------------------------------------------------------------------------------
//  Single choice
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder single choice, set items empty res - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(R.array.cities_empty)
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder single choice, set items res, widget color - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(R.array.cities)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder single choice, set items empty array - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(arrayOf<CharSequence>())
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder single choice, set items array, widget color - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder single choice, set items empty array, itemsIcons - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(arrayOf<CharSequence>())
            .itemsIcons(citiesIcons)
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder single choice, set items array, itemsIcons, widget color - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities)
            .itemsIcons(citiesIcons)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is ImageView)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                citiesIcons[i],
                (itemContainer.getChildAt(0) as ImageView).drawable
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder single choice, set items array, empty disablePositions, disableTail, widget color - all items look like enable`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(), "disable tail")
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(1.0F, itemContainer.alpha)
        }
    }

    @Test
    fun `builder single choice, set items array, disablePositions, disableTail, widget color - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "disable tail")
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " disable tail" else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder single choice, set items array, disablePositions, empty disableTail, widget color - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " " else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder single choice, set items array, disablePositions, null disableTail, widget color - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, null)
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    //todo gravity

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is false, click enable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is false, click disable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is true, click enable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .alwaysCallSingleChoiceCallback()
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is true, click disable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .alwaysCallSingleChoiceCallback()
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is true, widget color, click enable item - selection changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .alwaysCallSingleChoiceCallback()
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(1)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is true, widget color, click disable item - selection not changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .alwaysCallSingleChoiceCallback()
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertItemSelected(0)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, widget color, click enable item - selection changed, callback not triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(1)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, widget color, click disable item - selection not changed, callback not triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, widget color, click enable item, perform positive click - selection changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(1)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                null,
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, widget color, click disable item, perform positive click - selection not changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                null,
                0,
                "Nizhyn"
            )
        }
    }

//--------------------------------------------------------------------------------------------------
//  Multi choice
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder multi choice, set items empty res - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(R.array.cities_empty)
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder multi choice, set items res, widget color - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(R.array.cities)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder multi choice, set items empty array - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(arrayOf<CharSequence>())
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder multi choice, set items array, widget color - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder multi choice, set items empty array, itemsIcons - dialog without gridView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(arrayOf<CharSequence>())
            .itemsIcons(citiesIcons)
            .build()

        assertNull(dialog.gridView)
    }

    @Test
    fun `builder multi choice, set items array, itemsIcons, widget color - dialog with gridView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities)
            .itemsIcons(citiesIcons)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)
        assertEquals(cities.size, dialog.gridView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is ImageView)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                citiesIcons[i],
                (itemContainer.getChildAt(0) as ImageView).drawable
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder multi choice, set items array, empty disablePositions, disableTail, widget color - all items look like enable`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, intArrayOf(), "disable tail")
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(1.0F, itemContainer.alpha)
        }
    }

    @Test
    fun `builder multi choice, set items array, disablePositions, disableTail, widget color - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "disable tail")
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " disable tail" else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder multi choice, set items array, disablePositions, empty disableTail, widget color - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " " else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder multi choice, set items array, disablePositions, null disableTail, widget color - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, null)
            .widgetColor(Color.RED)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.gridView!!.adapter.getView(i, null, dialog.gridView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    //todo gravity

    @Test
    fun `builder multi choice, alwaysCallCallback is true, widget color, click enable, not checked item - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .alwaysCallMultiChoiceCallback()
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(0, 1)

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }
    }

    @Test
    fun `builder single choice, alwaysCallCallback is true, widget color, click disable item - selection not changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .alwaysCallMultiChoiceCallback()
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertItemSelected(0)

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 2),
                arrayOf("Nizhyn", "New-York")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, widget color, click enable, not checked item - selection changed, callback not triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(0, 1)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }
    }

    @Test
    fun `builder single choice, alwaysCallCallback is false, widget color, click disable item - selection not changed, callback not triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 2),
                arrayOf("Nizhyn", "New-York")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, widget color, click enable, not checked item, perform positive click - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .positiveText("Ok")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(0, 1)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }
    }

    @Test
    fun `builder single choice, alwaysCallCallback is false, widget color, click disable item, perform positive click - selection not changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .positiveText("Ok")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(2)

        assertItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 2),
                arrayOf("Nizhyn", "New-York")
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is true, widget color, click enable, checked item - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0, 1), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .alwaysCallMultiChoiceCallback()
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(0)

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, widget color, click enable, checked item - selection changed, callback not triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0, 1), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, widget color, click enable, checked item, perform positive click - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0, 1), itemsCallbackMultiChoice)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .items(cities, disablePositions, "")
            .positiveText("Ok")
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.gridView)

        performGridItemClick(1)

        assertItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }
}