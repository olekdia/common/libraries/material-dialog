package com.olekdia.materialdialog

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowLooper

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MaterialDialog_ProgressTest {

    private lateinit var context: Context

    private lateinit var dialog: MaterialDialog

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
    }

//--------------------------------------------------------------------------------------------------
//  Indeterminate
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder set progress style circular, widget color - isIndeterminate is true, dialog has correct progress bar, correct color`() {
        dialog = MaterialDialogBuilder(context)
            .progressIndeterminate(MaterialDialog.ProgressStyle.CIRCULAR)
            .widgetColor(Color.RED)
            .build()

        assertTrue(dialog.isIndeterminateProgress)
        assertTrue(dialog.progressBar!!.isIndeterminate)

        assertNotNull(dialog.progressBar)
        assertNull(dialog.progressLabel)
        assertNull(dialog.progressMinMaxLabel)

        assertEquals(Color.RED, dialog.progressBar!!.progressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.secondaryProgressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.indeterminateTintList!!.defaultColor)
    }

    @Test
    fun `builder set progress style horizontal, widget color - isIndeterminate is true, dialog has correct progress bar, correct color`() {
        dialog = MaterialDialogBuilder(context)
            .progressIndeterminate(MaterialDialog.ProgressStyle.HORIZONTAL)
            .widgetColor(Color.RED)
            .build()

        assertTrue(dialog.isIndeterminateProgress)
        assertTrue(dialog.progressBar!!.isIndeterminate)

        assertNotNull(dialog.progressBar)
        assertNull(dialog.progressLabel)
        assertNull(dialog.progressMinMaxLabel)

        assertEquals(Color.RED, dialog.progressBar!!.progressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.secondaryProgressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.indeterminateTintList!!.defaultColor)
    }

//--------------------------------------------------------------------------------------------------
//  Determinate
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder set progress style horizontal, widget color, progress max, showMinMax is false - isIndeterminate is false, dialog has correct progress bar, correct color`() {
        dialog = MaterialDialogBuilder(context)
            .progressIndeterminate(MaterialDialog.ProgressStyle.HORIZONTAL)
            .widgetColor(Color.RED)
            .progress(100, false)
            .build()

        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()

        assertFalse(dialog.isIndeterminateProgress)
        assertFalse(dialog.progressBar!!.isIndeterminate)

        assertNotNull(dialog.progressBar)
        assertNotNull(dialog.progressLabel)
        assertNotNull(dialog.progressMinMaxLabel)

        assertEquals("0%", dialog.progressLabel!!.text)
        assertEquals(View.GONE, dialog.progressMinMaxLabel!!.visibility)

        assertEquals(Color.RED, dialog.progressBar!!.progressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.secondaryProgressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.indeterminateTintList!!.defaultColor)
    }

    @Test
    fun `builder set progress style horizontal, showMinMax is false, set progress - progress label is correct`() {
        dialog = MaterialDialogBuilder(context)
            .progressIndeterminate(MaterialDialog.ProgressStyle.HORIZONTAL)
            .widgetColor(Color.RED)
            .progress(50, false)
            .build()

        dialog.setProgress(0)
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
        assertEquals("0%", dialog.progressLabel!!.text)

        dialog.setProgress(25)
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
        assertEquals("50%", dialog.progressLabel!!.text)

        dialog.setProgress(50)
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
        assertEquals("100%", dialog.progressLabel!!.text)
    }

    @Test
    fun `builder set progress style horizontal, widget color, progress max, showMinMax is true - isIndeterminate is false, dialog has correct progress bar, correct color`() {
        dialog = MaterialDialogBuilder(context)
            .progressIndeterminate(MaterialDialog.ProgressStyle.HORIZONTAL)
            .widgetColor(Color.RED)
            .progress(100, true)
            .build()

        assertFalse(dialog.isIndeterminateProgress)
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()

        assertNotNull(dialog.progressBar)
        assertNotNull(dialog.progressLabel)
        assertNotNull(dialog.progressMinMaxLabel)

        assertEquals("0%", dialog.progressLabel!!.text)
        assertEquals(View.VISIBLE, dialog.progressMinMaxLabel!!.visibility)
        assertEquals("0/100", dialog.progressMinMaxLabel!!.text)

        assertEquals(Color.RED, dialog.progressBar!!.progressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.secondaryProgressTintList!!.defaultColor)
        assertEquals(Color.RED, dialog.progressBar!!.indeterminateTintList!!.defaultColor)
    }

    @Test
    fun `builder set progress style horizontal, showMinMax is true, set progress - progress label is correct`() {
        dialog = MaterialDialogBuilder(context)
            .progressIndeterminate(MaterialDialog.ProgressStyle.HORIZONTAL)
            .widgetColor(Color.RED)
            .progress(50, true)
            .build()
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()

        dialog.setProgress(0)
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
        assertEquals("0%", dialog.progressLabel!!.text)
        assertEquals("0/50", dialog.progressMinMaxLabel!!.text)

        dialog.setProgress(25)
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
        assertEquals("50%", dialog.progressLabel!!.text)
        assertEquals("25/50", dialog.progressMinMaxLabel!!.text)

        dialog.setProgress(50)
        ShadowLooper.runUiThreadTasksIncludingDelayedTasks()
        assertEquals("100%", dialog.progressLabel!!.text)
        assertEquals("50/50", dialog.progressMinMaxLabel!!.text)
    }
}