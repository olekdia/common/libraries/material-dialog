package com.olekdia.materialdialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.Gravity
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.common.extensions.withAlpha
import com.olekdia.materialdialog.MaterialDialog.ItemsLayoutType
import com.olekdia.materialdialog.R
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MaterialDialog_ListTest {

    private lateinit var context: Context
    private lateinit var itemsCallback: MaterialDialog.ItemsCallback
    private lateinit var itemsCallbackSingleChoice: MaterialDialog.ItemsCallbackSingleChoice
    private lateinit var itemsCallbackMultiChoice: MaterialDialog.ItemsCallbackMultiChoice

    private lateinit var dialog: MaterialDialog

    private lateinit var cities: Array<CharSequence>
    private lateinit var citiesIcons: Array<Drawable>

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context

        itemsCallback = mockk(relaxed = true, relaxUnitFun = true)
        every {
            itemsCallback.onSelection(any(), any(), any(), any())
        } answers {
            dialog.isItemEnabled(args[2] as Int)
        }

        itemsCallbackSingleChoice = mockk(relaxed = true, relaxUnitFun = true)
        every {
            itemsCallbackSingleChoice.onSelection(any(), any(), any(), any())
        } answers { dialog.isItemEnabled(args[2] as Int) }

        itemsCallbackMultiChoice = mockk(relaxed = true, relaxUnitFun = true)
        every {
            itemsCallbackMultiChoice.onSelection(any(), any(), any())
        } answers {
            (args[1] as IntArray).let { arr ->
                arr.all { dialog.isItemEnabled(it) }
            }
        }

        cities = arrayOf(
            "Nizhyn", "Kyiv", "New-York", "London", "Beijing",
            "Dubai", "Hong Kong", "Paris", "Shanghai", "Singapore",
            "Sydney", "Tokyo", "Los Angeles", "Boston", "Chicago",
            "San Francisco", "Houston", "Sydney", "San Jose", "Melbourne",
            "Seattle", "Atlanta", "Washington", "Toronto", "Dallas",
            "San Diego", "Baltimore", "Shenzhen", "Miami", "Austin",
            "Philadelphia", "Phoenix", "Munich", "Brisbane", "Moscow",
            "Seoul", "Milan", "San Jose", "Detroit", "Madrid",
            "Rome", "Qingdao", "Austin", "Pune", "Dayton",
            "Dhaka", "Frankfurt"
        )

        val city1: Drawable = ContextCompat.getDrawable(context, R.drawable.city_1)!!
        val city2: Drawable = ContextCompat.getDrawable(context, R.drawable.city_2)!!

        citiesIcons = Array(cities.size) { i -> if (i % 2 == 0) city1 else city2 }
    }

    private fun performListItemClick(position: Int) {
        dialog.listView!!.let { list ->
            list.performItemClick(
                list.adapter.getView(position, null, list),
                position,
                list.getItemIdAtPosition(position)
            )
        }
    }

    private fun assertSingleItemSelected(position: Int) {
        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertEquals(
                i == position,
                (itemContainer!!.getChildAt(0) as RadioButton).isChecked
            )
        }
    }

    private fun assertMultiItemSelected(position: Int) {
        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertEquals(
                i == position,
                (itemContainer!!.getChildAt(0) as CheckBox).isChecked
            )
        }
    }

    private fun assertMultiItemSelected(position1: Int, position2: Int) {
        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertEquals(
                i == position1 || i == position2,
                (itemContainer!!.getChildAt(0) as CheckBox).isChecked
            )
        }
    }

//--------------------------------------------------------------------------------------------------
//  Regular
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder regular, set items empty res - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(R.array.cities_empty)
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder regular, set items res - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(R.array.cities)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )
        }
    }

    @Test
    fun `builder regular, set items empty array - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(arrayOf<CharSequence>())
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder regular, set items array - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )
        }
    }

    @Test
    fun `builder regular, set items empty array, itemsIcons - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(arrayOf<CharSequence>())
            .itemsIcons(citiesIcons)
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder regular, set items array, itemsIcons - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities)
            .itemsIcons(citiesIcons)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is ImageView)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                citiesIcons[i],
                (itemContainer.getChildAt(0) as ImageView).drawable
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )
        }
    }


    @Test
    fun `builder regular, set items array, empty disablePositions, disableTail - all items look like enable`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(), "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(1.0F, itemContainer.alpha)
        }
    }

    @Test
    fun `builder regular, set items array, disablePositions, disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " disable tail" else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder regular, set items array, disablePositions, empty disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i].toString() + if (i in disablePositions) " " else "",
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder regular, set items array, disablePositions, null disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, null)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(1, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is TextView)

            assertEquals(
                cities[i],
                (itemContainer.getChildAt(0) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder regular, set items array, items gravity end - items has proper gravity`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, null)
            .itemsGravity(MaterialDialog.DialogGravity.END)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer!!)
            assertNotEquals(
                Gravity.START, itemContainer.gravity and Gravity.START
            )
            assertEquals(
                Gravity.END, itemContainer.gravity and Gravity.END
            )
        }
    }

    @Test
    fun `builder regular, set items array, items gravity default - items has proper gravity`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, null)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer!!)
            assertEquals(
                Gravity.START, itemContainer.gravity and Gravity.START
            )
            assertNotEquals(
                Gravity.END, itemContainer.gravity and Gravity.END
            )
        }
    }

    @Test
    fun `builder regular, set items array, disablePositions, itemsCallback, click enable item - callback triggered, dialog dismissed`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .itemsCallback(itemsCallback)
            .autoDismiss(true)
            .build()

        dialog.show()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        verify(exactly = 1) {
            itemsCallback.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
        assertFalse(dialog.isShowing)
    }

    @Test
    fun `builder regular, set items array, disablePositions, itemsCallback, click disable item - callback triggered, dialog not dismissed`() {
        dialog = MaterialDialogBuilder(context)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .itemsCallback(itemsCallback)
            .autoDismiss(true)
            .build()

        dialog.show()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        verify(exactly = 1) {
            itemsCallback.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
        assertTrue(dialog.isShowing)
    }

//--------------------------------------------------------------------------------------------------
//  Single choice
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder single choice, set items empty res - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(R.array.cities_empty)
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder single choice, set items res - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(R.array.cities)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is RadioButton)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as RadioButton).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )
        }
    }

    @Test
    fun `builder single choice, set items empty array - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(arrayOf<CharSequence>())
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder single choice, set items array - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is RadioButton)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as RadioButton).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )
        }
    }

    @Test
    fun `builder single choice, set items empty array, itemsIcons - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(arrayOf<CharSequence>())
            .itemsIcons(citiesIcons)
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder single choice, set items array, itemsIcons, widget color - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities)
            .itemsIcons(citiesIcons)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is ImageView)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                citiesIcons[i],
                (itemContainer.getChildAt(0) as ImageView).drawable
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder single choice, set items array, empty disablePositions, disableTail - all items look like enable`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(), "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is RadioButton)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as RadioButton).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(1.0F, itemContainer.alpha)
        }
    }

    @Test
    fun `builder single choice, set items array, disablePositions, disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is RadioButton)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as RadioButton).isChecked
            )
            assertEquals(
                cities[i].toString() + if (i in disablePositions) " disable tail" else "",
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder single choice, set items array, disablePositions, empty disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is RadioButton)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as RadioButton).isChecked
            )
            assertEquals(
                cities[i].toString() + if (i in disablePositions) " " else "",
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder single choice, set items array, disablePositions, null disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, null)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is RadioButton)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as RadioButton).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder single choice, set items array, items gravity end - items has proper gravity`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, null)
            .itemsGravity(MaterialDialog.DialogGravity.END)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer!!)
            assertNotEquals(
                Gravity.START, itemContainer.gravity and Gravity.START
            )
            assertEquals(
                Gravity.END, itemContainer.gravity and Gravity.END
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is false, click enable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is false, click disable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is true, click enable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .alwaysCallSingleChoiceCallback()
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is null, alwaysCallCallback is true, click disable item - dialog not showing, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .alwaysCallSingleChoiceCallback()
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertFalse(dialog.isShowing)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is true, click enable item - selection changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .alwaysCallSingleChoiceCallback()
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertSingleItemSelected(1)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is true, click disable item - selection not changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .alwaysCallSingleChoiceCallback()
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertSingleItemSelected(0)

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, click enable item - selection changed, callback not triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertSingleItemSelected(1)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, click disable item - selection not changed, callback not triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertSingleItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(dialog, any(), any(), any())
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, click enable item, perform positive click - selection changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertSingleItemSelected(1)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                1,
                "Kyiv"
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                null,
                1,
                "Kyiv"
            )
        }
    }

    @Test
    fun `builder single choice, positiveText is not null, alwaysCallCallback is false, click disable item, perform positive click - selection not changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackSingleChoice(0, itemsCallbackSingleChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(2, 4, 6), "")
            .positiveText("Ok")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertSingleItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                any(),
                2,
                "New-York"
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackSingleChoice.onSelection(
                dialog,
                null,
                0,
                "Nizhyn"
            )
        }
    }

//--------------------------------------------------------------------------------------------------
//  Multi choice
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder multi choice, set items empty res - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(R.array.cities_empty)
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder multi choice, set items res - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(R.array.cities)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is CheckBox)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as CheckBox).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )
        }
    }

    @Test
    fun `builder multi choice, set items empty array - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(arrayOf<CharSequence>())
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder multi choice, set items array - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is CheckBox)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as CheckBox).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )
        }
    }

    @Test
    fun `builder multi choice, set items empty array, itemsIcons - dialog without listView`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(arrayOf<CharSequence>())
            .itemsIcons(citiesIcons)
            .build()

        assertNull(dialog.listView)
    }

    @Test
    fun `builder multi choice, set items array, itemsIcons, widget color - dialog with listView, contains correct items`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities)
            .itemsIcons(citiesIcons)
            .widgetColor(Color.RED)
            .build()

        assertNotNull(dialog.listView)
        assertEquals(cities.size, dialog.listView!!.adapter.count)

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is ImageView)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                citiesIcons[i],
                (itemContainer.getChildAt(0) as ImageView).drawable
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i == 0) {
                    Color.RED.withAlpha(MaterialDialog.SELECTED_COLOR_ALPHA)
                } else {
                    Color.TRANSPARENT
                },
                (itemContainer.background as ColorDrawable).color
            )
        }
    }

    @Test
    fun `builder multi choice, set items array, empty disablePositions, disableTail - all items look like enable`() {
        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, intArrayOf(), "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is CheckBox)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as CheckBox).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(1.0F, itemContainer.alpha)
        }
    }

    @Test
    fun `builder multi choice, set items array, disablePositions, disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "disable tail")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is CheckBox)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as CheckBox).isChecked
            )
            assertEquals(
                cities[i].toString() + if (i in disablePositions) " disable tail" else "",
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder multi choice, set items array, disablePositions, empty disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is CheckBox)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as CheckBox).isChecked
            )
            assertEquals(
                cities[i].toString() + if (i in disablePositions) " " else "",
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder multi choice, set items array, disablePositions, null disableTail - all items look correct`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, null)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer)
            assertEquals(2, itemContainer!!.childCount)
            assertTrue(itemContainer.getChildAt(0) is CheckBox)
            assertTrue(itemContainer.getChildAt(1) is TextView)

            assertEquals(
                i == 0,
                (itemContainer.getChildAt(0) as CheckBox).isChecked
            )
            assertEquals(
                cities[i],
                (itemContainer.getChildAt(1) as TextView).text
            )

            assertEquals(
                if (i in disablePositions) 0.8F else 1.0F,
                itemContainer.alpha
            )
        }
    }

    @Test
    fun `builder multi choice, set items array, items gravity end - items has proper gravity`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, null)
            .itemsGravity(MaterialDialog.DialogGravity.END)
            .build()

        for (i in cities.indices) {
            val itemContainer: LinearLayout? =
                dialog.listView!!.adapter.getView(i, null, dialog.listView) as? LinearLayout

            assertNotNull(itemContainer!!)
            assertNotEquals(
                Gravity.START, itemContainer.gravity and Gravity.START
            )
            assertEquals(
                Gravity.END, itemContainer.gravity and Gravity.END
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is true, click enable, not checked item - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .alwaysCallMultiChoiceCallback()
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertMultiItemSelected(0, 1)

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is true, click disable item - selection not changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .alwaysCallMultiChoiceCallback()
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertMultiItemSelected(0)

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 2),
                arrayOf("Nizhyn", "New-York")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, click enable, not checked item - selection changed, callback not triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertMultiItemSelected(0, 1)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, click disable item - selection not changed, callback not triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertMultiItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 2),
                arrayOf("Nizhyn", "New-York")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, click enable, not checked item, perform positive click - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .positiveText("Ok")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertMultiItemSelected(0, 1)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 1),
                arrayOf("Nizhyn", "Kyiv")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, click disable item, perform positive click - selection not changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .positiveText("Ok")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(2)

        assertMultiItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0, 2),
                arrayOf("Nizhyn", "New-York")
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is true, click enable, checked item - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0, 1), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .alwaysCallMultiChoiceCallback()
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertMultiItemSelected(0)

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, click enable, checked item - selection changed, callback not triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0, 1), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertMultiItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }

    @Test
    fun `builder multi choice, alwaysCallCallback is false, click enable, checked item, perform positive click - selection changed, callback triggered`() {
        val disablePositions: IntArray = intArrayOf(2, 4, 6)

        dialog = MaterialDialogBuilder(context)
            .itemsCallbackMultiChoice(intArrayOf(0, 1), itemsCallbackMultiChoice)
            .itemsLayoutType(ItemsLayoutType.LIST)
            .items(cities, disablePositions, "")
            .positiveText("Ok")
            .build()

        assertNotNull(dialog.listView)

        performListItemClick(1)

        assertMultiItemSelected(0)

        verify(exactly = 0) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }

        dialog.positiveButton!!.performClick()

        verify(exactly = 1) {
            itemsCallbackMultiChoice.onSelection(
                dialog,
                intArrayOf(0),
                arrayOf("Nizhyn")
            )
        }
    }
}