package com.olekdia.materialdialog

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ScrollView
import androidx.core.widget.NestedScrollView
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.materialdialog.R
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MaterialDialog_CustomViewTest {

    private lateinit var context: Context
    private lateinit var inflater: LayoutInflater

    private lateinit var dialog: MaterialDialog

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        inflater = LayoutInflater.from(context)
    }

//--------------------------------------------------------------------------------------------------
//  Custom view
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder set view res, wrapInScrollView is false - dialog has this view, without scroll view`() {
        dialog = MaterialDialogBuilder(context)
            .customView(R.layout.test_custom_view, false)
            .title("Title")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.customViewFrame)
        assertTrue(dialog.customViewFrame?.getChildAt(0) is LinearLayout)
        assertNotNull(dialog.customView)
        assertTrue(dialog.customView is LinearLayout)

        val container: LinearLayout = dialog.customView as LinearLayout

        assertEquals(2, container.childCount)
        assertTrue(container.getChildAt(0) is Button)
        assertTrue(container.getChildAt(1) is EditText)

        assertNotNull(container.findViewById(R.id.button))
        assertNotNull(container.findViewById(R.id.edit_text))
    }

    @Test
    fun `builder set view res, wrapInScrollView is true, bottomSheet is false - dialog has this view, with correct scroll view`() {
        dialog = MaterialDialogBuilder(context)
            .customView(R.layout.test_custom_view, true)
            .bottomSheet(false)
            .title("Title")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.customViewFrame)
        assertTrue(dialog.customViewFrame?.getChildAt(0) is ScrollView)
        assertNotNull(dialog.customView)
        assertTrue(dialog.customView is LinearLayout)

        val container: LinearLayout = dialog.customView as LinearLayout

        assertEquals(2, container.childCount)
        assertTrue(container.getChildAt(0) is Button)
        assertTrue(container.getChildAt(1) is EditText)

        assertNotNull(container.findViewById(R.id.button))
        assertNotNull(container.findViewById(R.id.edit_text))
    }

    @Test
    fun `builder set view res, wrapInScrollView is true, bottomSheet is true - dialog has this view, with correct scroll view`() {
        dialog = MaterialDialogBuilder(context)
            .customView(R.layout.test_custom_view, true)
            .bottomSheet(true)
            .title("Title")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.customViewFrame)
        assertTrue(dialog.customViewFrame?.getChildAt(0) is NestedScrollView)
        assertNotNull(dialog.customView)
        assertTrue(dialog.customView is LinearLayout)

        val container: LinearLayout = dialog.customView as LinearLayout

        assertEquals(2, container.childCount)
        assertTrue(container.getChildAt(0) is Button)
        assertTrue(container.getChildAt(1) is EditText)

        assertNotNull(container.findViewById(R.id.button))
        assertNotNull(container.findViewById(R.id.edit_text))
    }

    @Test
    fun `builder set view object, wrapInScrollView is false - dialog has this view, without scroll view`() {
        val view: View = inflater.inflate(R.layout.test_custom_view, null)

        dialog = MaterialDialogBuilder(context)
            .customView(view, false)
            .title("Title")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.customViewFrame)
        assertTrue(dialog.customViewFrame?.getChildAt(0) is LinearLayout)
        assertNotNull(dialog.customView)
        assertTrue(dialog.customView is LinearLayout)

        val container: LinearLayout = dialog.customView as LinearLayout

        assertEquals(2, container.childCount)
        assertTrue(container.getChildAt(0) is Button)
        assertTrue(container.getChildAt(1) is EditText)
    }

    @Test
    fun `builder set view object, wrapInScrollView is true, bottomSheet is false - dialog has this view, with correct scroll view`() {
        val view: View = inflater.inflate(R.layout.test_custom_view, null)

        dialog = MaterialDialogBuilder(context)
            .customView(view, true)
            .bottomSheet(false)
            .title("Title")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.customViewFrame)
        assertTrue(dialog.customViewFrame?.getChildAt(0) is ScrollView)
        assertNotNull(dialog.customView)
        assertTrue(dialog.customView is LinearLayout)

        val container: LinearLayout = dialog.customView as LinearLayout

        assertEquals(2, container.childCount)
        assertTrue(container.getChildAt(0) is Button)
        assertTrue(container.getChildAt(1) is EditText)
    }

    @Test
    fun `builder set view object, wrapInScrollView is true, bottomSheet is true - dialog has this view, with correct scroll view`() {
        val view: View = inflater.inflate(R.layout.test_custom_view, null)

        dialog = MaterialDialogBuilder(context)
            .customView(view, true)
            .bottomSheet(true)
            .title("Title")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.customViewFrame)
        assertTrue(dialog.customViewFrame?.getChildAt(0) is NestedScrollView)
        assertNotNull(dialog.customView)
        assertTrue(dialog.customView is LinearLayout)

        val container: LinearLayout = dialog.customView as LinearLayout

        assertEquals(2, container.childCount)
        assertTrue(container.getChildAt(0) is Button)
        assertTrue(container.getChildAt(1) is EditText)
    }

//--------------------------------------------------------------------------------------------------
//  Content
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder with customView - content view is null`() {
        dialog = MaterialDialogBuilder(context)
            .customView(R.layout.test_custom_view, false)
            .title("title")
            .build()

        assertNull(dialog.contentView)
    }

    @Test(expected = IllegalStateException::class)
    fun `builder with customView, set content text - content view is null, IllegalStateException`() {
        dialog = MaterialDialogBuilder(context)
            .customView(R.layout.test_custom_view, false)
            .content("Some content")
            .build()

        assertNull(dialog.contentView)
    }

    @Test(expected = IllegalStateException::class)
    fun `builder with customView, set content res - content view is null, IllegalStateException`() {
        dialog = MaterialDialogBuilder(context)
            .customView(R.layout.test_custom_view, false)
            .content(R.string.some_text)
            .build()

        assertNotNull(dialog.contentView)
    }

    @Test
    fun `dialog with customView, set content - nothing changed`() {
        dialog = MaterialDialogBuilder(context)
            .customView(R.layout.test_custom_view, false)
            .build()

        assertNull(dialog.contentView)

        dialog.setContent("Another text")

        assertNull(dialog.contentView)
    }
}