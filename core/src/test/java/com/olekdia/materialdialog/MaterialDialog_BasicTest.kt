package com.olekdia.materialdialog

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.View.MeasureSpec.EXACTLY
import android.view.View.MeasureSpec.makeMeasureSpec
import androidx.appcompat.view.ContextThemeWrapper
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.materialdialog.MaterialDialog.DialogAction
import com.olekdia.materialdialog.MaterialDialog.DialogGravity
import com.olekdia.materialdialog.R
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MaterialDialog_BasicTest {

    private lateinit var context: Context

    private lateinit var dialog: MaterialDialog

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
    }

//--------------------------------------------------------------------------------------------------
//  Title
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder do not set title - dialog do not has it`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .content("title")
            .build()

        assertNotNull(dialog.titleLabel)
        assertEquals(View.GONE, dialog.titleLabel!!.visibility)
        assertEquals(View.GONE, dialog.titleFrame!!.visibility)
    }

    @Test
    fun `builder set title res - dialog has this title`() {
        dialog = MaterialDialogBuilder(context)
            .title(R.string.some_title)
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.titleLabel)
        assertEquals(View.VISIBLE, dialog.titleLabel!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
        assertEquals(
            "Some title",
            dialog.titleLabel!!.text.toString()
        )
    }

    @Test
    fun `builder set title text - dialog has this title`() {
        dialog = MaterialDialogBuilder(context)
            .title("Material title")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.titleLabel)
        assertEquals(View.VISIBLE, dialog.titleLabel!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
        assertEquals(
            "Material title",
            dialog.titleLabel!!.text.toString()
        )
    }

    @Test
    fun `builder set title titleColorAttr - dialog has this title`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .title("Material title")
            .titleColorAttr(R.attr.some_color_1)
            .build()

        assertNotNull(dialog.titleLabel)
        assertEquals(View.VISIBLE, dialog.titleLabel!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
        assertEquals(
            Color.BLUE,
            dialog.titleLabel!!.currentTextColor
        )
    }

    @Test
    fun `builder set title gravity, title color, title typeface - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .title("Material title")
            .titleGravity(DialogGravity.END)
            .titleColor(Color.GREEN)
            .typeface(Typeface.MONOSPACE, Typeface.DEFAULT)
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.titleLabel)
        assertEquals(View.VISIBLE, dialog.titleLabel!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
        assertEquals(
            Color.GREEN,
            dialog.titleLabel!!.currentTextColor
        )
        assertNotEquals(
            Gravity.START, dialog.titleLabel!!.gravity and Gravity.START
        )
        assertEquals(
            Gravity.END, dialog.titleLabel!!.gravity and Gravity.END
        )
        assertEquals(
            Typeface.MONOSPACE,
            dialog.titleLabel!!.typeface
        )
    }

    @Test
    fun `dialog change title - title changed`() {
        dialog = MaterialDialogBuilder(context)
            .title("tit")
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.titleLabel)

        dialog.setTitle("Some title")
        assertEquals(View.VISIBLE, dialog.titleLabel!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
        assertEquals(
            "Some title",
            dialog.titleLabel!!.text.toString()
        )
    }

    @Test
    fun `dialog set title - title appeard`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText("Ok")
            .negativeText("Cancel")
            .build()

        assertNotNull(dialog.titleLabel)
        assertEquals(View.GONE, dialog.titleLabel!!.visibility)
        assertEquals(View.GONE, dialog.titleFrame!!.visibility)

        dialog.setTitle("Some title")
        assertEquals(View.VISIBLE, dialog.titleLabel!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
        assertEquals(
            "Some title",
            dialog.titleLabel!!.text.toString()
        )
    }

//--------------------------------------------------------------------------------------------------
//  Icon
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder do not set icon - dialog do not has it`() {
        dialog = MaterialDialogBuilder(context)
            .title("title")
            .build()

        assertNotNull(dialog.titleIcon)
        assertEquals(View.GONE, dialog.titleIcon!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
    }

    @Test
    fun `builder set icon res - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .iconRes(R.drawable.icb_accept)
            .build()

        assertNotNull(dialog.titleIcon)
        assertEquals(View.VISIBLE, dialog.titleIcon!!.visibility)
        assertEquals(View.GONE, dialog.titleFrame!!.visibility)
        assertEquals(
            (context.getDrawable(R.drawable.icb_accept) as BitmapDrawable).bitmap,
            (dialog.titleIcon!!.drawable as BitmapDrawable).bitmap
        )
    }

    @Test
    fun `builder set icon drawable - dialog has it`() {
        val drw = context.getDrawable(R.drawable.icb_accept)
        dialog = MaterialDialogBuilder(context)
            .icon(drw)
            .build()

        assertNotNull(dialog.titleIcon)
        assertEquals(View.VISIBLE, dialog.titleIcon!!.visibility)
        assertEquals(View.GONE, dialog.titleFrame!!.visibility)
        assertEquals(
            drw,
            dialog.titleIcon!!.drawable
        )
    }

    @Test
    fun `builder set icon attr - dialog has it`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .title("tit")
            .iconAttr(R.attr.some_icon)
            .build()

        assertNotNull(dialog.titleIcon)
        assertEquals(View.VISIBLE, dialog.titleIcon!!.visibility)
        assertEquals(View.VISIBLE, dialog.titleFrame!!.visibility)
        assertEquals(
            (context.getDrawable(R.drawable.icb_accept) as BitmapDrawable).bitmap,
            (dialog.titleIcon!!.drawable as BitmapDrawable).bitmap
        )
    }

    @Test
    fun `dialog set icon - icon changed`() {
        dialog = MaterialDialogBuilder(context)
            .build()

        assertNotNull(dialog.titleIcon)
        assertEquals(View.GONE, dialog.titleIcon!!.visibility)

        dialog.setIcon(R.drawable.icb_accept)
        assertEquals(View.VISIBLE, dialog.titleIcon!!.visibility)
        assertEquals(
            (context.getDrawable(R.drawable.icb_accept) as BitmapDrawable).bitmap,
            (dialog.titleIcon!!.drawable as BitmapDrawable).bitmap
        )
    }

//--------------------------------------------------------------------------------------------------
//  Content
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder do not set content - dialog do not has it`() {
        dialog = MaterialDialogBuilder(context)
            .title("title")
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.GONE, dialog.contentView!!.visibility)
    }

    @Test
    fun `builder set content text - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .content("Some content")
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            "Some content",
            dialog.contentView!!.text.toString()
        )
    }

    @Test
    fun `builder set content res - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .content(R.string.some_text)
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            "Some text",
            dialog.contentView!!.text.toString()
        )
    }

    @Test
    fun `builder set content res with args - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .content(R.string.some_text_with_number, 5)
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            "Some text 5",
            dialog.contentView!!.text.toString()
        )
    }

    @Test
    fun `builder set content color - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .content(R.string.some_text)
            .contentColor(Color.BLUE)
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            Color.BLUE,
            dialog.contentView!!.currentTextColor
        )
    }

    @Test
    fun `builder set content color res - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .content(R.string.some_text)
            .contentColorRes(R.color.yellow_color)
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            Color.YELLOW,
            dialog.contentView!!.currentTextColor
        )
    }

    @Test
    fun `builder set content color attr - dialog has it`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .content(R.string.some_text)
            .contentColorAttr(R.attr.some_color_1)
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            Color.BLUE,
            dialog.contentView!!.currentTextColor
        )
    }

    @Test
    fun `builder set content gravity, content typeface, content line spacing - dialog has it`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .content(R.string.some_text)
            .contentGravity(DialogGravity.END)
            .typeface(Typeface.DEFAULT, Typeface.MONOSPACE)
            .contentLineSpacing(1.67f)
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            Typeface.MONOSPACE,
            dialog.contentView!!.typeface
        )
        assertNotEquals(
            Gravity.START, dialog.contentView!!.gravity and Gravity.START
        )
        assertEquals(
            Gravity.END, dialog.contentView!!.gravity and Gravity.END
        )
        assertEquals(
            1.67f,
            dialog.contentView!!.lineSpacingMultiplier
        )
    }

    @Test
    fun `dialog set content - content changed`() {
        dialog = MaterialDialogBuilder(context)
            .build()

        assertNotNull(dialog.contentView)
        assertEquals(View.GONE, dialog.contentView!!.visibility)
        dialog.setContent("Another text")

        assertEquals(View.VISIBLE, dialog.contentView!!.visibility)
        assertEquals(
            "Another text",
            dialog.contentView!!.text.toString()
        )
    }

//--------------------------------------------------------------------------------------------------
//  Buttons
//--------------------------------------------------------------------------------------------------

    @Test
    fun `dialog has buttons, getActionButton returns correct values`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .neutralText("Just Neutral")
            .build()

        assertNotNull(dialog.positiveButton)
        assertNotNull(dialog.negativeButton)
        assertNotNull(dialog.neutralButton)
        assertNotNull(dialog.getActionButton(DialogAction.POSITIVE))
        assertNotNull(dialog.getActionButton(DialogAction.NEGATIVE))
        assertNotNull(dialog.getActionButton(DialogAction.NEUTRAL))

        assertEquals(dialog.positiveButton, dialog.getActionButton(DialogAction.POSITIVE))
        assertEquals(dialog.negativeButton, dialog.getActionButton(DialogAction.NEGATIVE))
        assertEquals(dialog.neutralButton, dialog.getActionButton(DialogAction.NEUTRAL))
    }

    @Test
    fun `builder set button text - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .neutralText("Just Neutral")
            .build()

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.negativeButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.neutralButton!!.visibility)

        assertEquals(
            "Just Ok",
            dialog.positiveButton!!.text.toString()
        )
        assertEquals(
            "Just Cancel",
            dialog.negativeButton!!.text.toString()
        )
        assertEquals(
            "Just Neutral",
            dialog.neutralButton!!.text.toString()
        )
    }

    @Test
    fun `builder set button text res - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText(R.string.some_text)
            .negativeText(R.string.some_text)
            .neutralText(R.string.some_title)
            .build()

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.negativeButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.neutralButton!!.visibility)

        assertEquals(
            "Some text",
            dialog.positiveButton!!.text.toString()
        )
        assertEquals(
            "Some text",
            dialog.negativeButton!!.text.toString()
        )
        assertEquals(
            "Some title",
            dialog.neutralButton!!.text.toString()
        )
    }

    @Test
    fun `builder do not set button text - dialog do not has this visible buttons`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText("Just Ok")
            .build()

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.GONE, dialog.negativeButton!!.visibility)
        assertEquals(View.GONE, dialog.neutralButton!!.visibility)

        assertEquals(
            "Just Ok",
            dialog.positiveButton!!.text.toString()
        )
        assertEquals(
            "",
            dialog.negativeButton!!.text.toString()
        )
        assertEquals(
            "",
            dialog.neutralButton!!.text.toString()
        )
    }

    @Test
    fun `builder set button color - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText("Just Ok")
            .positiveColor(Color.RED)
            .negativeText("Just Cancel")
            .negativeColor(Color.GREEN)
            .neutralText("Just Neutral")
            .neutralColor(Color.BLUE)
            .build()

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.negativeButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.neutralButton!!.visibility)

        assertEquals(
            Color.RED,
            dialog.positiveButton!!.currentTextColor
        )
        assertEquals(
            Color.GREEN,
            dialog.negativeButton!!.currentTextColor
        )
        assertEquals(
            Color.BLUE,
            dialog.neutralButton!!.currentTextColor
        )
    }

    @Test
    fun `builder set button color res - dialog has it`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText("Just Ok")
            .positiveColorRes(R.color.yellow_color)
            .negativeText("Just Cancel")
            .negativeColorRes(R.color.cyan_color)
            .neutralText("Just Neutral")
            .neutralColorRes(R.color.magenta_color)
            .build()

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.negativeButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.neutralButton!!.visibility)

        assertEquals(
            Color.YELLOW,
            dialog.positiveButton!!.currentTextColor
        )
        assertEquals(
            Color.CYAN,
            dialog.negativeButton!!.currentTextColor
        )
        assertEquals(
            Color.MAGENTA,
            dialog.neutralButton!!.currentTextColor
        )
    }

    @Test
    fun `builder set button color attr - dialog has it`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .positiveColorAttr(R.attr.some_color_1)
            .negativeText("Just Cancel")
            .negativeColorAttr(R.attr.some_color_2)
            .neutralText("Just Neutral")
            .neutralColorAttr(R.attr.some_color_3)
            .build()

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.negativeButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.neutralButton!!.visibility)

        assertEquals(
            Color.BLUE,
            dialog.positiveButton!!.currentTextColor
        )
        assertEquals(
            Color.YELLOW,
            dialog.negativeButton!!.currentTextColor
        )
        assertEquals(
            Color.CYAN,
            dialog.neutralButton!!.currentTextColor
        )
    }

    @Test
    fun `dialog numberOfActionButtons, hasActionButtons, setActionButton - works correctly`() {
        dialog = MaterialDialogBuilder(context)
            .positiveText("OK")
            .neutralText("INFO")
            .build()

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.GONE, dialog.negativeButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.neutralButton!!.visibility)

        assertEquals(2, dialog.numberOfActionButtons())
        assertTrue(dialog.hasActionButtons())

        dialog.setActionButton(DialogAction.NEGATIVE, "Cancel")

        assertEquals(3, dialog.numberOfActionButtons())
        assertTrue(dialog.hasActionButtons())

        dialog.setActionButton(DialogAction.POSITIVE, null)
        dialog.setActionButton(DialogAction.NEGATIVE, null)
        dialog.setActionButton(DialogAction.NEUTRAL, null)
        assertEquals(0, dialog.numberOfActionButtons())
        assertFalse(dialog.hasActionButtons())

        dialog.setActionButton(DialogAction.NEUTRAL, R.string.some_title)
        assertEquals(1, dialog.numberOfActionButtons())
    }

    @Test
    fun `builder do not set forceStacking - buttons not stacked`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .forceStacking(false)
            .build()

        dialog._view!!
            .measure(makeMeasureSpec(200, EXACTLY), makeMeasureSpec(200, EXACTLY));

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.negativeButton!!.visibility)
        assertEquals(View.GONE, dialog.neutralButton!!.visibility)

        dialog.positiveButton!!.let { button ->
            assertFalse(
                MdButton::class.memberProperties
                    .firstOrNull { it.name == "stacked" }
                    ?.apply {
                        isAccessible = true
                    }?.get(button) as Boolean
            )
        }
        dialog.negativeButton!!.let { button ->
            assertFalse(
                MdButton::class.memberProperties
                    .firstOrNull { it.name == "stacked" }
                    ?.apply {
                        isAccessible = true
                    }?.get(button) as Boolean
            )
        }
    }

    @Test
    fun `builder set forceStacking - buttons stacked`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .forceStacking(true)
            .build()

        dialog._view!!
            .measure(makeMeasureSpec(200, EXACTLY), makeMeasureSpec(200, EXACTLY));

        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertEquals(View.VISIBLE, dialog.negativeButton!!.visibility)
        assertEquals(View.GONE, dialog.neutralButton!!.visibility)

        dialog.positiveButton!!.let { button ->
            assertTrue(
                MdButton::class.memberProperties
                    .firstOrNull { it.name == "stacked" }
                    ?.apply {
                        isAccessible = true
                    }?.get(button) as Boolean
            )
        }
        dialog.negativeButton!!.let { button ->
            assertTrue(
                MdButton::class.memberProperties
                    .firstOrNull { it.name == "stacked" }
                    ?.apply {
                        isAccessible = true
                    }?.get(button) as Boolean
            )
        }
    }

//--------------------------------------------------------------------------------------------------
//  Listeners
//--------------------------------------------------------------------------------------------------

    @Test
    fun `dialog dismiss listener - called once`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)

        var counter = 0
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .setDismissListener {
                counter++
            }
            .build()

        dialog.dismiss()
        assertEquals(1, counter)
    }

    @Test
    fun `dialog cancel listener - called once`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)

        var counter = 0
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .setCancelListener() {
                counter++
            }
            .build()

        dialog.cancel()
        assertEquals(1, counter)
    }

    @Test
    fun `dialog dismiss, cancel listeners, dismiss action - called once`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)

        var counter = 0
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .setCancelListener() {
                counter++
                counter++ // Not triggered by design
            }
            .setDismissListener {
                counter++
            }
            .build()

        dialog.dismiss()
        assertEquals(1, counter)
    }

    @Test
    fun `dialog dismiss, cancel listeners, cancel action - called once`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)

        var counter = 0
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .setCancelListener() {
                counter++
            }
            .setDismissListener {
                counter++
            }
            .build()

        dialog.cancel()
        assertEquals(2, counter)
    }
}