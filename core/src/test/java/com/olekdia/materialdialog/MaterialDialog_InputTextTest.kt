package com.olekdia.materialdialog

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.text.InputType
import android.view.View
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.androidcommon.extensions.getColorCompat
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MaterialDialog_InputTextTest {

    private lateinit var context: Context
    private lateinit var inputCallback: MaterialDialog.InputCallback

    private lateinit var dialog: MaterialDialog

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        inputCallback = mockk(relaxed = true, relaxUnitFun = true)
    }

//--------------------------------------------------------------------------------------------------
//  Input
//--------------------------------------------------------------------------------------------------

    @Test
    fun `builder set input params - dialog input params is correct`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "prefill", false, inputCallback)
            .build()

        assertNotNull(dialog.inputEditText)
        assertEquals(View.VISIBLE, dialog.inputEditText!!.visibility)

        assertEquals("hint", dialog.inputEditText!!.hint)
        assertEquals("prefill", dialog.inputEditText!!.text.toString())
    }

    @Test
    fun `builder allowEmptyInput is false, input is not empty - positive button is enable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "prefill", false, inputCallback)
            .build()

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertTrue(dialog.positiveButton!!.isEnabled)
    }

    @Test
    fun `builder allowEmptyInput is false, input is empty - positive button is disable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "", false, inputCallback)
            .build()

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertFalse(dialog.positiveButton!!.isEnabled)
    }

    @Test
    fun `builder allowEmptyInput is true, input is not empty - positive button is enable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "prefill", true, inputCallback)
            .build()

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertTrue(dialog.positiveButton!!.isEnabled)
    }

    @Test
    fun `builder allowEmptyInput is true, input is empty - positive button is enable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "", true, inputCallback)
            .build()

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertTrue(dialog.positiveButton!!.isEnabled)
    }

    @Test
    fun `builder alwaysCallInputCallback is false, perform positive click - callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "prefill", true, inputCallback)
            .build()

        assertNotNull(dialog.inputEditText)
        assertEquals(View.VISIBLE, dialog.inputEditText!!.visibility)

        dialog.positiveButton!!.performClick()

        assertEquals("prefill", dialog.inputEditText!!.text.toString())
        verify(exactly = 1) { inputCallback.onInput(dialog, dialog.inputEditText!!.text) }
    }

    @Test
    fun `builder alwaysCallInputCallback is true, perform positive click - callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "prefill", true, inputCallback)
            .alwaysCallInputCallback()
            .build()

        assertNotNull(dialog.inputEditText)
        assertEquals(View.VISIBLE, dialog.inputEditText!!.visibility)

        dialog.positiveButton!!.performClick()

        assertEquals("prefill", dialog.inputEditText!!.text.toString())
        verify(exactly = 1) { inputCallback.onInput(dialog, dialog.inputEditText!!.text) }
    }

    @Test
    fun `builder alwaysCallInputCallback is false, set text - text changed, callback not triggered`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "", true, inputCallback)
            .build()

        assertNotNull(dialog.inputEditText)
        assertEquals(View.VISIBLE, dialog.inputEditText!!.visibility)

        dialog.inputEditText!!.setText("new text")

        assertEquals("new text", dialog.inputEditText!!.text.toString())
        verify(exactly = 0) { inputCallback.onInput(dialog, dialog.inputEditText!!.text) }
    }

    @Test
    fun `builder alwaysCallInputCallback is true, set text - text changed, callback triggered`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "", true, inputCallback)
            .alwaysCallInputCallback()
            .build()

        assertNotNull(dialog.inputEditText)
        assertEquals(View.VISIBLE, dialog.inputEditText!!.visibility)

        dialog.inputEditText!!.setText("new text")

        assertEquals("new text", dialog.inputEditText!!.text.toString())
        verify(exactly = 1) { inputCallback.onInput(dialog, dialog.inputEditText!!.text) }
    }

    @Test
    fun `builder set inputMaxLength, valid length - default text color, positive button is enable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "55555", false, inputCallback)
            .contentColor(Color.RED)
            .inputMaxLength(5)
            .build()

        assertNotNull(dialog.inputMinMaxLabel)
        assertEquals(View.VISIBLE, dialog.inputMinMaxLabel!!.visibility)
        assertEquals(Color.RED, dialog.inputMinMaxLabel!!.currentTextColor)

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertTrue(dialog.positiveButton!!.isEnabled)
    }

    @Test
    fun `builder set inputMaxLength, invalid length - default error text color, positive button is disable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "666666", false, inputCallback)
            .contentColor(Color.RED)
            .inputMaxLength(5)
            .build()

        assertNotNull(dialog.inputMinMaxLabel)
        assertEquals(View.VISIBLE, dialog.inputMinMaxLabel!!.visibility)
        assertEquals(context.getColorCompat(R.color.md_edittext_error), dialog.inputMinMaxLabel!!.currentTextColor)

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertFalse(dialog.positiveButton!!.isEnabled)
    }

    @Test
    fun `builder set inputMaxLength and errorColor, valid length - default text color, positive button is enable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "55555", false, inputCallback)
            .contentColor(Color.RED)
            .inputMaxLength(5, Color.BLACK)
            .build()

        assertNotNull(dialog.inputMinMaxLabel)
        assertEquals(View.VISIBLE, dialog.inputMinMaxLabel!!.visibility)
        assertEquals(Color.RED, dialog.inputMinMaxLabel!!.currentTextColor)

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertTrue(dialog.positiveButton!!.isEnabled)
    }

    @Test
    fun `builder set inputMaxLength and errorColor, invalid length - correct text color, positive button is disable`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "666666", false, inputCallback)
            .contentColor(Color.RED)
            .inputMaxLength(5, Color.BLACK)
            .build()

        assertNotNull(dialog.inputMinMaxLabel)
        assertEquals(View.VISIBLE, dialog.inputMinMaxLabel!!.visibility)
        assertEquals(Color.BLACK, dialog.inputMinMaxLabel!!.currentTextColor)

        assertNotNull(dialog.positiveButton)
        assertEquals(View.VISIBLE, dialog.positiveButton!!.visibility)
        assertFalse(dialog.positiveButton!!.isEnabled)

        assertEquals(Color.RED, Color.parseColor("#FFFF0000"))
    }

    @Test
    fun `builder set input type - dialog input type is correct`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "prefill", false, inputCallback)
            .inputType(InputType.TYPE_CLASS_PHONE)
            .build()

        assertNotNull(dialog.inputEditText)
        assertEquals(InputType.TYPE_CLASS_PHONE, dialog.inputEditText!!.inputType)
    }

    @Test
    fun `builder set input type, change input type - dialog input type is correct`() {
        dialog = MaterialDialogBuilder(context)
            .input("hint", "prefill", false, inputCallback)
            .inputType(InputType.TYPE_CLASS_PHONE)
            .build()

        assertNotNull(dialog.inputEditText)
        assertEquals(InputType.TYPE_CLASS_PHONE, dialog.inputEditText!!.inputType)

        dialog.inputEditText!!.inputType = InputType.TYPE_CLASS_DATETIME

        assertEquals(InputType.TYPE_CLASS_DATETIME, dialog.inputEditText!!.inputType)
    }
}