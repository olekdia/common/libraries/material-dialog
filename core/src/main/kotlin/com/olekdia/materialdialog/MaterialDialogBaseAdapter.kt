package com.olekdia.materialdialog

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.annotation.LayoutRes
import com.olekdia.common.extensions.toLocalNumerals
import com.olekdia.common.extensions.withAlpha
import com.olekdia.materialdialog.MaterialDialog.Companion.SELECTED_COLOR_ALPHA

/**
 * @author Oleksandr Albul
 */
abstract class MaterialDialogBaseAdapter(
    @JvmField
    protected val dialog: MaterialDialog,
    @JvmField
    @LayoutRes
    protected val layoutRes: Int
) : BaseAdapter() {
    @JvmField
    protected val builder: MaterialDialogBuilder = dialog.builder
    @JvmField
    protected val inflater: LayoutInflater = LayoutInflater.from(dialog.context)
    @JvmField
    protected val selectedColor: Int = builder.widgetColor.withAlpha(SELECTED_COLOR_ALPHA)

    override fun hasStableIds(): Boolean = true

    override fun getCount(): Int = builder.items?.size ?: 0

    override fun getItem(position: Int): CharSequence? = builder.items
        ?.getOrNull(position)
        ?.toLocalNumerals(builder.numeralSystem)

    override fun getItemId(position: Int): Long = position.toLong()

    protected fun isItemEnabled(position: Int): Boolean = dialog.isItemEnabled(position)

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup?
    ): View = (convertView ?: newView(parent))
        .also { v ->
            bindView(v, position)
        }

    protected abstract fun newView(parent: ViewGroup?): View

    protected abstract fun bindView(view: View, index: Int)
}