package com.olekdia.materialdialog

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.view.Gravity
import android.view.PointerIcon
import android.view.View
import com.google.android.material.textview.MaterialTextView
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.backgroundCompat
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.materialdialog.MaterialDialog.DialogGravity

/**
 * @author Kevin Barry (teslacoil) 4/02/2015
 */
class MdButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : MaterialTextView(context, attrs, defStyleAttr) {

    private var stacked = false
    @DialogGravity
    private var stackedGravity = DialogGravity.END
    private var stackedEndPadding = context.getDimensionPixelSize(R.dimen.md_dialog_frame_margin)
    private var stackedBackground: Drawable? = null
    private var defaultBackground: Drawable? = null

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            pointerIcon = PointerIcon.getSystemIcon(context, PointerIcon.TYPE_HAND)
        }
    }

    /**
     * Set if the button should be displayed in stacked mode.
     * This should only be called from MDRootLayout's onMeasure, and we must be measured
     * after calling this.
     */
    fun setStacked(stacked: Boolean, force: Boolean) {
        if (this.stacked != stacked
            || force
        ) {
            gravity = if (stacked) {
                Gravity.CENTER_VERTICAL or stackedGravity.fromDialogGravityToGravity()
            } else {
                Gravity.CENTER
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                textAlignment = if (stacked) {
                    stackedGravity.fromDialogGravityToTextAlignment()
                } else {
                    View.TEXT_ALIGNMENT_CENTER
                }
            }
            this.backgroundCompat = if (stacked) stackedBackground else defaultBackground
            if (stacked) {
                setPadding(
                    stackedEndPadding,
                    paddingTop,
                    stackedEndPadding,
                    paddingBottom
                )
            } /* Else the padding was properly reset by the drawable */
            this.stacked = stacked
        }
    }

    fun setStackedGravity(@DialogGravity gravity: Int) {
        stackedGravity = gravity
    }

    fun setStackedSelector(d: Drawable?) {
        stackedBackground = d
        if (stacked) setStacked(stacked = true, force = true)
    }

    fun setDefaultSelector(d: Drawable?) {
        defaultBackground = d
        if (!stacked) setStacked(stacked = false, force = true)
    }
}