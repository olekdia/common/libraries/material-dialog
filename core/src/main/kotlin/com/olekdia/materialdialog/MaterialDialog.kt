package com.olekdia.materialdialog

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.WindowManager.BadTokenException
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.annotation.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.*
import com.olekdia.common.INVALID
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.common.extensions.ifNotNullAnd
import com.olekdia.common.extensions.toLocalNumerals
import java.text.NumberFormat
import java.util.*

@SuppressLint("InflateParams")
open class MaterialDialog(val builder: MaterialDialogBuilder) :
    DialogBase(builder.context, builder.getThemeRes()),
    View.OnClickListener,
    OnItemClickListener {

    var listView: ListView? = null
        internal set
    var gridView: GridView? = null
        internal set
    var titleIcon: ImageView? = null
        internal set
    /**
     * Retrieves the TextView that contains the dialog title. If you want to update the
     * title, use #[.setTitle] instead.
     */
    var titleLabel: TextView? = null
        internal set
    @JvmField
    internal var titleFrame: View? = null
    @JvmField
    internal var customViewFrame: FrameLayout? = null
    var progressBar: ProgressBar? = null
        internal set
    var progressLabel: TextView? = null
        internal set
    var progressMinMaxLabel: TextView? = null
        internal set

    private var _handler: Handler? = null
    /**
     * Retrieves the TextView that contains the dialog content. If you want to update the
     * content (message), use #[.setContent] instead.
     */
    var contentView: TextView? = null
        internal set
    var inputEditText: EditText? = null
        internal set
    @JvmField
    internal var inputMinMaxLabel: TextView? = null
    @JvmField
    internal var bottomSheetBehavior: BottomSheetBehavior<FrameLayout>? = null
    @JvmField
    internal var bottomSheetContainer: FrameLayout? = null
    @JvmField
    internal var positiveButton: MdButton? = null
    @JvmField
    internal var neutralButton: MdButton? = null
    @JvmField
    internal var negativeButton: MdButton? = null
    @JvmField
    @ItemsChoiceMode
    internal var itemsChoiceMode = ItemsChoiceMode.REGULAR
    @JvmField
    internal var selectedIndicesList: MutableList<Int>? = null

    init {
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        _view = LayoutInflater.from(builder.context)
            .inflate(builder.getRootLayoutRes(), null) as MdRootLayout

        DialogConstructor(this, _view!!).construct()

        window?.let { w ->
            val isWideScreen: Boolean = builder.context
                .resources
                .getBoolean(R.bool.md_is_wide_screen)
            if (builder.bottomSheet) {
                w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                w.statusBarColor = context.resolveColor(R.attr.md_bottom_sheet_status_bar_color, 0)
                w.navigationBarColor = context.resolveColor(R.attr.md_bottom_sheet_navigation_bar_color, 0)
                w.setLayout(
                    if (isWideScreen) {
                        builder.context
                            .resources
                            .getDimensionPixelSize(R.dimen.md_default_bottom_dialog_width)
                    } else {
                        ViewGroup.LayoutParams.MATCH_PARENT
                    },
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            } else if (isWideScreen) {
                w.setLayout(
                    builder.context
                        .resources
                        .getDimensionPixelSize(R.dimen.md_default_dialog_width),
                    w.attributes.height
                )
            }
        }
    }

//--------------------------------------------------------------------------------------------------
//  Dialog methods
//--------------------------------------------------------------------------------------------------

    @UiThread
    override fun show() {
        try {
            super.show()
        } catch (e: BadTokenException) {
            throw DialogException("Bad window token, you cannot show a dialog before an Activity is created or after it's hidden.")
        }
    }

    override fun cancel() {
        builder.cancelListener?.onCancel(this)
        val behavior = bottomSheetBehavior
        if (behavior == null
            || behavior.state == BottomSheetBehavior.STATE_HIDDEN
        ) {
            super.cancel()
        } else {
            behavior.setState(BottomSheetBehavior.STATE_HIDDEN)
        }
    }

    override fun dismiss() {
        builder.dismissListener?.onDismiss(this)
        super.dismiss()
    }

    override fun setCancelable(cancelable: Boolean) {
        super.setCancelable(cancelable)
        if (builder.cancelable != cancelable) {
            builder.cancelable = cancelable
            bottomSheetBehavior?.isHideable = cancelable
        }
    }

    val isCancelled: Boolean
        get() = !isShowing

    override fun onStart() {
        super.onStart()
        bottomSheetBehavior?.let { behavior ->
            if (behavior.state == BottomSheetBehavior.STATE_HIDDEN) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            } else if (context.resources.isLandOrientation
                || builder.bottomSheetForceExpand
            ) {
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
    }

    override fun onStop() {
        builder.stopListener?.onStop(this)
        super.onStop()

        (inputEditText ?: window?.currentFocus)?.let {
            it.post {
                //it.clearFocus()
                context.hideKeyboard(it)
            }
        }
    }

    override fun onShow(dialog: DialogInterface) {
        if (builder.keyboardMode == KeyboardMode.SHOWN) {
            (inputEditText ?: view).let {
                it.post {
                    context.acquireFocus(it)
                }
            }

            inputEditText?.apply {
                if (text.isNotEmpty()) setSelection(text.length)
            }
        }
        super.onShow(dialog)
    }

//--------------------------------------------------------------------------------------------------
//  Getters && setters
//--------------------------------------------------------------------------------------------------

    /**
     * Retrieves the view representing the dialog as a whole. Be careful with this.
     */
    val view: View
        get() = _view!!

    /**
     * Retrieves the custom view that was inflated or set to the MaterialDialog during building.
     *
     * @return The custom view that was passed into the Builder.
     */
    val customView: View?
        get() = builder.customView

    /**
     * Retrieves the view of an action button, allowing you to modify properties such as whether or not it's enabled.
     * Use [.setActionButton] to change text, since the view returned here is not
     * the view that displays text.
     *
     * @param which The action button of which to get the view for.
     * @return The view from the dialog's layout representing this action button.
     */
    fun getActionButton(@DialogAction which: Int): View? = when (which) {
        DialogAction.POSITIVE -> positiveButton
        DialogAction.NEGATIVE -> negativeButton
        DialogAction.NEUTRAL -> neutralButton
        else -> null
    }

    /**
     * Updates an action button's title, causing invalidation to check if the action buttons should be stacked.
     * Setting an action button's text to null is a shortcut for hiding it, too.
     *
     * @param which The action button to update.
     * @param title The new title of the action button.
     */
    @UiThread
    fun setActionButton(@DialogAction which: Int, title: CharSequence?) {
        when (which) {
            DialogAction.NEUTRAL -> {
                builder.neutralText = title
                neutralButton?.apply {
                    this.text = title
                    this.visibility = if (title.isNullOrEmpty()) View.GONE else View.VISIBLE
                }
            }
            DialogAction.NEGATIVE -> {
                builder.negativeText = title
                negativeButton?.apply {
                    this.text = title
                    this.visibility = if (title.isNullOrEmpty()) View.GONE else View.VISIBLE
                }
            }
            else -> {
                builder.positiveText = title
                positiveButton?.apply {
                    this.text = title
                    this.visibility = if (title.isNullOrEmpty()) View.GONE else View.VISIBLE
                }
            }
        }
    }

    /**
     * Updates an action button's title, causing invalidation to check if the action buttons should be stacked.
     *
     * @param which    The action button to update.
     * @param titleRes The string resource of the new title of the action button.
     */
    fun setActionButton(@DialogAction which: Int, @StringRes titleRes: Int) {
        setActionButton(which, context.getText(titleRes))
    }

    /**
     * Gets whether or not the positive, neutral, or negative action button is visible.
     *
     * @return Whether or not 1 or more action buttons is visible.
     */
    fun hasActionButtons(): Boolean = numberOfActionButtons() > 0

    /**
     * Gets the number of visible action buttons.
     *
     * @return 0 through 3, depending on how many should be or are visible.
     */
    fun numberOfActionButtons(): Int = run {
        var number = 0
        if (builder.positiveText != null && positiveButton?.visibility == View.VISIBLE) number++
        if (builder.neutralText != null && neutralButton?.visibility == View.VISIBLE) number++
        if (builder.negativeText != null && negativeButton?.visibility == View.VISIBLE) number++
        number
    }

    @UiThread
    override fun setTitle(newTitle: CharSequence?) {
        titleLabel?.apply {
            text = newTitle
            (if (newTitle.isNullOrEmpty()) View.GONE else View.VISIBLE).let { visibility ->
                this.visibility = visibility
                titleFrame?.visibility = visibility
            }
        }
    }

    @UiThread
    override fun setTitle(@StringRes newTitleRes: Int) {
        setTitle(builder.context.getString(newTitleRes))
    }

    @UiThread
    fun setTitle(@StringRes newTitleRes: Int, vararg formatArgs: Any?) {
        setTitle(builder.context.getString(newTitleRes, *formatArgs))
    }

    @UiThread
    fun setIcon(@DrawableRes resId: Int) {
        titleIcon?.apply {
            this.setImageResource(resId)
            this.visibility = if (resId != NO_RESOURCE) View.VISIBLE else View.GONE
        }
    }

    @UiThread
    fun setIcon(d: Drawable?) {
        titleIcon?.apply {
            this.setImageDrawable(d)
            this.visibility = if (d != null) View.VISIBLE else View.GONE
        }
    }

    @UiThread
    fun setIconAttribute(@AttrRes attrId: Int) {
        setIcon(builder.context.resolveDrawable(attrId))
    }

    @UiThread
    fun setContent(newContent: CharSequence?) {
        contentView?.apply {
            this.text = newContent
            this.visibility = if (newContent.isNullOrEmpty()) View.GONE else View.VISIBLE
        }
    }

    @UiThread
    fun setContent(@StringRes newContentRes: Int) {
        setContent(builder.context.getString(newContentRes))
    }

    @UiThread
    fun setContent(@StringRes newContentRes: Int, vararg formatArgs: Any?) {
        setContent(builder.context.getString(newContentRes, *formatArgs))
    }

    @UiThread
    fun setItems(items: Array<CharSequence>?) {
        checkNotNull(builder.adapter) { "This MaterialDialog instance does not yet have an adapter set to it. You cannot use setItems()." }
        builder.items = items
        if (builder.adapter is MaterialDialogBaseAdapter) {
            builder.adapter = if (builder.itemsLayoutType == ItemsLayoutType.LIST) {
                MaterialDialogListAdapter(this)
            } else {
                MaterialDialogGridAdapter(this)
            }
        } else {
            throw IllegalStateException("When using a custom adapter, setItems() cannot be used. Set items through the adapter instead.")
        }
        listView?.adapter = builder.adapter
    }

    val currentProgress: Int
        get() = progressBar?.progress ?: -1

    fun incrementProgress(by: Int) {
        setProgress(currentProgress + by)
    }

    fun setProgress(progress: Int) {
        check(builder.progress > -2) { "Cannot use setProgress() on this dialog." }
        progressBar?.progress = progress
        handler.post {
            val percent: Float = currentProgress.toFloat() / maxProgress.toFloat()
            progressLabel?.text = (builder.progressPercentFormat?.format(percent) ?: percent.toString())
                .toLocalNumerals(builder.numeralSystem)

            progressMinMaxLabel?.text = String
                .format(
                    builder.progressNumberFormat,
                    currentProgress,
                    maxProgress
                )
                .toLocalNumerals(builder.numeralSystem)
        }
    }

    private val handler: Handler
        get() = _handler ?: Handler(Looper.getMainLooper()).also { _handler = it }

    val isIndeterminateProgress: Boolean
        get() = builder.indeterminateProgress

    var maxProgress: Int
        get() = progressBar?.max ?: -1
        set(max) {
            check(builder.progress > -2) { "Cannot use setMaxProgress() on this dialog." }
            progressBar?.max = max
        }

    /**
     * Change the format of the small text showing the percentage of progress.
     * The default is NumberFormat.getPercentageInstance().
     */
    fun setProgressPercentFormat(format: NumberFormat) {
        builder.progressPercentFormat = format
        setProgress(currentProgress) // invalidates display
    }

    /**
     * Change the format of the small text showing current and maximum units of progress.
     * The default is "%1d/%2d".
     */
    fun setProgressNumberFormat(format: String) {
        builder.progressNumberFormat = format
        setProgress(currentProgress) // invalidates display
    }

    /**
     * Convenience method for getting the currently selected index of a single choice list.
     *
     * @return Currently selected index of a single choice list, or -1 if not showing a single choice list
     */
    /**
     * Convenience method for setting the currently selected index of a single choice list.
     * This only works if you are not using a custom adapter; if you're using a custom adapter,
     * an IllegalStateException is thrown. Note that this does not call the respective single choice callback.
     *
     * @param index The index of the list item to check.
     */
    @set:UiThread
    var selectedIndex: Int
        get() = if (builder.itemsCallbackSingleChoice != null) {
            builder.selectedIndex
        } else {
            INVALID
        }
        set(index) {
            builder.selectedIndex = index
            if (builder.adapter is MaterialDialogBaseAdapter) {
                builder.adapter?.notifyDataSetChanged()
            } else {
                throw IllegalStateException("You can only use setSelectedIndex() with the default adapter implementation.")
            }
        }

    /**
     * Convenience method for getting the currently selected indices of a multi choice list
     *
     * @return Currently selected index of a multi choice list, or null if not showing a multi choice list
     */
    val selectedIndices: IntArray?
        get() = if (builder.itemsCallbackMultiChoice == null) {
            null
        } else {
            selectedIndicesList?.toIntArray()
        }

    /**
     * Convenience method for setting the currently selected indices of a multi choice list.
     * This only works if you are not using a custom adapter; if you're using a custom adapter,
     * an IllegalStateException is thrown. Note that this does not call the respective multi choice callback.
     *
     * @param indices The indices of the list items to check.
     */
    @UiThread
    fun setSelectedIndices(indices: IntArray) {
        builder.selectedIndices = indices
        selectedIndicesList = indices.toMutableList()

        if (builder.adapter is MaterialDialogBaseAdapter) {
            builder.adapter?.notifyDataSetChanged()
        } else {
            throw IllegalStateException("You can only use setSelectedIndices() with the default adapter implementation.")
        }
    }

    /**
     * Clears all selected checkboxes from multi choice list dialogs.
     */
    fun clearSelectedIndices() {
        checkNotNull(selectedIndicesList) { "You can only use clearSelectedIndicies() with multi choice list dialogs." }
        builder.selectedIndices = null
        selectedIndicesList?.clear()
        if (builder.adapter is MaterialDialogBaseAdapter) {
            builder.adapter?.notifyDataSetChanged()
        } else {
            throw IllegalStateException("You can only use clearSelectedIndicies() with the default adapter implementation.")
        }
    }

    @SuppressLint("SetTextI18n")
    fun invalidateInputMinMaxIndicator(currentLength: Int, emptyDisabled: Boolean) {
        inputMinMaxLabel?.let { input ->
            input.text = "$currentLength/${builder.inputMaxLength}"
                .toLocalNumerals(builder.numeralSystem)
            val isDisabled: Boolean = emptyDisabled && currentLength == 0 || currentLength > builder.inputMaxLength
            val colorText: Int = if (isDisabled) builder.inputMaxLengthErrorColor else builder.contentColor
            val colorWidget: Int = if (isDisabled) builder.inputMaxLengthErrorColor else builder.widgetColor
            input.setTextColor(colorText)
            inputEditText?.setTint(colorWidget)
            getActionButton(DialogAction.POSITIVE)?.isEnabled = !isDisabled
        }
    }

    private fun hasIcons(): Boolean {
        return builder.icons
            ?.let { icons ->
                icons.isNotEmpty() && icons.size == builder.items?.size
            }
            ?: false
    }

    val listItemLayout: Int
        get() = if (hasIcons()) {
            R.layout.md_listitem_icon
        } else {
            when (itemsChoiceMode) {
                ItemsChoiceMode.SINGLE -> R.layout.md_listitem_singlechoice
                ItemsChoiceMode.MULTI -> R.layout.md_listitem_multichoice
                /* REGULAR_LIST_TYPE */
                else -> R.layout.md_listitem_regular
            }
        }

    val gridItemLayout: Int
        get() = if (hasIcons()) {
            R.layout.md_griditem_icon
        } else {
            R.layout.md_griditem_regular
        }

    fun isItemEnabled(position: Int): Boolean =
        builder.itemsDisabledPositions.isDialogItemEnabled(position)

//--------------------------------------------------------------------------------------------------
//  Event handlers
//--------------------------------------------------------------------------------------------------

    override fun onItemClick(
        parent: AdapterView<*>?,
        view: View,
        position: Int,
        id: Long
    ) {
        when {
            builder.itemsCallbackCustom != null ->
                onCustomItemClick(view, position)
            itemsChoiceMode == ItemsChoiceMode.REGULAR ->
                onRegularItemClick(view, position)
            itemsChoiceMode == ItemsChoiceMode.MULTI ->
                onMultiChoiceItemClick(view, position)
            itemsChoiceMode == ItemsChoiceMode.SINGLE ->
                onSingleChoiceItemClick(view, position)
        }
    }

    private fun onCustomItemClick(view: View, position: Int) {
        val text: CharSequence = if (view is TextView) {
            view.text
        } else {
            ((view as? ViewGroup)
                ?.getChildAt(0) as? TextView)
                ?.text
                ?: ""
        }
        builder.itemsCallbackCustom
            ?.onSelection(this, view, position, text)
    }

    private fun onRegularItemClick(view: View, position: Int) {
        val allowSelection = builder.itemsCallback
            ?.onSelection(
                this,
                view,
                position,
                builder.items?.getOrNull(position)
            )
            ?: true
        // If auto dismiss is enabled, dismiss the dialog when a list item is selected
        if (allowSelection && builder.autoDismiss) dismiss()
    }

    private fun onMultiChoiceItemClick(view: View, position: Int) {
        ifNotNull(
            builder.adapter as? MaterialDialogBaseAdapter, selectedIndicesList
        ) { adapter, selectedIndices ->
            val shouldBeChecked: Boolean = !selectedIndices.contains(position)
            if (shouldBeChecked) { // Add the selection to the states first so the callback includes it (when alwaysCallMultiChoiceCallback)
                selectedIndices.add(position)
                if (builder.alwaysCallMultiChoiceCallback) { // If the checkbox wasn't previously selected, and the callback returns true, add it to the states and check it
                    if (sendMultiChoiceCallback()) {
                        adapter.getView(position, view, listView)
                    } else { // The callback cancelled selection, remove it from the states
                        selectedIndices.remove(position)
                    }
                } else { // The callback was not used to check if selection is allowed, just select it
                    if (isItemEnabled(position)) {
                        adapter.getView(position, view, listView)
                    } else { // Item is not enabled, so cancel selection, remove it from the states
                        selectedIndices.remove(position)
                    }
                }
            } else { // The checkbox was unchecked
                selectedIndices.remove(position)
                adapter.getView(position, view, listView)
                if (builder.alwaysCallMultiChoiceCallback) {
                    sendMultiChoiceCallback()
                }
            }
            Unit
        }
    }

    /**
     * If alwaysCallSingleChoiceCallback set true, and click on disabled item,
     * item will be selected if only onSelection returns true.
     * If alwaysCallSingleChoiceCallback set false, and click on disabled item,
     * item will not be selected.
     * If autoDismiss set true and without Positive button, and click on disabled item,
     * item will be selected if only onSelection returns true, also dialog will be dismissed
     */
    private fun onSingleChoiceItemClick(view: View, position: Int) {
        ifNotNull(
            builder.adapter as? MaterialDialogBaseAdapter
        ) { adapter ->
            if (builder.autoDismiss
                && builder.positiveText == null
            ) { // If auto dismiss is enabled, and no action button is visible to approve the selection, dismiss the dialog
                val selectedBefore = builder.selectedIndex
                builder.selectedIndex = position
                val allowSelection = sendSingleChoiceCallback(view)

                if (allowSelection) {
                    dismiss()
                } else {
                    // Restore the old selected index, because selection is not allowed
                    builder.selectedIndex = selectedBefore
                }
            } else if (builder.alwaysCallSingleChoiceCallback) {
                val selectedBefore = builder.selectedIndex
                builder.selectedIndex = position
                // Only allow the radio button to be checked if the callback returns true
                val allowSelection = sendSingleChoiceCallback(view)
                if (allowSelection) {
                    adapter.notifyDataSetChanged()
                } else {
                    // Restore the old selected index, because selection is not allowed
                    builder.selectedIndex = selectedBefore
                }
            } else {
                val allowSelection = isItemEnabled(position)
                if (allowSelection
                    && builder.selectedIndex != position
                ) {
                    builder.selectedIndex = position
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun sendSingleChoiceCallback(v: View?): Boolean {
        val text: CharSequence? = if (builder.selectedIndex >= 0) {
            builder.items?.getOrNull(builder.selectedIndex)
        } else {
            null
        }

        return builder.itemsCallbackSingleChoice
            ?.onSelection(this, v, builder.selectedIndex, text)
            ?: true
    }

    private fun sendMultiChoiceCallback(): Boolean {
        val selectedIndices: MutableList<Int> = selectedIndicesList ?: return true

        selectedIndices.sort() // Make sure the indicies are in order
        val selectedTitles: MutableList<CharSequence> = ArrayList()

        for (i in selectedIndices) {
            builder.items?.getOrNull(i)?.let {
                selectedTitles.add(it)
            }
        }
        return builder.itemsCallbackMultiChoice
            ?.onSelection(
                this,
                selectedIndices.toIntArray(),
                selectedTitles.toTypedArray()
            )
            ?: true
    }

    override fun onClick(v: View) {
        @DialogAction val tag: Int = v.tag as Int
        when (tag) {
            DialogAction.POSITIVE -> onPositiveClick()
            DialogAction.NEGATIVE -> onNegativeClick()
            DialogAction.NEUTRAL -> onNeutralClick()
        }
    }

    open fun onPositiveClick() {
        ifNotNullAnd(
            builder.itemsCallbackSingleChoice, !builder.alwaysCallSingleChoiceCallback
        ) {
            sendSingleChoiceCallback(null)
        }
        ifNotNullAnd(
            builder.itemsCallbackMultiChoice, !builder.alwaysCallMultiChoiceCallback
        ) {
            sendMultiChoiceCallback()
        }
        ifNotNullAnd(
            builder.inputCallback, inputEditText, !builder.alwaysCallInputCallback
        ) { callback, editText ->
            callback.onInput(this, editText.text)
        }

        builder.callback?.let {
            it.onAny(this)
            it.onPositive(this)
        }

        if (builder.autoDismiss) dismiss()
    }

    open fun onNegativeClick() {
        builder.callback?.let {
            it.onAny(this)
            it.onNegative(this)
        }
        if (builder.autoDismiss) dismiss()
    }

    open fun onNeutralClick() {
        builder.callback?.let {
            it.onAny(this)
            it.onNeutral(this)
        }
        if (builder.autoDismiss) dismiss()
    }

//--------------------------------------------------------------------------------------------------
//  Classes
//--------------------------------------------------------------------------------------------------

    class DialogException(message: String?) : BadTokenException(message)

    /**
     * A callback used for regular list dialogs.
     */
    interface ItemsCallback {
        /**
         * @return True to allow the item to be selected.
         */
        fun onSelection(
            dialog: MaterialDialog?,
            itemView: View?,
            which: Int,
            text: CharSequence?
        ): Boolean
    }

    /**
     * A callback used for multi choice (check box) list dialogs.
     */
    interface ItemsCallbackSingleChoice {
        /**
         * Return true to allow the radio button to be checked, if the alwaysCallSingleChoice() option is used.
         *
         * @param dialog The dialog of which a list item was selected.
         * @param which  The index of the item that was selected.
         * @param text   The text of the  item that was selected.
         * @return True to allow the radio button to be selected.
         */
        fun onSelection(
            dialog: MaterialDialog?,
            itemView: View?,
            which: Int,
            text: CharSequence?
        ): Boolean
    }

    /**
     * A callback used for multi choice (check box) list dialogs.
     */
    interface ItemsCallbackMultiChoice {
        /**
         * Return true to allow the check box to be checked, if the alwaysCallSingleChoice() option is used.
         *
         * @param dialog The dialog of which a list item was selected.
         * @param which  The indices of the items that were selected.
         * @param text   The text of the items that were selected.
         * @return True to allow the checkbox to be selected.
         */
        fun onSelection(
            dialog: MaterialDialog?,
            which: IntArray,
            text: Array<CharSequence>?
        ): Boolean
    }

    interface OnStopListener {
        fun onStop(dialog: MaterialDialog?)
    }

    interface IButtonCallback {
        fun onAny(dialog: MaterialDialog?)
        fun onPositive(dialog: MaterialDialog?)
        fun onNegative(dialog: MaterialDialog?)
        fun onNeutral(dialog: MaterialDialog?)
    }

    interface InputCallback {
        fun onInput(dialog: MaterialDialog?, input: CharSequence)
    }

    /**
     * Override these as needed, so no needing to sub empty methods from an interface
     */
    abstract class ButtonCallback : IButtonCallback, Cloneable {
        override fun onAny(dialog: MaterialDialog?) {}
        override fun onPositive(dialog: MaterialDialog?) {}
        override fun onNegative(dialog: MaterialDialog?) {}
        override fun onNeutral(dialog: MaterialDialog?) {}
    }

    @IntDef(
        DialogAction.POSITIVE,
        DialogAction.NEUTRAL,
        DialogAction.NEGATIVE
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class DialogAction {
        companion object {
            const val POSITIVE = 0
            const val NEUTRAL = 1
            const val NEGATIVE = 2
        }
    }

    @IntDef(
        KeyboardMode.UNCHANGED,
        KeyboardMode.SHOWN,
        KeyboardMode.HIDDEN
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class KeyboardMode {
        companion object {
            const val UNCHANGED = 0
            const val SHOWN = 1
            const val HIDDEN = 2
        }
    }

    @IntDef(
        ItemsChoiceMode.REGULAR,
        ItemsChoiceMode.SINGLE,
        ItemsChoiceMode.MULTI
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class ItemsChoiceMode {
        companion object {
            const val REGULAR = 0
            const val SINGLE = 1
            const val MULTI = 2
        }
    }

    @IntDef(
        DialogGravity.START,
        DialogGravity.CENTER,
        DialogGravity.END
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class DialogGravity {
        companion object {
            const val START = 0
            const val CENTER = 1
            const val END = 2
        }
    }

    @IntDef(
        ProgressStyle.CIRCULAR,
        ProgressStyle.HORIZONTAL
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class ProgressStyle {
        companion object {
            const val CIRCULAR = 0
            const val HORIZONTAL = 1
        }
    }

    @IntDef(
        ItemsLayoutType.LIST,
        ItemsLayoutType.GRID
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class ItemsLayoutType  {
        companion object {
            const val LIST = 0
            const val GRID = 1
        }
    }

    companion object {
        const val SELECTED_COLOR_ALPHA = 0x32

        const val INPUT_TEXT = "INPUT_TEXT"
        const val SELECTED_INDEX_KEY = "SELECTED_INDEX"
        const val SELECTED_INDEX_LIST_KEY = "SELECTED_INDEX_LIST"

        @StyleRes
        internal fun MaterialDialogBuilder.getThemeRes(): Int =
            if (this.dialogStyleRes != NO_RESOURCE) {
                 this.dialogStyleRes
            } else {
                val isDarkTheme: Boolean = this.context.resolveBoolean(
                    R.attr.md_dark_theme,
                    this.isDarkTheme
                )
                this.isDarkTheme = isDarkTheme

                if (this.bottomSheet) {
                    if (isDarkTheme) {
                        R.style.MaterialDialog_Dark_BottomSheetDialog
                    } else {
                        R.style.MaterialDialog_Light_BottomSheetDialog
                    }
                } else {
                    if (isDarkTheme) {
                        R.style.MaterialDialog_Dark_Dialog
                    } else {
                        R.style.MaterialDialog_Light_Dialog
                    }
                }
            }

        @LayoutRes
        internal fun MaterialDialogBuilder.getRootLayoutRes(): Int =
            when {
                this.customView != null -> R.layout.md_dialog_custom

                !this.items.isNullOrEmpty() || this.adapter != null ->
                    if (this.itemsLayoutType == ItemsLayoutType.GRID) {
                        R.layout.md_dialog_grid
                    } else {
                        R.layout.md_dialog_list
                    }

                this.progress > -2 -> R.layout.md_dialog_progress

                this.indeterminateProgress ->
                    if (this.progressStyle == ProgressStyle.HORIZONTAL) {
                        R.layout.md_dialog_progress_indeterminate_horizontal
                    } else {
                        R.layout.md_dialog_progress_indeterminate_circular
                    }

                this.inputCallback != null -> R.layout.md_dialog_input
                else -> R.layout.md_dialog_basic
            }
    }
}