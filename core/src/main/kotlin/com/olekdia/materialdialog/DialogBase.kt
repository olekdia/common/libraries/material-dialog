package com.olekdia.materialdialog

import android.content.Context
import android.content.DialogInterface
import android.content.DialogInterface.OnShowListener
import android.view.View
import android.view.ViewGroup
import androidx.activity.ComponentDialog
import androidx.annotation.IdRes

/**
 * @author Aidan Follestad (afollestad)
 */
open class DialogBase protected constructor(
    context: Context,
    theme: Int
) : ComponentDialog(context, theme),
    OnShowListener {

    @JvmField
    internal var _view: MdRootLayout? = null

    private var showListener: OnShowListener? = null

    override fun <T : View> findViewById(@IdRes id: Int): T? {
        return _view?.findViewById(id) as? T?
    }

    override fun setOnShowListener(listener: OnShowListener?) {
        showListener = listener
    }

    fun setOnShowListenerInternal() {
        super.setOnShowListener(this)
    }

    fun setViewInternal(view: View) {
        super.setContentView(view)
    }

    override fun onShow(dialog: DialogInterface) {
        showListener?.onShow(dialog)
    }

    @Deprecated("Specify a custom view in the Builder instead")
    @Throws(IllegalAccessError::class)
    override fun setContentView(layoutResID: Int): Unit =
        throw IllegalAccessError("Not supported")

    @Deprecated("Specify a custom view in the Builder instead")
    @Throws(IllegalAccessError::class)
    override fun setContentView(view: View): Unit =
        throw IllegalAccessError("Not supported")

    @Deprecated("Specify a custom view in the Builder instead")
    @Throws(IllegalAccessError::class)
    override fun setContentView(
        view: View,
        params: ViewGroup.LayoutParams?
    ): Unit = throw IllegalAccessError("Not supported")
}