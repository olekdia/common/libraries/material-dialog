@file:JvmName("MdExt")
package com.olekdia.materialdialog

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.annotation.AttrRes
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.widget.CompoundButtonCompat
import com.olekdia.androidcommon.extensions.getColorStateListCompat
import com.olekdia.androidcommon.extensions.resolveColor
import com.olekdia.androidcommon.extensions.setColorFilterCompat
import com.olekdia.common.extensions.adjustAlpha
import com.olekdia.materialdialog.MaterialDialog.DialogGravity

fun TextView?.setTypefaceSubpixel(t: Typeface?) {
    if (this != null) {
        this.paintFlags = this.paintFlags or Paint.SUBPIXEL_TEXT_FLAG
        this.typeface = t
    }
}

@SuppressLint("PrivateResource")
fun RadioButton.setTint(color: Int) {
    val csl = ColorStateList(
        arrayOf(
            intArrayOf(-android.R.attr.state_checked),
            intArrayOf(android.R.attr.state_checked)
        ), intArrayOf(
            this.context.resolveColor(androidx.appcompat.R.attr.colorControlNormal),
            color
        )
    )
    CompoundButtonCompat.setButtonTintList(this, csl)
}

fun SeekBar.setTint(color: Int) {
    val csl = ColorStateList.valueOf(color)
    this.thumbTintList = csl
    this.progressTintList = csl
}

fun ProgressBar.setTint(color: Int) {
    this.setTint(color, false)
}

fun ProgressBar.setTint(color: Int, skipIndeterminate: Boolean) {
    val scl = ColorStateList.valueOf(color)
    this.progressTintList = scl
    this.secondaryProgressTintList = scl
    if (!skipIndeterminate) {
        this.indeterminateTintList = scl
    }
}

private fun createEditTextColorStateList(
    context: Context,
    color: Int
): ColorStateList {
    val states: Array<IntArray?> = arrayOfNulls<IntArray>(3)
    val colors: IntArray = IntArray(3)
    var i = 0
    states[i] = intArrayOf(-android.R.attr.state_enabled)
    colors[i] = context.resolveColor(androidx.appcompat.R.attr.colorControlNormal)
    i++
    states[i] = intArrayOf(-android.R.attr.state_pressed, -android.R.attr.state_focused)
    colors[i] = context.resolveColor(androidx.appcompat.R.attr.colorControlNormal)
    i++
    states[i] = intArrayOf()
    colors[i] = color

    return ColorStateList(states, colors)
}

@SuppressLint("RestrictedApi")
fun EditText.setTint(color: Int) {
    if (this is AppCompatEditText) {
        this.supportBackgroundTintList = createEditTextColorStateList(this.context, color)
    } else {
        this.backgroundTintList = createEditTextColorStateList(this.context, color)
    }
}

@SuppressLint("PrivateResource")
fun CheckBox.setTint(color: Int) {
    val csl = ColorStateList(
        arrayOf(
            intArrayOf(-android.R.attr.state_checked),
            intArrayOf(android.R.attr.state_checked)
        ), intArrayOf(
            this.context.resolveColor(androidx.appcompat.R.attr.colorControlNormal),
            color
        )
    )
    CompoundButtonCompat.setButtonTintList(this, csl)
}

@DialogGravity
fun Context.resolveDialogGravity(
    @AttrRes attr: Int,
    @DialogGravity defaultGravity: Int
): Int = this.theme.obtainStyledAttributes(intArrayOf(attr)).let { a ->
    a.getInt(0, defaultGravity)
        .also { a.recycle() }
}

// Try to resolve the colorAttr attribute.
fun Context.resolveActionTextColorStateList(
    @AttrRes colorAttr: Int,
    fallback: ColorStateList
): ColorStateList {
    val a: TypedArray = this.theme.obtainStyledAttributes(intArrayOf(colorAttr))
    return try {
        val value: TypedValue = a.peekValue(0) ?: return fallback
        if (value.type >= TypedValue.TYPE_FIRST_COLOR_INT
            && value.type <= TypedValue.TYPE_LAST_COLOR_INT
        ) {
            this.getActionTextStateList(value.data)
        } else {
            a.getColorStateList(0) ?: fallback
        }
    } finally {
        a.recycle()
    }
}

fun Context.getActionTextStateList(primaryColor: Int): ColorStateList {
    var newPrimaryColor: Int = primaryColor
    val fallBackButtonColor: Int = this.resolveColor(android.R.attr.textColorPrimary)
    if (newPrimaryColor == Color.TRANSPARENT) newPrimaryColor = fallBackButtonColor
    val states = arrayOf(
        intArrayOf(-android.R.attr.state_enabled), // disabled
        intArrayOf() // enabled
    )
    val colors: IntArray = intArrayOf(
        newPrimaryColor.adjustAlpha(0.4f),
        newPrimaryColor
    )
    return ColorStateList(states, colors)
}

// Get the specified color resource, creating a ColorStateList if the resource
// points to a color value.
fun Context.getActionTextColorStateList(@ColorRes colorId: Int): ColorStateList {
    val value = TypedValue()
    this.resources.getValue(colorId, value, true)
    return if (value.type >= TypedValue.TYPE_FIRST_COLOR_INT
        && value.type <= TypedValue.TYPE_LAST_COLOR_INT
    ) {
        this.getActionTextStateList(value.data)
    } else {
        this.getColorStateListCompat(colorId)!!
    }
}

@SuppressLint("RtlHardcoded")
fun @receiver:DialogGravity Int.fromDialogGravityToGravity(): Int {
    return when (this) {
        DialogGravity.START -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) Gravity.START else Gravity.RIGHT
        DialogGravity.CENTER -> Gravity.CENTER_HORIZONTAL
        DialogGravity.END -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) Gravity.END else Gravity.LEFT
        else -> throw IllegalStateException("Invalid gravity constant")
    }
}

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
fun @receiver:DialogGravity Int.fromDialogGravityToTextAlignment(): Int {
    return when (this) {
        DialogGravity.CENTER -> View.TEXT_ALIGNMENT_CENTER
        DialogGravity.END -> View.TEXT_ALIGNMENT_VIEW_END
        else -> View.TEXT_ALIGNMENT_VIEW_START
    }
}

fun IntArray?.isDialogItemEnabled(position: Int): Boolean {
    return if (this == null) {
        true
    } else {
        for (i in this.indices.reversed()) {
            if (this[i] == position) return false
        }
        true
    }
}

fun isBottomSheetListSupported(): Boolean = true