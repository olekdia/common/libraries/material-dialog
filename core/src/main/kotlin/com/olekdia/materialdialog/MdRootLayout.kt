package com.olekdia.materialdialog

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.ViewTreeObserver.OnScrollChangedListener
import android.webkit.WebView
import android.widget.AdapterView
import android.widget.ScrollView
import androidx.core.widget.NestedScrollView
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.getDimensionPixelSize
import com.olekdia.androidcommon.extensions.resolveColor
import com.olekdia.androidcommon.extensions.scaleCompat
import com.olekdia.common.extensions.ifNotNull
import com.olekdia.materialdialog.MaterialDialog.DialogGravity
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

/**
 * @author Kevin Barry (teslacoil) 4/02/2015
 *
 *
 * This is the top level view for all MaterialDialogs
 * It handles the layout of:
 * titleFrame (md_stub_titleframe)
 * content (text, custom view, listview, etc)
 * buttonDefault... (either stacked or horizontal)
 */
class MdRootLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = NO_RESOURCE
) : ViewGroup(context, attrs, defStyleAttr) {

    private var titleBar: View? = null
    private var content: View? = null
    private var drawTopDivider = false
    private var drawBottomDivider = false
    private val buttons = arrayOfNulls<MdButton>(3)
    private var forceStack = false
    private var isStacked = false
    private var useFullPadding = true
    private var reducePaddingNoTitleNoButtons = false
    private var noTitleNoPadding = false
    private var noTitlePaddingFull = context.getDimensionPixelSize(R.dimen.md_notitle_vertical_padding)
    private var buttonPaddingFull = context.getDimensionPixelSize(R.dimen.md_button_frame_vertical_padding)
    private var buttonBarHeight = context.getDimensionPixelSize(R.dimen.md_button_height)
    @DialogGravity
    private var buttonGravity = DialogGravity.START
    /* Margin from dialog frame to first button */
    private var buttonHorizontalEdgeMargin = context.getDimensionPixelSize(R.dimen.md_button_padding_frame_side)
    private val dividerPaint: Paint = Paint()
        .apply {
            color = context.resolveColor(R.attr.md_divider_color)
        }
    private var topOnScrollChangedListener: OnScrollChangedListener? = null
    private var bottomOnScrollChangedListener: OnScrollChangedListener? = null
    private var dividerWidth: Float = context.getDimensionPixelSize(R.dimen.md_divider_height).toFloat()

    init {
        val a: TypedArray = context.obtainStyledAttributes(
            attrs, R.styleable.MdRootLayout, defStyleAttr, 0
        )
        reducePaddingNoTitleNoButtons = a.getBoolean(
            R.styleable.MdRootLayout_md_reduce_padding_no_title_no_buttons, true
        )
        a.recycle()
        setWillNotDraw(false)
    }

    fun noTitleNoPadding() {
        noTitleNoPadding = true
    }

    public override fun onFinishInflate() {
        super.onFinishInflate()
        for (i in 0 until childCount) {
            val v = getChildAt(i)
            when (v.id) {
                R.id.titleFrame -> titleBar = v
                R.id.buttonDefaultNeutral -> buttons[INDEX_NEUTRAL] = v as MdButton
                R.id.buttonDefaultNegative -> buttons[INDEX_NEGATIVE] = v as MdButton
                R.id.buttonDefaultPositive -> buttons[INDEX_POSITIVE] = v as MdButton
                else -> content = v
            }
        }
    }

    @ExperimentalContracts
    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width: Int = MeasureSpec.getSize(widthMeasureSpec)
        val height: Int = MeasureSpec.getSize(heightMeasureSpec)
        useFullPadding = true
        var hasButtons: Boolean = false
        val stacked: Boolean
        if (!forceStack) {
            var buttonsWidth = 0
            for (button: MdButton? in buttons) {
                if (button.isNotNullAndVisible()) {
                    button.setStacked(stacked = false, force = false)
                    measureChild(button, widthMeasureSpec, heightMeasureSpec)
                    buttonsWidth += button.measuredWidth
                    hasButtons = true
                }
            }
            val buttonBarPadding: Int = context.getDimensionPixelSize(R.dimen.md_neutral_button_margin)
            val buttonFrameWidth: Int = width - 2 * buttonBarPadding
            stacked = buttonsWidth > buttonFrameWidth
        } else {
            stacked = true
        }
        var stackedHeight: Int = 0
        isStacked = stacked
        if (stacked) {
            for (button: MdButton? in buttons) {
                if (button.isNotNullAndVisible()) {
                    button.setStacked(stacked = true, force = false)
                    measureChild(button, widthMeasureSpec, heightMeasureSpec)
                    stackedHeight += button.measuredHeight
                    hasButtons = true
                }
            }
        }
        var availableHeight: Int = height
        var fullPadding: Int = 0
        var minPadding: Int = 0
        if (hasButtons) {
            if (isStacked) {
                availableHeight -= stackedHeight
                fullPadding += 2 * buttonPaddingFull
                minPadding += 2 * buttonPaddingFull
            } else {
                availableHeight -= buttonBarHeight
                fullPadding += 2 * buttonPaddingFull
                /* No minPadding */
            }
        } else { /* Content has 8dp, we add 16dp and get 24dp, the frame margin */
            fullPadding += 2 * buttonPaddingFull
        }

        val titleBar: View? = titleBar
        if (titleBar.isNotNullAndVisible()) {
            titleBar.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.UNSPECIFIED
            )
            availableHeight -= titleBar.measuredHeight
        } else if (!noTitleNoPadding) {
            fullPadding += noTitlePaddingFull
        }

        val content: View? = content
        if (content.isNotNullAndVisible()) {
            content.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(availableHeight - minPadding, MeasureSpec.AT_MOST)
            )
            if (content.measuredHeight <= availableHeight - fullPadding) {
                if (!reducePaddingNoTitleNoButtons
                    || titleBar.isNotNullAndVisible()
                    || hasButtons
                ) {
                    useFullPadding = true
                    availableHeight -= content.measuredHeight + fullPadding
                } else {
                    useFullPadding = false
                    availableHeight -= content.measuredHeight + minPadding
                }
            } else {
                useFullPadding = false
                availableHeight = 0
            }
        }
        setMeasuredDimension(width, height - availableHeight)
    }

    public override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        content?.let {
            if (drawTopDivider) {
                val y: Float = it.top.toFloat()
                canvas.drawRect(
                    0f,
                    y - dividerWidth,
                    measuredWidth.toFloat(),
                    y,
                    dividerPaint
                )
            }
            if (drawBottomDivider) {
                val y: Float = it.bottom.toFloat()
                canvas.drawRect(
                    0f,
                    y,
                    measuredWidth.toFloat(),
                    y + dividerWidth,
                    dividerPaint
                )
            }
        }
    }

    @ExperimentalContracts
    override fun onLayout(
        changed: Boolean,
        l: Int,
        top: Int,
        r: Int,
        bottom: Int
    ) {
        var t = top
        var b = bottom
        val titleBar: View? = titleBar
        val content: View? = content

        if (titleBar.isNotNullAndVisible()) {
            val height: Int = titleBar.measuredHeight
            titleBar.layout(l, t, r, t + height)
            t += height
        } else if (!noTitleNoPadding && useFullPadding) {
            t += noTitlePaddingFull
        }
        if (content.isNotNullAndVisible()) {
            content.layout(l, t, r, t + content.measuredHeight)
        }
        if (isStacked) {
            b -= buttonPaddingFull
            for (button: MdButton? in buttons) {
                if (button.isNotNullAndVisible()) {
                    button.layout(l, b - button.measuredHeight, r, b)
                    b -= button.measuredHeight
                }
            }
        } else {
            val barTop: Int
            var barBottom: Int = b
            if (useFullPadding) barBottom -= buttonPaddingFull
            barTop = barBottom - buttonBarHeight
            /* START:
               Neutral   Negative  Positive

               CENTER:
               Negative  Neutral   Positive

               END:
               Positive  Negative  Neutral

               (With no Positive, Negative takes it's place except for CENTER)
             */
            var offset: Int = buttonHorizontalEdgeMargin
            /* Used with CENTER gravity */
            var neutralLeft: Int = -1
            var neutralRight: Int = -1
            val positiveButton: MdButton? = buttons[INDEX_POSITIVE]
            if (positiveButton.isNotNullAndVisible()) {
                val bl: Int
                val br: Int
                if (buttonGravity == DialogGravity.END) {
                    bl = l + offset
                    br = bl +positiveButton.measuredWidth
                } else { /* START || CENTER */
                    br = r - offset
                    bl = br -positiveButton.measuredWidth
                    neutralRight = bl
                }
                positiveButton.layout(bl, barTop, br, barBottom)
                offset += positiveButton.measuredWidth
            }

            val negativeButton: MdButton? = buttons[INDEX_NEGATIVE]
            if (negativeButton.isNotNullAndVisible()) {
                val bl: Int
                val br: Int
                when (buttonGravity) {
                    DialogGravity.END -> {
                        bl = l + offset
                        br = bl + negativeButton.measuredWidth
                    }
                    DialogGravity.START -> {
                        br = r - offset
                        bl = br - negativeButton.measuredWidth
                    }
                    else -> { /* CENTER */
                        bl = l + buttonHorizontalEdgeMargin
                        br = bl + negativeButton.measuredWidth
                        neutralLeft = br
                    }
                }
                negativeButton.layout(bl, barTop, br, barBottom)
            }

            val neutralButton: MdButton? = buttons[INDEX_NEUTRAL]
            if (neutralButton.isNotNullAndVisible()) {
                val bl: Int
                val br: Int
                when (buttonGravity) {
                    DialogGravity.END -> {
                        br = r - buttonHorizontalEdgeMargin
                        bl = br - neutralButton.measuredWidth
                    }
                    DialogGravity.START -> {
                        bl = l + buttonHorizontalEdgeMargin
                        br = bl + neutralButton.measuredWidth
                    }
                    else -> { /* CENTER */
                        if (neutralLeft == -1 && neutralRight != -1) {
                            neutralLeft = neutralRight - neutralButton.measuredWidth
                        } else if (neutralRight == -1 && neutralLeft != -1) {
                            neutralRight = neutralLeft + neutralButton.measuredWidth
                        } else if (neutralRight == -1) {
                            neutralLeft = (r - l) / 2 - neutralButton.measuredWidth / 2
                            neutralRight = neutralLeft + neutralButton.measuredWidth
                        }
                        bl = neutralLeft
                        br = neutralRight
                    }
                }
                neutralButton.layout(bl, barTop, br, barBottom)
            }
        }
        setUpDividersVisibility(content, setForTop = true, setForBottom = true)
    }

    fun setForceStack(forceStack: Boolean) {
        this.forceStack = forceStack
        invalidate()
    }

    fun setDividerColor(color: Int) {
        dividerPaint.color = color
        invalidate()
    }

    fun setButtonGravity(@DialogGravity gravity: Int) {
        buttonGravity = gravity
    }

    fun setButtonStackedGravity(@DialogGravity gravity: Int) {
        for (button in buttons) {
            button?.setStackedGravity(gravity)
        }
    }

    private fun setUpDividersVisibility(
        view: View?,
        setForTop: Boolean,
        setForBottom: Boolean
    ) {
        when (view) {
            is ScrollView -> {
                val scroll: ScrollView = view
                if (canScrollViewScroll(scroll)) {
                    addScrollListener(scroll, setForTop, setForBottom)
                } else {
                    if (setForTop) drawTopDivider = false
                    if (setForBottom) drawBottomDivider = false
                }
            }
            is NestedScrollView -> {
                val nestedScroll: NestedScrollView = view
                if (canNestedScrollViewScroll(nestedScroll)) {
                    addScrollListener(nestedScroll, setForTop, setForBottom)
                } else {
                    if (setForTop) drawTopDivider = false
                    if (setForBottom) drawBottomDivider = false
                }
            }
            is AdapterView<*> -> {
                val adapterView: AdapterView<*> = view
                if (canAdapterViewScroll(adapterView)) {
                    addScrollListener(adapterView, setForTop, setForBottom)
                } else {
                    if (setForTop) drawTopDivider = false
                    if (setForBottom) drawBottomDivider = false
                }
            }
            is WebView -> {
                val webView: WebView = view
                view.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                    override fun onPreDraw(): Boolean {
                        if (view.measuredHeight != 0) {
                            if (!canWebViewScroll(webView)) {
                                if (setForTop) drawTopDivider = false
                                if (setForBottom) drawBottomDivider = false
                            } else {
                                addScrollListener(webView, setForTop, setForBottom)
                            }
                            view.getViewTreeObserver().removeOnPreDrawListener(this)
                        }
                        return true
                    }
                })
            }
            is ViewGroup -> {
                val topView: View? = view.getTopView()
                setUpDividersVisibility(topView, setForTop, setForBottom)
                val bottomView: View? = view.getBottomView()
                if (bottomView !== topView) {
                    setUpDividersVisibility(bottomView, setForTop = false, setForBottom = true)
                }
            }
        }
    }

    private fun addScrollListener(
        vg: ViewGroup,
        setForTop: Boolean,
        setForBottom: Boolean
    ) {
        if ((!setForBottom && topOnScrollChangedListener == null
                    || (setForBottom && bottomOnScrollChangedListener == null))) {

            val onScrollChangedListener: OnScrollChangedListener = object : OnScrollChangedListener {
                override fun onScrollChanged() {
                    var hasButtons: Boolean = false
                    for (button: MdButton? in buttons) {
                        if (button != null
                            && button.visibility != View.GONE
                        ) {
                            hasButtons = true
                            break
                        }
                    }

                    if (vg is WebView) {
                        invalidateDividersForWebView(
                            vg,
                            setForTop,
                            setForBottom,
                            hasButtons
                        )
                    } else {
                        invalidateDividersForScrollingView(
                            vg,
                            setForTop,
                            setForBottom,
                            hasButtons
                        )
                    }
                    invalidate()
                }
            }

            if (!setForBottom) {
                topOnScrollChangedListener = onScrollChangedListener
                vg.viewTreeObserver.addOnScrollChangedListener(topOnScrollChangedListener)
            } else {
                bottomOnScrollChangedListener = onScrollChangedListener
                vg.viewTreeObserver.addOnScrollChangedListener(bottomOnScrollChangedListener)
            }
            onScrollChangedListener.onScrollChanged()
        }
    }

    private fun invalidateDividersForScrollingView(
        view: ViewGroup,
        setForTop: Boolean,
        setForBottom: Boolean,
        hasButtons: Boolean
    ) {
        if (setForTop && view.childCount > 0) {
            drawTopDivider = ifNotNull(
                titleBar, view.getChildAt(0)
            ) { titleBar, child ->
                // Not scrolled to the top
                titleBar.visibility != View.GONE && view.scrollY + view.paddingTop > child.top
            } ?: false
        }
        if (setForBottom && view.childCount > 0) {
            drawBottomDivider = view.getChildAt(view.childCount - 1)
                ?.let { child ->
                    hasButtons && view.scrollY + view.height - view.paddingBottom < child.bottom
                }
                ?: false
        }
    }

    private fun invalidateDividersForWebView(
        view: WebView,
        setForTop: Boolean,
        setForBottom: Boolean,
        hasButtons: Boolean
    ) {
        if (setForTop) {
            drawTopDivider = titleBar
                ?.let {
                    // Not scrolled to the top
                    it.visibility != View.GONE && view.scrollY + view.paddingTop > 0
                }
                ?: false
        }
        if (setForBottom) {
            drawBottomDivider =
                hasButtons && view.scrollY + view.measuredHeight - view.paddingBottom < view.contentHeight * view.scaleCompat
        }
    }

    companion object {
        private const val INDEX_NEUTRAL = 0
        private const val INDEX_NEGATIVE = 1
        private const val INDEX_POSITIVE = 2

        private fun canScrollViewScroll(sv: ScrollView): Boolean {
            if (sv.childCount == 0) return false

            val childHeight: Int = sv.getChildAt(0).measuredHeight
            return sv.measuredHeight - sv.paddingTop - sv.paddingBottom < childHeight
        }

        private fun canNestedScrollViewScroll(sv: NestedScrollView): Boolean {
            if (sv.childCount == 0) return false

            val childHeight: Int = sv.getChildAt(0).measuredHeight
            return sv.measuredHeight - sv.paddingTop - sv.paddingBottom < childHeight
        }

        private fun canWebViewScroll(view: WebView): Boolean {
            return view.measuredHeight < view.contentHeight * view.scaleCompat
        }

        private fun canAdapterViewScroll(av: AdapterView<*>): Boolean { /* Force it to layout it's children */
            if (av.lastVisiblePosition == -1) return false

            /* We can scroll if the first or last item is not visible */
            val firstItemVisible: Boolean = av.firstVisiblePosition == 0
            val lastItemVisible: Boolean = av.lastVisiblePosition == av.count - 1

            return if (firstItemVisible
                && lastItemVisible
                && av.childCount > 0
            ) { /* Or the first item's top is above or own top */
                if (av.getChildAt(0).top < av.paddingTop) {
                    true
                } else {
                    av.getChildAt(av.childCount - 1).bottom > av.height - av.paddingBottom
                }
                /* or the last item's bottom is beyond our own bottom */
            } else {
                true
            }
        }

        /**
         * Find the view touching the bottom of this ViewGroup. Non visible children are ignored,
         * however getChildDrawingOrder is not taking into account for simplicity and because it behaves
         * inconsistently across platform versions.
         *
         * @return View touching the bottom of this ViewGroup or null
         */
        private fun ViewGroup?.getBottomView(): View? {
            if (this == null || this.childCount == 0) return null

            var bottomView: View? = null
            for (i in this.childCount - 1 downTo 0) {
                val child: View? = this.getChildAt(i)
                if (child != null
                    && child.visibility == View.VISIBLE
                    && child.bottom == this.measuredHeight
                ) {
                    bottomView = child
                    break
                }
            }
            return bottomView
        }

        private fun ViewGroup?.getTopView(): View? {
            if (this == null || this.childCount == 0) return null

            var topView: View? = null
            for (i in this.childCount - 1 downTo 0) {
                val child: View? = this.getChildAt(i)
                if (child != null
                    && child.visibility == View.VISIBLE
                    && child.top == 0
                ) {
                    topView = child
                    break
                }
            }
            return topView
        }
    }
}

@ExperimentalContracts
private fun View?.isNotNullAndVisible(): Boolean {
    contract {
        returns(true) implies (this@isNotNullAndVisible != null)
    }
    var visible: Boolean = this != null && this.visibility != View.GONE
    if (visible && this is MdButton) {
        visible = this.text.toString().trim { it <= ' ' }.isNotEmpty()
    }
    return visible
}