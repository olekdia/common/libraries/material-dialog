package com.olekdia.materialdialog

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.*
import androidx.appcompat.view.ContextThemeWrapper
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.AccessibilityDelegateCompat
import androidx.core.view.ViewCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import androidx.core.widget.NestedScrollView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.*
import com.olekdia.common.INVALID
import com.olekdia.common.extensions.adjustAlpha
import com.olekdia.common.extensions.ifNotNullAnd
import com.olekdia.common.extensions.toLocalNumerals
import com.olekdia.materialdialog.MaterialDialog.*
import java.util.*

internal class DialogConstructor(
    private val dialog: MaterialDialog,
    private val rootView: MdRootLayout
) {
    private val builder: MaterialDialogBuilder = dialog.builder

    fun construct() {
        setupBottomSheet()

        // Set cancelable flag and dialog background color
        dialog.setCancelable(builder.cancelable)
        dialog.setCanceledOnTouchOutside(builder.cancelable)

        if (builder.backgroundColor != 0) {
            rootView.backgroundCompat = GradientDrawable()
                .apply {
                    val r = builder.context.resources
                        .getDimension(R.dimen.md_bg_corner_radius)
                    if (builder.bottomSheet) {
                        this.cornerRadii = floatArrayOf(r, r, r, r, 0f, 0f, 0f, 0f)
                    } else {
                        this.cornerRadius = r
                    }

                    this.setColor(builder.backgroundColor)
                }
        }

        // Retrieve references to views
        dialog.titleLabel = rootView.findViewById(R.id.title)
        dialog.titleIcon = rootView.findViewById(R.id.icon)
        dialog.titleFrame = rootView.findViewById(R.id.titleFrame)
        dialog.contentView = rootView.findViewById(R.id.content)
        dialog.listView = rootView.findViewById(R.id.contentListView)
        dialog.gridView = rootView.findViewById(R.id.contentGridView)
        // Button views initially used by checkIfStackingNeeded()
        dialog.positiveButton = rootView.findViewById(R.id.buttonDefaultPositive)
        dialog.neutralButton = rootView.findViewById(R.id.buttonDefaultNeutral)
        dialog.negativeButton = rootView.findViewById(R.id.buttonDefaultNegative)

        // Setup icon
        dialog.titleIcon?.let { titleIcon ->
            if (builder.icon != null) {
                titleIcon.visibility = View.VISIBLE
                titleIcon.setImageDrawable(builder.icon)
            } else {
                val d: Drawable? = builder.context.resolveDrawable(R.attr.md_icon)
                if (d != null) {
                    titleIcon.visibility = View.VISIBLE
                    titleIcon.setImageDrawable(d)
                } else {
                    titleIcon.visibility = View.GONE
                }
            }

            // Setup icon size limiting
            var maxIconSize: Int = builder.maxIconSize
            if (maxIconSize == INVALID) {
                maxIconSize = builder.context.resolveDimension(R.attr.md_icon_max_size)
            }
            if (builder.limitIconToDefaultSize
                || builder.context.resolveBoolean(R.attr.md_icon_limit_icon_to_default_size)
            ) {
                maxIconSize = builder.context.getDimensionPixelSize(R.dimen.md_title_icon_max_size)
            }
            if (maxIconSize > -1) {
                titleIcon.adjustViewBounds = true
                titleIcon.maxHeight = maxIconSize
                titleIcon.maxWidth = maxIconSize
                titleIcon.requestLayout()
            }
        }

        // Setup divider color in case content scrolls
        if (!builder.dividerColorSet) {
            val dividerFallback: Int = builder.context.resolveColor(R.attr.md_divider)
            builder.dividerColor = builder.context.resolveColor(R.attr.md_divider_color, dividerFallback)
        }
        rootView.setDividerColor(builder.dividerColor)

        // Setup title and title frame
        dialog.titleLabel?.let { titleLabel ->
            titleLabel.setTypefaceSubpixel(builder.mediumFont)
            titleLabel.setTextColor(builder.titleColor)
            titleLabel.gravity = builder.titleGravity.fromDialogGravityToGravity()
            titleLabel.textAlignment = builder.titleGravity.fromDialogGravityToTextAlignment()
            if (builder.title == null) {
                dialog.titleFrame?.visibility = View.GONE
                titleLabel.visibility = View.GONE
            } else {
                titleLabel.text = builder.title?.toLocalNumerals(builder.numeralSystem)
                dialog.titleFrame?.visibility = View.VISIBLE
                titleLabel.visibility = View.VISIBLE
            }
        }

        setupContent()

        setupActionButtons()

        setupList()

        setupGrid()

        setupProgress()

        setupInput()

        setupCustomView()

        // Setup user listeners
        builder.showListener?.run {
            dialog.setOnShowListener(this)
        }
        builder.cancelListener?.run {
            dialog.setOnCancelListener(this)
        }
        builder.dismissListener?.run {
            dialog.setOnDismissListener(this)
        }
        builder.keyListener?.run {
            dialog.setOnKeyListener(this)
        }

        // Setup internal show listener
        dialog.setOnShowListenerInternal()

        // Other internal initialization
        ifNotNullAnd(
            dialog.bottomSheetContainer, builder.bottomSheet
        ) {
            dialog.setViewInternal(it)
        } ?: run {
            dialog.setViewInternal(rootView)
        }

        dialog.listView.checkIfInitScroll()
        dialog.gridView.checkIfInitScroll()

        if (dialog.gridView == null
            && dialog.listView == null
            && dialog.customView == null
        ) {
            dialog.positiveButton?.requestFocus()
        }
    }

    private fun setupActionButtons() { // Don't allow the submit button to not be shown for input dialogs
        if (builder.inputCallback != null && builder.positiveText == null) {
            builder.positiveText = builder.context.getText(android.R.string.ok)
        }
        // Set up the initial visibility of action buttons based on whether or not text was set
        dialog.positiveButton?.visibility = if (builder.positiveText != null) View.VISIBLE else View.GONE
        dialog.neutralButton?.visibility = if (builder.neutralText != null) View.VISIBLE else View.GONE
        dialog.negativeButton?.visibility = if (builder.negativeText != null) View.VISIBLE else View.GONE
        rootView.setButtonGravity(builder.buttonsGravity)
        rootView.setButtonStackedGravity(builder.btnStackedGravity)
        rootView.setForceStack(builder.forceStacking)
        var textAllCaps: Boolean = builder.context.resolveBoolean(android.R.attr.textAllCaps, true)
        if (textAllCaps) {
            textAllCaps = builder.context.resolveBoolean(androidx.appcompat.R.attr.textAllCaps, true)
        }
        dialog.positiveButton?.run {
            this.setTypefaceSubpixel(builder.mediumFont)
            this.isAllCaps = textAllCaps
            this.text = builder.positiveText
            this.setTextColor(builder.positiveColor)
            this.setStackedSelector(
                getButtonSelector(DialogAction.POSITIVE, true)
            )
            this.setDefaultSelector(
                getButtonSelector(DialogAction.POSITIVE, false)
            )
            this.tag = DialogAction.POSITIVE
            this.setOnClickListener(dialog)
        }
        dialog.negativeButton?.run {
            this.setTypefaceSubpixel(builder.mediumFont)
            this.isAllCaps = textAllCaps
            this.text = builder.negativeText
            this.setTextColor(builder.negativeColor)
            this.setStackedSelector(
                getButtonSelector(DialogAction.NEGATIVE, true)
            )
            this.setDefaultSelector(
                getButtonSelector(DialogAction.NEGATIVE, false)
            )
            this.tag = DialogAction.NEGATIVE
            this.setOnClickListener(dialog)
        }
        dialog.neutralButton?.run {
            this.setTypefaceSubpixel(builder.mediumFont)
            this.isAllCaps = textAllCaps
            this.text = builder.neutralText
            this.setTextColor(builder.neutralColor)
            this.setStackedSelector(
                getButtonSelector(DialogAction.NEUTRAL, true)
            )
            this.setDefaultSelector(
                getButtonSelector(DialogAction.NEUTRAL, false)
            )
            this.tag = DialogAction.NEUTRAL
            this.setOnClickListener(dialog)
        }
    }

    private fun setupContent() {
        dialog.contentView?.run {
            this.movementMethod = LinkMovementMethod()
            this.setTypefaceSubpixel(builder.regularFont)
            this.setLineSpacing(0f, builder.contentLineSpacingMultiplier)
            if (builder.positiveColorSet) {
                this.setLinkTextColor(builder.positiveColor)
            } else {
                this.setLinkTextColor(
                    builder.context.resolveColor(android.R.attr.textColorPrimary)
                )
            }
            this.setTextColor(builder.contentColor)
            this.gravity = builder.contentGravity.fromDialogGravityToGravity()
            this.textAlignment = builder.contentGravity.fromDialogGravityToTextAlignment()
            if (builder.content != null) {
                this.text = builder.content?.toLocalNumerals(builder.numeralSystem)
                this.visibility = View.VISIBLE
            } else {
                this.visibility = View.GONE
            }
            isFocusable = false
        }
    }

    private fun setupList() {
        dialog.listView?.run {
            if (builder.items != null
                && builder.items?.isNotEmpty() == true
                && builder.itemsLayoutType == ItemsLayoutType.LIST
                || builder.adapter != null
            ) {
                itemsCanFocus = true
                if (builder.itemsCallbackMultiChoice != null) {
                    dialog.selectedIndicesList = ArrayList()
                }
                this.selector = getListSelector()
                // No custom adapter specified, setup the list with a MaterialDialogListAdapter.
                // Which supports regular lists and single/multi choice dialogs.
                if (builder.adapter == null) {
                    setupItemsSelectionType()
                    builder.adapter = MaterialDialogListAdapter(dialog)
                }
                if (builder.bottomSheet) {
                    this.isNestedScrollingEnabled = true
                }
                // Set up list with adapter
                this.adapter = builder.adapter
                this.onItemClickListener = dialog
            }
        }
    }

    private fun AdapterView<*>?.checkIfInitScroll() {
        val view: AdapterView<*> = this ?: return

        view.viewTreeObserver
            .addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    view.removeOnGlobalLayoutListenerCompat(this)
                    val choiceMode: Int = dialog.itemsChoiceMode
                    if (choiceMode == ItemsChoiceMode.SINGLE
                        || choiceMode == ItemsChoiceMode.MULTI
                    ) {
                        val selectedIndex: Int = if (choiceMode == ItemsChoiceMode.SINGLE) {
                            if (builder.selectedIndex < 0) {
                                INVALID
                            } else {
                                builder.selectedIndex
                            }
                        } else {
                            builder.selectedIndices
                                ?.let { selectedIndices ->
                                    if (selectedIndices.isNotEmpty()) {
                                        val indicesList: MutableList<Int> = selectedIndices.toMutableList()
                                        indicesList.sort()
                                        indicesList[0]
                                    } else {
                                        INVALID
                                    }
                                }
                                ?: INVALID
                        }

                        if (selectedIndex != INVALID
                            && view.lastVisiblePosition < selectedIndex
                        ) {
                            val totalVisible: Int = view.lastVisiblePosition - view.firstVisiblePosition
                            // Scroll so that the selected index appears in the middle (vertically) of the ListView
                            var scrollIndex: Int = selectedIndex - totalVisible / 2
                            if (scrollIndex < 0) scrollIndex = 0
                            val fScrollIndex: Int = scrollIndex

                            view.post {
                                view.setSelection(fScrollIndex)
                                view.requestFocus()
                            }
                        }
                    }
                }
            })
    }

    private fun setupGrid() {
        dialog.gridView?.run {
            if (builder.items != null
                && builder.items?.isNotEmpty() == true
                && builder.itemsLayoutType == ItemsLayoutType.GRID
            ) {
                if (builder.itemsCallbackMultiChoice != null) {
                    dialog.selectedIndicesList = ArrayList()
                }
                if (builder.bottomSheet) {
                    this.isNestedScrollingEnabled = true
                }
                if (builder.adapter == null) {
                    setupItemsSelectionType()
                    builder.adapter = MaterialDialogGridAdapter(dialog)
                }
                this.selector = getListSelector()
                this.adapter = builder.adapter
                this.onItemClickListener = dialog
            }
        }
    }

    private fun setupItemsSelectionType() { // Determine list type
        when {
            builder.itemsCallbackSingleChoice != null ->
                dialog.itemsChoiceMode = ItemsChoiceMode.SINGLE

            builder.itemsCallbackMultiChoice != null ->
                dialog.itemsChoiceMode = ItemsChoiceMode.MULTI
                    .also {
                        builder.selectedIndices?.let {
                            dialog.selectedIndicesList = it.toMutableList()
                        }
                    }

            else ->
                dialog.itemsChoiceMode = ItemsChoiceMode.REGULAR
        }
    }

    private fun setupProgress() {
        if (builder.indeterminateProgress || builder.progress > -2) {
            dialog.progressBar = rootView
                .findViewById<ProgressBar?>(android.R.id.progress)
                ?.apply {
                    if (builder.indeterminateProgress
                        && builder.progressStyle == ProgressStyle.CIRCULAR
                        && Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH
                        && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP
                    ) {
                        this.indeterminateDrawable = CircularProgressDrawable(builder.context)
                            .also {
                                it.colorFilter = PorterDuffColorFilter(
                                    builder.widgetColor,
                                    PorterDuff.Mode.SRC_IN
                                )
                                it.strokeWidth = builder.context
                                    .resources
                                    .getDimension(R.dimen.md_circular_progress_border)
                            }
                        this.setTint(builder.widgetColor, true)
                    } else {
                        this.setTint(builder.widgetColor)
                    }

                    if (!builder.indeterminateProgress
                        || builder.progressStyle == ProgressStyle.HORIZONTAL
                    ) {
                        this.isIndeterminate = builder.indeterminateProgress
                        this.progress = 0
                        this.max = builder.progressMax
                        dialog.progressLabel = rootView
                            .findViewById<TextView?>(R.id.label)
                            ?.apply {
                                this.setTextColor(builder.contentColor)
                                this.setTypefaceSubpixel(builder.mediumFont)
                            }

                        dialog.progressMinMaxLabel = rootView
                            .findViewById<TextView?>(R.id.minMax)
                            ?.also { minMaxLabel ->
                                minMaxLabel.setTextColor(builder.contentColor)
                                minMaxLabel.setTypefaceSubpixel(builder.regularFont)
                                if (builder.showMinMax) {
                                    minMaxLabel.visibility = View.VISIBLE
                                    this.layoutParams = (this.layoutParams as MarginLayoutParams)
                                        .apply {
                                            this.leftMargin = 0
                                            this.rightMargin = 0
                                        }
                                } else {
                                    minMaxLabel.visibility = View.GONE
                                }
                            }
                            ?: run {
                                builder.showMinMax = false
                                null
                            }
                    }

                    if (!builder.indeterminateProgress) dialog.setProgress(0)
                }
        }
    }

    private fun setupInput() {
        dialog.inputEditText = rootView
            .findViewById<EditText?>(android.R.id.input)
            ?.apply {
                this.setTypefaceSubpixel(builder.regularFont)
                builder.inputPrefill?.let { this.setText(it) }
                this.setInternalInputCallback()
                this.hint = builder.inputHint
                this.setSingleLine()
                this.setTextColor(builder.contentColor)
                this.setHintTextColor(builder.contentColor.adjustAlpha(0.3f))
                this.setTint(dialog.builder.widgetColor)
                if (builder.inputType != INVALID) {
                    this.inputType = builder.inputType
                    if (builder.inputType
                        and InputType.TYPE_TEXT_VARIATION_PASSWORD == InputType.TYPE_TEXT_VARIATION_PASSWORD
                    ) { // If the flags contain TYPE_TEXT_VARIATION_PASSWORD, apply the password transformation method automatically
                        this.transformationMethod = PasswordTransformationMethod.getInstance()
                    }
                }
                dialog.inputMinMaxLabel = rootView.findViewById<TextView?>(R.id.minMax)
                dialog.inputMinMaxLabel?.let { inputMinMax ->
                        if (builder.inputMaxLength != INVALID) {
                            dialog.invalidateInputMinMaxIndicator(
                                this.text.toString().length,
                                !builder.inputAllowEmpty
                            )
                        } else {
                            inputMinMax.visibility = View.GONE
                            dialog.inputMinMaxLabel = null
                        }
                    }
            }
    }

    private fun EditText.setInternalInputCallback() {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                val length: Int = s.toString().length
                var emptyDisabled = false
                if (!builder.inputAllowEmpty) {
                    emptyDisabled = length == 0
                    dialog.getActionButton(DialogAction.POSITIVE)?.isEnabled = !emptyDisabled
                }
                dialog.invalidateInputMinMaxIndicator(length, emptyDisabled)
                if (builder.alwaysCallInputCallback) {
                    builder.inputCallback?.onInput(dialog, s)
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }


    private fun setupCustomView() {
        builder.customView?.let { customView ->
            (rootView.findViewById<View>(R.id.root) as? MdRootLayout?)?.noTitleNoPadding()

            val frame: FrameLayout? = rootView
                .findViewById<FrameLayout>(R.id.customViewFrame)
                ?.also { dialog.customViewFrame = it }

            var innerView: View = customView
            innerView.isFocusable = false

            if (builder.wrapCustomViewInScroll) { /* Apply the frame padding to the content, this allows the ScrollView to draw it's over scroll glow without clipping */
                val framePadding = dialog.context.getDimensionPixelSize(R.dimen.md_dialog_frame_margin)
                val scroll: ViewGroup = if (builder.bottomSheet) {
                    NestedScrollView(
                        ContextThemeWrapper(
                            builder.context,
                            R.style.MaterialDialog_NestedScrollViewStyle
                        )
                    )
                } else {
                    ScrollView(builder.context)
                }
                val paddingTop: Int = dialog.context.getDimensionPixelSize(R.dimen.md_content_padding_top)
                val paddingBottom: Int = dialog.context.getDimensionPixelSize(R.dimen.md_content_padding_bottom)
                scroll.clipToPadding = false
                scroll.isFocusable = false
                if (innerView is EditText) { // Setting padding to an EditText causes visual errors, set it to the parent instead
                    scroll.setPadding(framePadding, paddingTop, framePadding, paddingBottom)
                } else { // Setting padding to scroll view pushes the scroll bars out, don't do it if not necessary (like above)
                    scroll.setPadding(0, paddingTop, 0, paddingBottom)
                    innerView.setPadding(framePadding, 0, framePadding, 0)
                }
                scroll.addView(
                    innerView,
                    FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                )
                innerView = scroll
            }

            frame?.addView(
                innerView,
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            )
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupBottomSheet() {
        if (!builder.bottomSheet) return

        dialog.bottomSheetContainer = (View.inflate(
            dialog.context, R.layout.md_dialog_bottom_sheet, null
        ) as? FrameLayout)
            ?.also { bottomSheetContainer ->
                val coordinator: CoordinatorLayout = bottomSheetContainer.findViewById(R.id.coordinator)
                val bottomSheet: FrameLayout = bottomSheetContainer.findViewById(R.id.design_bottom_sheet)

                val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
                    .also { dialog.bottomSheetBehavior = it }
                bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {

                    override fun onStateChanged(
                        bottomSheet: View, @BottomSheetBehavior.State newState: Int
                    ) {
                        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                            dialog.cancel()
                        }
                    }

                    override fun onSlide(bottomSheet: View, slideOffset: Float) {}
                })
                bottomSheetBehavior.isHideable = builder.cancelable

                bottomSheet.addView(rootView)
                // We treat the CoordinatorLayout as outside the dialog though it is technically inside
                coordinator
                    .findViewById<View?>(R.id.touch_outside)
                    ?.setOnClickListener { _ ->
                        if (builder.cancelable && dialog.isShowing) {
                            dialog.cancel()
                        }
                    }
                // Handle accessibility events
                ViewCompat.setAccessibilityDelegate(
                    bottomSheet,
                    object : AccessibilityDelegateCompat() {
                        override fun onInitializeAccessibilityNodeInfo(
                            host: View,
                            info: AccessibilityNodeInfoCompat
                        ) {
                            super.onInitializeAccessibilityNodeInfo(host, info)
                            if (builder.cancelable) {
                                info.addAction(AccessibilityNodeInfoCompat.ACTION_DISMISS)
                                info.isDismissable = true
                            } else {
                                info.isDismissable = false
                            }
                        }

                        override fun performAccessibilityAction(
                            host: View,
                            action: Int,
                            args: Bundle?
                        ): Boolean {
                            if (action == AccessibilityNodeInfoCompat.ACTION_DISMISS
                                && builder.cancelable
                            ) {
                                dialog.cancel()
                                return true
                            }
                            return super.performAccessibilityAction(host, action, args)
                        }
                    })
                // Consume the event and prevent it from falling through
                bottomSheet.setOnTouchListener { _, _ -> true  }
            }
    }

//--------------------------------------------------------------------------------------------------
//  Selectors
//--------------------------------------------------------------------------------------------------

    private fun getListSelector(): Drawable? =
        if (builder.listSelector != NO_RESOURCE) {
            builder.context.getDrawableCompat(builder.listSelector)
        } else {
            builder.context.resolveDrawable(R.attr.md_list_selector)
                ?: dialog.context.resolveDrawable(R.attr.md_list_selector)
        }

    private fun getButtonSelector(@DialogAction which: Int, isStacked: Boolean): Drawable? =
        if (isStacked) {
            if (builder.btnSelectorStacked != NO_RESOURCE) {
                builder.context.getDrawableCompat(builder.btnSelectorStacked)
            } else {
                builder.context.resolveDrawable(R.attr.md_btn_stacked_selector)
                    ?: dialog.context.resolveDrawable(R.attr.md_btn_stacked_selector)
            }
        } else {
            when (which) {
                DialogAction.NEUTRAL -> {
                    if (builder.btnSelectorNeutral != NO_RESOURCE) {
                        builder.context.getDrawableCompat(builder.btnSelectorNeutral)
                    } else {
                        builder.context.resolveDrawable(R.attr.md_btn_neutral_selector)
                            ?: dialog.context.resolveDrawable(R.attr.md_btn_neutral_selector)
                    }
                }
                DialogAction.NEGATIVE -> {
                    if (builder.btnSelectorNegative != NO_RESOURCE) {
                        builder.context.getDrawableCompat(builder.btnSelectorNegative)
                    } else {
                        builder.context.resolveDrawable(R.attr.md_btn_negative_selector)
                            ?: dialog.context.resolveDrawable(R.attr.md_btn_negative_selector)
                    }
                }
                /* DialogAction.POSITIVE */
                else -> {
                    if (builder.btnSelectorPositive != NO_RESOURCE) {
                        builder.context.getDrawableCompat(builder.btnSelectorPositive)
                    } else {
                        builder.context.resolveDrawable(R.attr.md_btn_positive_selector)
                            ?: dialog.context.resolveDrawable(R.attr.md_btn_positive_selector)
                    }
                }
            }
        }

}