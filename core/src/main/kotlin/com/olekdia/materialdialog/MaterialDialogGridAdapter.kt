package com.olekdia.materialdialog

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.olekdia.materialdialog.MaterialDialog.ItemsChoiceMode

/**
 * @author Oleksandr Albul
 */
internal class MaterialDialogGridAdapter(dialog: MaterialDialog) :
    MaterialDialogBaseAdapter(dialog, dialog.gridItemLayout) {

    override fun newView(parent: ViewGroup?): View {
        val view = inflater.inflate(layoutRes, parent, false)

        val holder = ItemHolder(
            title = view.findViewById(R.id.title),
            imageView = view.findViewById(R.id.icon)
        )

        holder.title.setTextColor(builder.itemColor)
        holder.title.setTypefaceSubpixel(builder.regularFont)
        view.tag = holder
        return view
    }

    @SuppressLint("SwitchIntDef")
    override fun bindView(view: View, index: Int) {
        val holder: ItemHolder = view.tag as ItemHolder

        holder.imageView?.let { v ->
            val drawable: Drawable? = builder.icons?.getOrNull(index)
            v.setImageDrawable(drawable)
        }

        when (dialog.itemsChoiceMode) {
            ItemsChoiceMode.SINGLE -> {
                val selected: Boolean = builder.selectedIndex == index
                view.setBackgroundColor(if (selected) selectedColor else Color.TRANSPARENT)
            }
            ItemsChoiceMode.MULTI -> {
                val selected: Boolean = dialog.selectedIndicesList?.contains(index) == true
                view.setBackgroundColor(if (selected) selectedColor else Color.TRANSPARENT)
            }
        }

        val isEnabled: Boolean = isItemEnabled(index)
        val itemText: CharSequence? = getItem(index)

        holder.title.text = if (!isEnabled && builder.itemsDisabledTail != null) {
            TextUtils.concat(itemText, builder.itemsDisabledTail)
        } else {
            itemText
        }
        view.alpha = if (isEnabled) 1f else 0.8f
    }

    class ItemHolder(
        val title: TextView,
        val imageView: ImageView?
    )
}