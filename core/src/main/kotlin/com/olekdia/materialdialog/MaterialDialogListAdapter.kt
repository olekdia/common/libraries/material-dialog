package com.olekdia.materialdialog

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import com.olekdia.materialdialog.MaterialDialog.DialogGravity
import com.olekdia.materialdialog.MaterialDialog.ItemsChoiceMode

/**
 * @author Oleksandr Albul
 */
@SuppressLint("SwitchIntDef")
internal class MaterialDialogListAdapter(dialog: MaterialDialog) :
    MaterialDialogBaseAdapter(dialog, dialog.listItemLayout) {

    @DialogGravity
    private val itemGravity: Int = builder.itemsGravity
    private val itemIntGravity: Int = itemGravity.fromDialogGravityToGravity()

    override fun newView(parent: ViewGroup?): View {
        val view: View = inflater.inflate(layoutRes, parent, false)
        val holder = ItemHolder(
            view.findViewById(R.id.title),
            view.findViewById(R.id.icon)
        )

        holder.title.setTextColor(builder.itemColor)
        holder.title.setTypefaceSubpixel(builder.regularFont)

        if (holder.imageView == null) {
            when (dialog.itemsChoiceMode) {
                ItemsChoiceMode.SINGLE ->
                    holder.radioButton = view.findViewById<RadioButton?>(R.id.control)
                        ?.apply { this.setTint(builder.widgetColor) }

                ItemsChoiceMode.MULTI ->
                    holder.checkBox = view.findViewById<CheckBox?>(R.id.control)
                        ?.apply { this.setTint(builder.widgetColor) }
            }
        }

        setupGravity(view)

        // Remove circular selector from check boxes and radio buttons on Lollipop
        holder.radioButton?.background = null
        holder.checkBox?.background = null
        view.tag = holder
        return view
    }

    override fun bindView(view: View, index: Int) {
        val holder: ItemHolder = view.tag as ItemHolder

        holder.imageView?.let { v ->
            val drawable: Drawable? = builder.icons?.getOrNull(index)
            v.setImageDrawable(drawable)
        }

        when (dialog.itemsChoiceMode) {
            ItemsChoiceMode.SINGLE -> {
                val selected: Boolean = builder.selectedIndex == index
                if (holder.imageView != null) {
                    view.setBackgroundColor(if (selected) selectedColor else Color.TRANSPARENT)
                } else {
                    holder.radioButton?.isChecked = selected
                }
            }
            ItemsChoiceMode.MULTI -> {
                val selected: Boolean = dialog.selectedIndicesList?.contains(index) == true
                if (holder.imageView != null) {
                    view.setBackgroundColor(if (selected) selectedColor else Color.TRANSPARENT)
                } else {
                    holder.checkBox?.isChecked = selected
                }
            }
        }
        val isEnabled: Boolean = isItemEnabled(index)
        val itemText: CharSequence? = getItem(index)

        holder.title.text = if (!isEnabled && builder.itemsDisabledTail != null) {
            TextUtils.concat(itemText, builder.itemsDisabledTail)
        } else {
            itemText
        }
        view.alpha = if (isEnabled) 1f else 0.8f
    }

    private inline fun setupGravity(view: View) {
        if (itemGravity != DialogGravity.START) {
            (view as LinearLayout).gravity = itemIntGravity or Gravity.CENTER_VERTICAL
        }
    }

    class ItemHolder (
        val title: TextView,
        val imageView: ImageView?
    ) {
        var radioButton: RadioButton? = null
        var checkBox: CheckBox?  = null
    }
}