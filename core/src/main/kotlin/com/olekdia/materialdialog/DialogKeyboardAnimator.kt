package com.olekdia.materialdialog

import android.os.Build
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.*
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updateLayoutParams
import androidx.core.view.updateMargins
import kotlin.math.max

class DialogKeyboardAnimator(private val window: Window) {

    private val sceneRoot: ViewGroup? by lazy(LazyThreadSafetyMode.NONE) {
        window.decorView.findViewById<View>(Window.ID_ANDROID_CONTENT)?.parent as? ViewGroup
    }

    /**
     * Method to start animating the keyboard appearing. Calling this method sets [insetsListener] as
     * [View.OnApplyWindowInsetsListener] to the [Window.getDecorView].
     */
    fun start() {
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)

//            window.decorView.setOnApplyWindowInsetsListener { view, insets ->
//                insets
//            }

            // Sometimes when open new window that hides keyboard,
            // the previous window is not adjusted and not notified that keyboard is hidden with onProgress callback.
            // And even onEnd callback not fired every time, so that's why here duplicated code in two sections.
            window.decorView.findViewById<View>(android.R.id.content).let { view ->
                var isFirstTime = true
                view.setOnApplyWindowInsetsListener { view, insets ->
                    if (isFirstTime) {
                        view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                            val sysBarInsets = insets.getInsets(WindowInsets.Type.systemBars())

                            updateMargins(
                                top = sysBarInsets.top,
                                bottom = sysBarInsets.bottom
                            )
                        }
                        isFirstTime = false
                    } else {
                        val showingKeyboard =
                            view.rootWindowInsets?.isVisible(WindowInsets.Type.ime()) == true
                        if (!showingKeyboard) {
                            view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                                val sysBarInsets = insets.getInsets(WindowInsets.Type.systemBars())

                                updateMargins(
                                    top = sysBarInsets.top,
                                    bottom = sysBarInsets.bottom
                                )
                            }
                        }
                    }

                    WindowInsets.CONSUMED
                }

                // It is weired. When ime hidden Type.systemBars() shows correct inset from the bottom, but when ime is shown,
                // Type.ime() shows full inset from the bottom including Type.systemBars().
                // Nevertheless when ime hides, Type.ime() shows 0 inset, so Type.systemBars() excluded by that time

                view.setWindowInsetsAnimationCallback(
                    object : WindowInsetsAnimation.Callback(DISPATCH_MODE_STOP) {

                        override fun onProgress(
                            insets: WindowInsets,
                            animations: MutableList<WindowInsetsAnimation>
                        ): WindowInsets {
                            view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                                updateMargins(
                                    bottom = max(
                                        insets.getInsets(WindowInsetsCompat.Type.ime()).bottom,
                                        insets.getInsets(WindowInsetsCompat.Type.systemBars()).bottom
                                    )
                                )
                            }

                            return WindowInsets.CONSUMED
                        }

                        override fun onEnd(animation: WindowInsetsAnimation) {
                            super.onEnd(animation)
                            val showingKeyboard = view.rootWindowInsets?.isVisible(WindowInsets.Type.ime()) == true
                            if (!showingKeyboard) {
                                view.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                                    view.rootWindowInsets?.getInsets(WindowInsets.Type.systemBars())?.let { sysBarInsets ->
                                        updateMargins(
                                            top = sysBarInsets.top,
                                            bottom = sysBarInsets.bottom
                                        )
                                    }
                                }
                            }
                        }
                    }
                )
            }
        } else*/ if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            window.decorView.setOnApplyWindowInsetsListener { view, insets ->
                // This animator starts delayed [ChangeBounds] transition before system get chance to apply insets.
                sceneRoot?.let { TransitionManager.beginDelayedTransition(it, ChangeBounds()) }
                view.onApplyWindowInsets(insets)
            }
        }
    }
}