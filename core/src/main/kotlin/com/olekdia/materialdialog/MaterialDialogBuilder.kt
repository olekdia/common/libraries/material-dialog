package com.olekdia.materialdialog

import android.content.Context
import android.content.DialogInterface.*
import android.content.res.ColorStateList
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.BaseAdapter
import androidx.annotation.*
import com.olekdia.androidcommon.NO_RESOURCE
import com.olekdia.androidcommon.extensions.*
import com.olekdia.common.INVALID
import com.olekdia.common.NumeralSystem
import com.olekdia.common.NumeralSystem.Companion.WESTERN_ARABIC
import com.olekdia.common.extensions.isDarkColor
import com.olekdia.materialdialog.MaterialDialog.*
import java.text.NumberFormat

@Suppress("unused", "WeakerAccess", "UnusedReturnValue")
open class MaterialDialogBuilder(@JvmField val context: Context) {
    @JvmField
    internal var bottomSheet = false
    @JvmField
    internal var bottomSheetForceExpand = false
    @JvmField
    internal var numeralSystem = WESTERN_ARABIC
    var title: CharSequence? = null
        internal set
    @JvmField
    @DialogGravity
    internal var titleGravity = DialogGravity.START
    @JvmField
    @DialogGravity
    internal var contentGravity = DialogGravity.START
    @JvmField
    @DialogGravity
    internal var btnStackedGravity = DialogGravity.END
    @JvmField
    @DialogGravity
    internal var itemsGravity = DialogGravity.START
    @JvmField
    @DialogGravity
    internal var buttonsGravity: Int
    var titleColor: Int = 0
        internal set
    @JvmField
    internal var titleColorSet = false
    var contentColor: Int = 0
        internal set
    @JvmField
    internal var contentColorSet = false
    @JvmField
    internal var content: CharSequence? = null
    @JvmField
    internal var items: Array<CharSequence>? = null
    @JvmField
    internal var icons: Array<Drawable>? = null
    @JvmField
    internal var itemsDisabledPositions: IntArray? = null
    @JvmField
    internal var itemsDisabledTail: CharSequence? = null
    @JvmField
    internal var positiveText: CharSequence? = null
    @JvmField
    internal var neutralText: CharSequence? = null
    @JvmField
    internal var negativeText: CharSequence? = null
    var customView: View? = null
        internal set
    var widgetColor: Int
        internal set
    @JvmField
    internal var widgetColorSet = false
    @JvmField
    internal var positiveColor: ColorStateList
    @JvmField
    internal var positiveColorSet = false
    @JvmField
    internal var negativeColor: ColorStateList
    @JvmField
    internal var negativeColorSet = false
    @JvmField
    internal var neutralColor: ColorStateList
    @JvmField
    internal var neutralColorSet = false
    @JvmField
    internal var callback: IButtonCallback? = null
    @JvmField
    internal var itemsCallback: ItemsCallback? = null
    @JvmField
    internal var itemsCallbackSingleChoice: ItemsCallbackSingleChoice? = null
    @JvmField
    internal var itemsCallbackMultiChoice: ItemsCallbackMultiChoice? = null
    @JvmField
    internal var itemsCallbackCustom: ItemsCallback? = null
    @JvmField
    internal var alwaysCallMultiChoiceCallback = false
    @JvmField
    internal var alwaysCallSingleChoiceCallback = false
    var isDarkTheme: Boolean = false
        internal set
    @JvmField
    internal var cancelable = true
    @JvmField
    internal var contentLineSpacingMultiplier = 1.2f
    @JvmField
    internal var selectedIndex = INVALID
    @JvmField
    internal var selectedIndices: IntArray? = null
    @JvmField
    internal var autoDismiss = true
    var regularFont: Typeface? = null
        private set
    var mediumFont: Typeface? = null
        private set
    @JvmField
    internal var icon: Drawable? = null
    @JvmField
    internal var limitIconToDefaultSize = false
    @JvmField
    internal var maxIconSize = INVALID
    @JvmField
    internal var adapter: BaseAdapter? = null
    @JvmField
    internal var dismissListener: OnDismissListener? = null
    @JvmField
    internal var cancelListener: OnCancelListener? = null
    @JvmField
    internal var stopListener: OnStopListener? = null
    @JvmField
    internal var keyListener: OnKeyListener? = null
    @JvmField
    internal var showListener: OnShowListener? = null
    @JvmField
    internal var forceStacking = false
    var wrapCustomViewInScroll = false
        internal set
    var dividerColor = 0
        internal set
    @JvmField
    internal var dividerColorSet = false
    var backgroundColor = 0
        internal set
    @JvmField
    internal var backgroundColorSet = false
    var itemColor = 0
        internal set
    @JvmField
    internal var itemColorSet = false
    @JvmField
    internal var indeterminateProgress = false
    @JvmField
    internal var showMinMax = false
    @JvmField
    internal var progress = -2
    @JvmField
    internal var progressMax = 0
    @JvmField
    @ItemsLayoutType
    internal var itemsLayoutType = ItemsLayoutType.LIST
    @JvmField
    internal var inputPrefill: CharSequence? = null
    @JvmField
    internal var inputHint: CharSequence? = null
    @JvmField
    internal var inputCallback: InputCallback? = null
    @JvmField
    internal var inputAllowEmpty = false
    @JvmField
    internal var inputType = INVALID
    @JvmField
    internal var alwaysCallInputCallback = false
    @JvmField
    internal var inputMaxLength = INVALID
    @JvmField
    internal var inputMaxLengthErrorColor = 0

    @KeyboardMode
    var keyboardMode = KeyboardMode.UNCHANGED
        internal set
    @JvmField
    internal var progressNumberFormat: String
    @JvmField
    internal var progressPercentFormat: NumberFormat? = null
    @JvmField
    @ProgressStyle
    internal var progressStyle = 0
    @DrawableRes
    var listSelector = 0
        internal set
    @JvmField
    @DrawableRes
    internal var btnSelectorStacked = 0
    @JvmField
    @DrawableRes
    internal var btnSelectorPositive = 0
    @JvmField
    @DrawableRes
    internal var btnSelectorNeutral = 0
    @JvmField
    @DrawableRes
    internal var btnSelectorNegative = 0
    @JvmField
    @StyleRes
    internal var dialogStyleRes = NO_RESOURCE

    init {
        buttonsGravity = if (context.resources.isLayoutRtl()) DialogGravity.END else DialogGravity.START
        val widgetColorFallback: Int = context.getColorCompat(R.color.md_teal)
        // Retrieve default accent colors, which are used on the action buttons and progress bars
        widgetColor = context.resolveColor(androidx.appcompat.R.attr.colorAccent, widgetColorFallback)
        positiveColor = context.getActionTextStateList(widgetColor)
        negativeColor = context.getActionTextStateList(widgetColor)
        neutralColor = context.getActionTextStateList(widgetColor)
        progressNumberFormat = "%1d/%2d"
        // Set the default theme based on the Activity theme's primary color darkness (more white or more black)
        val primaryTextColor: Int = context.resolveColor(android.R.attr.textColorPrimary)
        if (!isDarkTheme) {
            isDarkTheme = !primaryTextColor.isDarkColor()
        }

        // Retrieve gravity settings from global theme attributes if needed
        titleGravity = context.resolveDialogGravity(R.attr.md_title_gravity, titleGravity)
        contentGravity = context.resolveDialogGravity(R.attr.md_content_gravity, contentGravity)
        btnStackedGravity = context.resolveDialogGravity(R.attr.md_btnstacked_gravity, DialogGravity.END)
        itemsGravity = context.resolveDialogGravity(R.attr.md_items_gravity, itemsGravity)
        buttonsGravity = context.resolveDialogGravity(R.attr.md_buttons_gravity, buttonsGravity)

        val mediumFont: String? = context.resolveString(R.attr.md_medium_font)
        val regularFont: String? = context.resolveString(R.attr.md_regular_font)
        typeface(mediumFont, regularFont)
        if (this.mediumFont == null) {
            try {
                this.mediumFont = context.getTypeface(SANS_SERIF_MEDIUM_FONT, Typeface.NORMAL)
            } catch (ignored: Exception) {
            }
        }
        if (this.regularFont == null) {
            try {
                this.regularFont = context.getTypeface(SANS_SERIF_FONT, Typeface.NORMAL)
            } catch (ignored: Exception) {
            }
        }
    }

    fun bottomSheet(bottomSheet: Boolean): MaterialDialogBuilder = this.also {
        it.bottomSheet = bottomSheet
    }

    fun bottomSheet(
        bottomSheet: Boolean,
        forceExpand: Boolean
    ): MaterialDialogBuilder {
        if (!isBottomSheetListSupported()
            && (items != null || adapter != null)
        ) {
            this.bottomSheet = false
            this.bottomSheetForceExpand = false
        } else {
            this.bottomSheet = bottomSheet
            this.bottomSheetForceExpand = forceExpand
        }
        return this
    }

    open fun numeralSystem(@NumeralSystem numeralSystem: Int) = this.apply {
        this.numeralSystem = numeralSystem
    }

    fun title(@StringRes titleRes: Int): MaterialDialogBuilder = this.apply {
        title(context.getText(titleRes))
    }

    fun title(title: CharSequence): MaterialDialogBuilder = this.apply {
        this.title = title
    }

    fun titleGravity(@DialogGravity gravity: Int): MaterialDialogBuilder = this.apply {
        this.titleGravity = gravity
    }

    open fun titleColor(@ColorInt color: Int): MaterialDialogBuilder = this.apply {
        this.titleColor = color
        this.titleColorSet = true
    }

    fun titleColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder = this.apply {
        titleColor(context.getColorCompat(colorRes))
    }

    fun titleColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder = this.apply {
        titleColor(context.resolveColor(colorAttr))
    }

    /**
     * Sets the fonts used in the dialog. It's recommended that you use [.typeface] instead,
     * to avoid duplicate Typeface allocations and high memory usage.
     *
     * @param medium  The font used on titles and action buttons. Null uses device default.
     * @param regular The font used everywhere else, like on the content and list items. Null uses device default.
     * @return The Builder instance so you can chain calls to it.
     */
    fun typeface(medium: Typeface?, regular: Typeface?): MaterialDialogBuilder = this.apply {
        this.mediumFont = medium
        this.regularFont = regular
    }

    /**
     * Sets the fonts used in the dialog, by file names. This also uses TypefaceHelper in order
     * to avoid any un-needed allocations (it recycles typefaces for you).
     *
     * @param medium  The name of font in assets/fonts used on titles and action buttons (null uses device default). E.g. [your-project]/app/main/assets/fonts/[medium]
     * @param regular The name of font in assets/fonts used everywhere else, like content and list items (null uses device default). E.g. [your-project]/app/main/assets/fonts/[regular]
     * @return The Builder instance so you can chain calls to it.
     */
    fun typeface(medium: String?, regular: String?): MaterialDialogBuilder = this.apply {
        if (medium?.isNotEmpty() == true) {
            this.mediumFont = context.getTypeface(medium)
        }
        if (regular?.isNotEmpty() == true) {
            this.regularFont = context.getTypeface(regular)
        }
    }

    fun icon(icon: Drawable?): MaterialDialogBuilder = this.apply {
        this.icon = icon
    }

    fun iconRes(@DrawableRes iconRes: Int): MaterialDialogBuilder = this.apply {
        this.icon = context.getDrawableCompat(iconRes)
    }

    fun iconAttr(@AttrRes iconAttr: Int): MaterialDialogBuilder = this.apply {
        this.icon = context.resolveDrawable(iconAttr)
    }

    fun content(@StringRes contentRes: Int): MaterialDialogBuilder = this.apply {
        content(context.getText(contentRes))
    }

    fun content(content: CharSequence): MaterialDialogBuilder = this.apply {
        check(customView == null) { "You cannot set content() when you're using a custom view." }
        this.content = content
    }

    fun content(@StringRes contentRes: Int, vararg formatArgs: Any?): MaterialDialogBuilder =
        this.apply {
            content(context.getString(contentRes, *formatArgs))
        }

    open fun contentColor(@ColorInt color: Int): MaterialDialogBuilder = this.apply {
        this.contentColor = color
        this.contentColorSet = true
    }

    fun contentColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder = this.apply {
        contentColor(context.getColorCompat(colorRes))
    }

    fun contentColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder = this.apply {
        contentColor(context.resolveColor(colorAttr))
    }

    fun contentGravity(@DialogGravity gravity: Int): MaterialDialogBuilder = this.apply {
        this.contentGravity = gravity
    }

    fun contentLineSpacing(multiplier: Float): MaterialDialogBuilder = this.apply {
        this.contentLineSpacingMultiplier = multiplier
    }

    fun items(@ArrayRes itemsRes: Int): MaterialDialogBuilder = this.apply {
        items(context.resources.getTextArray(itemsRes))
    }

    fun itemsDisabledPositions(disabledPositions: IntArray?): MaterialDialogBuilder = this.apply {
        this.itemsDisabledPositions = disabledPositions
    }

    fun itemsDisabledTail(disabledTail: CharSequence?): MaterialDialogBuilder = this.apply {
        this.itemsDisabledTail = if (disabledTail != null) {
            if (disabledTail.startsWith(' ')) {
                disabledTail
            } else {
                TextUtils.concat(" ", disabledTail)
            }
        } else {
            null
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun items(items: Array<String>): MaterialDialogBuilder = this.apply {
        this.items(items as Array<CharSequence>)
    }

    fun items(items: Array<CharSequence>): MaterialDialogBuilder = this.apply {
        check(customView == null) { "You cannot set items() when you're using a custom view." }
        if (!isBottomSheetListSupported()
            && bottomSheet
        ) {
            this.bottomSheet(false, false)
        }
        this.items = items
    }

    fun itemsIcons(icons: Array<Drawable>?): MaterialDialogBuilder = this.apply {
        check(customView == null) { "You cannot set itemsIcons() when you're using a custom view." }
        this.icons = icons
    }

    fun itemsLayoutType(type: Int): MaterialDialogBuilder = this.apply {
        this.itemsLayoutType = type
    }

    fun items(
        items: Array<CharSequence>,
        disabledPositions: IntArray?,
        disabledTail: CharSequence?
    ): MaterialDialogBuilder = this.apply {
        items(items)
        this.itemsDisabledPositions = disabledPositions
        itemsDisabledTail(disabledTail)
    }

    fun itemsCallback(callback: ItemsCallback): MaterialDialogBuilder = this.apply {
        this.itemsCallback = callback
        this.itemsCallbackSingleChoice = null
        this.itemsCallbackMultiChoice = null
    }

    fun itemColor(@ColorInt color: Int): MaterialDialogBuilder = this.apply {
        this.itemColor = color
        this.itemColorSet = true
    }

    fun itemColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder =
        itemColor(context.getColorCompat(colorRes))

    fun itemColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder =
        itemColor(context.resolveColor(colorAttr))

    fun itemsGravity(@DialogGravity gravity: Int): MaterialDialogBuilder = this.apply {
        this.itemsGravity = gravity
    }

    fun buttonsGravity(@DialogGravity gravity: Int): MaterialDialogBuilder = this.apply {
        this.buttonsGravity = gravity
    }

    /**
     * Pass anything below 0 (such as -1) for the selected index to leave all options unselected initially.
     * Otherwise pass the index of an item that will be selected initially.
     *
     * @param selectedIndex The checkbox index that will be selected initially.
     * @param callback      The callback that will be called when the presses the positive button.
     * @return The Builder instance so you can chain calls to it.
     */
    fun itemsCallbackSingleChoice(
        selectedIndex: Int,
        callback: ItemsCallbackSingleChoice
    ): MaterialDialogBuilder = this.apply {
        this.selectedIndex = selectedIndex
        this.itemsCallback = null
        this.itemsCallbackSingleChoice = callback
        this.itemsCallbackMultiChoice = null
    }

    /**
     * By default, the single choice callback is only called when the user clicks the positive button
     * or if there are no buttons. Call this to force it to always call on item clicks even if the
     * positive button exists.
     *
     * @return The Builder instance so you can chain calls to it.
     */
    fun alwaysCallSingleChoiceCallback(): MaterialDialogBuilder = this.apply {
        this.alwaysCallSingleChoiceCallback = true
    }

    /**
     * Pass null for the selected indices to leave all options unselected initially. Otherwise pass
     * an array of indices that will be selected initially.
     *
     * @param selectedIndices The radio button indices that will be selected initially.
     * @param callback        The callback that will be called when the presses the positive button.
     * @return The Builder instance so you can chain calls to it.
     */
    fun itemsCallbackMultiChoice(
        selectedIndices: IntArray?,
        callback: ItemsCallbackMultiChoice
    ): MaterialDialogBuilder = this.apply {
        this.selectedIndices = selectedIndices
        this.itemsCallback = null
        this.itemsCallbackSingleChoice = null
        this.itemsCallbackMultiChoice = callback
    }

    /**
     * By default, the multi choice callback is only called when the user clicks the positive button
     * or if there are no buttons. Call this to force it to always call on item clicks even if the
     * positive button exists.
     *
     * @return The Builder instance so you can chain calls to it.
     */
    fun alwaysCallMultiChoiceCallback(): MaterialDialogBuilder = this.apply {
        this.alwaysCallMultiChoiceCallback = true
    }

    fun positiveText(@StringRes postiveRes: Int): MaterialDialogBuilder = this.apply {
        positiveText(context.getText(postiveRes))
    }

    fun positiveText(message: CharSequence): MaterialDialogBuilder = this.apply {
        this.positiveText = message
    }

    fun positiveColor(@ColorInt color: Int): MaterialDialogBuilder =
        positiveColor(context.getActionTextStateList(color))

    fun positiveColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder =
        positiveColor(context.getActionTextColorStateList(colorRes))

    fun positiveColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder =
        positiveColor(context.resolveActionTextColorStateList(colorAttr, this.positiveColor))

    fun positiveColor(colorStateList: ColorStateList): MaterialDialogBuilder = this.apply {
        this.positiveColor = colorStateList
        this.positiveColorSet = true
    }

    fun neutralText(@StringRes neutralRes: Int): MaterialDialogBuilder =
        neutralText(context.getText(neutralRes))

    fun neutralText(message: CharSequence): MaterialDialogBuilder = this.apply {
        this.neutralText = message
    }

    fun negativeColor(@ColorInt color: Int): MaterialDialogBuilder =
        negativeColor(context.getActionTextStateList(color))

    fun negativeColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder =
        negativeColor(context.getActionTextColorStateList(colorRes))

    fun negativeColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder =
        negativeColor(context.resolveActionTextColorStateList(colorAttr, this.negativeColor))

    fun negativeColor(colorStateList: ColorStateList): MaterialDialogBuilder = this.apply {
        this.negativeColor = colorStateList
        this.negativeColorSet = true
    }

    fun negativeText(@StringRes negativeRes: Int): MaterialDialogBuilder =
        negativeText(context.getText(negativeRes))

    fun negativeText(message: CharSequence): MaterialDialogBuilder = this.apply {
        this.negativeText = message
    }

    fun neutralColor(@ColorInt color: Int): MaterialDialogBuilder =
        neutralColor(context.getActionTextStateList(color))

    fun neutralColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder =
        neutralColor(context.getActionTextColorStateList(colorRes))

    fun neutralColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder =
        neutralColor(context.resolveActionTextColorStateList(colorAttr, this.neutralColor))

    fun neutralColor(colorStateList: ColorStateList): MaterialDialogBuilder = this.apply {
        this.neutralColor = colorStateList
        this.neutralColorSet = true
    }

    fun listSelector(@DrawableRes selectorRes: Int): MaterialDialogBuilder = this.apply {
        this.listSelector = selectorRes
    }

    fun btnSelectorStacked(@DrawableRes selectorRes: Int): MaterialDialogBuilder = this.apply {
        this.btnSelectorStacked = selectorRes
    }

    fun btnSelector(@DrawableRes selectorRes: Int): MaterialDialogBuilder = this.apply {
        this.btnSelectorPositive = selectorRes
        this.btnSelectorNeutral = selectorRes
        this.btnSelectorNegative = selectorRes
    }

    fun btnSelector(
        @DrawableRes selectorRes: Int,
        @DialogAction which: Int
    ): MaterialDialogBuilder = this.apply {
        when (which) {
            DialogAction.NEUTRAL -> this.btnSelectorNeutral = selectorRes
            DialogAction.NEGATIVE -> this.btnSelectorNegative = selectorRes
            else -> this.btnSelectorPositive = selectorRes
        }
    }

    /**
     * Sets the gravity used for the text in stacked action buttons.
     *
     * @param gravity The gravity to use.
     * @return The Builder instance so calls can be chained.
     */
    fun btnStackedGravity(@DialogGravity gravity: Int): MaterialDialogBuilder = this.apply {
        this.btnStackedGravity = gravity
    }

    open fun customView(
        @LayoutRes layoutRes: Int,
        wrapInScrollView: Boolean
    ): MaterialDialogBuilder =
        customView(
            LayoutInflater.from(context).inflate(layoutRes, null),
            wrapInScrollView
        )

    open fun customView(
        view: View,
        wrapInScrollView: Boolean
    ): MaterialDialogBuilder = this.apply {
        check(content == null) { "You cannot use customView() when you have content set." }
        check(items == null) { "You cannot use customView() when you have items set." }
        view.parent?.let { parent ->
            if (parent is ViewGroup) parent.removeView(view)
        }
        this.customView = view
        this.wrapCustomViewInScroll = wrapInScrollView
    }

    /**
     * Makes this dialog a progress dialog.
     * Progress bar is shown that is incremented or set via the built MaterialDialog instance.
     * @param max           The max value the horizontal progress bar can get to.
     * @param showMinMax    For determinate dialogs, the min and max will be displayed to the left (start) of the progress bar, e.g. 50/100.
     * @return An instance of the Builder so calls can be chained.
     */
    fun progress(
        max: Int,
        showMinMax: Boolean
    ): MaterialDialogBuilder = this.apply {
        check(customView == null) { "You cannot set progress() when you're using a custom view." }
        this.indeterminateProgress = false
        this.progressMax = max
        this.showMinMax = showMinMax
        this.progress = -1
        this.progressPercentFormat = this.progressPercentFormat ?: NumberFormat.getPercentInstance()
    }

    /**
     * hange the format of the small text showing current and maximum units of progress.
     * The default is "%1d/%2d".
     */
    fun progressNumberFormat(format: String): MaterialDialogBuilder = this.apply {
        this.progressNumberFormat = format
    }

    /**
     * Change the format of the small text showing the percentage of progress.
     * The default is NumberFormat.getPercentageInstance().
     */
    fun progressPercentFormat(format: NumberFormat): MaterialDialogBuilder = this.apply {
        this.progressPercentFormat = format
    }

    /**
     * By default, indeterminate progress dialogs will use a circular indicator. You
     * can change it to use a isHorizontalStyle progress indicator.
     */
    fun progressIndeterminate(
        @ProgressStyle progressStyle: Int
    ): MaterialDialogBuilder = this.apply {
        this.indeterminateProgress = true
        this.progressStyle = progressStyle
        this.progress = -2
    }

    open fun widgetColor(@ColorInt color: Int): MaterialDialogBuilder = this.apply {
        this.widgetColor = color
        this.widgetColorSet = true
    }

    fun widgetColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder =
        widgetColor(context.getColorCompat(colorRes))

    fun widgetColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder =
        widgetColorRes(context.resolveColor(colorAttr))

    fun dividerColor(@ColorInt color: Int): MaterialDialogBuilder = this.apply {
        this.dividerColor = color
        this.dividerColorSet = true
    }

    fun dividerColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder =
        dividerColor(context.getColorCompat(colorRes))

    fun dividerColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder =
        dividerColor(context.resolveColor(colorAttr))

    open fun backgroundColor(@ColorInt color: Int): MaterialDialogBuilder = this.apply {
        this.backgroundColor = color
        this.backgroundColorSet = true
    }

    fun backgroundColorRes(@ColorRes colorRes: Int): MaterialDialogBuilder =
        backgroundColor(context.getColorCompat(colorRes))

    fun backgroundColorAttr(@AttrRes colorAttr: Int): MaterialDialogBuilder =
        backgroundColor(context.resolveColor(colorAttr))

    fun callback(callback: IButtonCallback): MaterialDialogBuilder = this.apply {
        this.callback = callback
    }

    open fun isDarkTheme(isDarkTheme: Boolean): MaterialDialogBuilder = this.apply {
        this.isDarkTheme = isDarkTheme
    }

    fun dialogStyleRes(@StyleRes styleRes: Int): MaterialDialogBuilder = this.apply {
        this.dialogStyleRes = styleRes
    }

    fun cancelable(cancelable: Boolean): MaterialDialogBuilder = this.apply {
        this.cancelable = cancelable
    }

    /**
     * This defaults to true. If set to false, the dialog will not automatically be dismissed
     * when an action button is pressed, and not automatically dismissed when the user selects
     * a list item.
     *
     * @param dismiss Whether or not to dismiss the dialog automatically.
     * @return The Builder instance so you can chain calls to it.
     */
    fun autoDismiss(dismiss: Boolean): MaterialDialogBuilder = this.apply {
        this.autoDismiss = dismiss
    }

    /**
     * Sets a custom [android.widget.ListAdapter] for the dialog's list
     *
     * @param adapter  The adapter to set to the list.
     * @param callback The callback invoked when an item in the list is selected.
     * @return This Builder object to allow for chaining of calls to set methods
     */
    fun adapter(
        adapter: BaseAdapter,
        callback: ItemsCallback?
    ): MaterialDialogBuilder = this.apply {
        check(customView == null) { "You cannot set adapter() when you're using a custom view." }
        if (!isBottomSheetListSupported()
            && bottomSheet
        ) {
            this.bottomSheet(false, false)
        }
        this.adapter = adapter
        this.itemsCallbackCustom = callback
    }

    /**
     * Limits the display size of a set mIcon to 48dp.
     */
    fun limitIconToDefaultSize(): MaterialDialogBuilder = this.apply {
        this.limitIconToDefaultSize = true
    }

    fun maxIconSize(maxIconSize: Int): MaterialDialogBuilder = this.apply {
        this.maxIconSize = maxIconSize
    }

    fun maxIconSizeRes(@DimenRes maxIconSizeRes: Int): MaterialDialogBuilder =
        maxIconSize(context.resources.getDimension(maxIconSizeRes).toInt())

    fun setShowListener(listener: OnShowListener): MaterialDialogBuilder = this.apply {
        this.showListener = listener
    }

    fun setDismissListener(listener: OnDismissListener): MaterialDialogBuilder =
        this.apply {
            this.dismissListener = listener
        }

    fun setStopListener(listener: OnStopListener): MaterialDialogBuilder = this.apply {
        this.stopListener = listener
    }

    fun setCancelListener(listener: OnCancelListener): MaterialDialogBuilder = this.apply {
        this.cancelListener = listener
    }

    fun setKeyListener(listener: OnKeyListener): MaterialDialogBuilder = this.apply {
        this.keyListener = listener
    }

    fun forceStacking(stacked: Boolean): MaterialDialogBuilder = this.apply {
        this.forceStacking = stacked
    }

    fun input(
        hint: CharSequence?,
        prefill: CharSequence?,
        allowEmptyInput: Boolean,
        callback: InputCallback
    ): MaterialDialogBuilder = this.apply {
        check(customView == null) { "You cannot set content() when you're using a custom view." }
        this.inputCallback = callback
        this.inputHint = hint
        this.inputPrefill = prefill
        this.inputAllowEmpty = allowEmptyInput
    }

    fun input(
        hint: CharSequence?,
        prefill: CharSequence?,
        callback: InputCallback
    ): MaterialDialogBuilder = input(hint, prefill, true, callback)

    fun input(
        @StringRes hint: Int,
        @StringRes prefill: Int,
        allowEmptyInput: Boolean,
        callback: InputCallback
    ): MaterialDialogBuilder = input(
        if (hint == NO_RESOURCE) null else context.getText(hint),
        if (prefill == NO_RESOURCE) null else context.getText(prefill),
        allowEmptyInput,
        callback
    )

    fun input(
        @StringRes hint: Int,
        @StringRes prefill: Int,
        callback: InputCallback
    ): MaterialDialogBuilder = input(hint, prefill, true, callback)

    fun inputType(type: Int): MaterialDialogBuilder = this.apply {
        this.inputType = type
    }

    /**
     * @param errorColor Pass in 0 for the default red error color (as specified in guidelines).
     */
    @JvmOverloads
    fun inputMaxLength(maxLength: Int, errorColor: Int = NO_RESOURCE): MaterialDialogBuilder =
        this.apply {
            require(maxLength >= 1) { "Max length for input dialogs cannot be less than 1." }
            this.inputMaxLength = maxLength
            this.inputMaxLengthErrorColor = if (errorColor == NO_RESOURCE) {
                context.getColorCompat(R.color.md_edittext_error)
            } else {
                errorColor
            }
        }

    fun keyboardMode(@KeyboardMode mode: Int): MaterialDialogBuilder = this.apply {
        this.keyboardMode = mode
    }

    /**
     * Same as #[.inputMaxLength], but it takes a color resource ID for the error color.
     */
    fun inputMaxLengthRes(maxLength: Int, @ColorRes errorColor: Int): MaterialDialogBuilder =
        inputMaxLength(maxLength, context.getColorCompat(errorColor))

    fun alwaysCallInputCallback(): MaterialDialogBuilder = this.apply {
        this.alwaysCallInputCallback = true
    }

    protected open fun invalidateColors() {
        if (!backgroundColorSet) {
            val backgroundColorFallback: Int = context.resolveColor(android.R.attr.colorBackground)
            backgroundColor(context.resolveColor(R.attr.md_background_color, backgroundColorFallback))
        }
        // Retrieve action button colors from theme attributes or the Builder
        if (!positiveColorSet) {
            positiveColor(
                context.resolveActionTextColorStateList(
                    R.attr.md_positive_color, positiveColor
                )
            )
        }
        if (!neutralColorSet) {
            neutralColor(
                context.resolveActionTextColorStateList(
                    R.attr.md_neutral_color, neutralColor
                )
            )
        }
        if (!negativeColorSet) {
            negativeColor(
                context.resolveActionTextColorStateList(
                    R.attr.md_negative_color, negativeColor
                )
            )
        }
        if (!widgetColorSet) {
            widgetColor(
                context.resolveColor(R.attr.md_widget_color, widgetColor)
            )
        }

        // Retrieve default title/content colors
        if (!titleColorSet) {
            val titleColorFallback: Int = context.resolveColor(android.R.attr.textColorPrimary)
            titleColor(
                context.resolveColor(
                    R.attr.md_title_color, titleColorFallback
                )
            )
        }
        if (!contentColorSet) {
            val contentColorFallback: Int = context.resolveColor(android.R.attr.textColorSecondary)
            contentColor(
                context.resolveColor(
                    R.attr.md_content_color, contentColorFallback
                )
            )
        }
        if (!itemColorSet) {
            val contentColorFallback: Int = context.resolveColor(android.R.attr.textColorSecondary)
            itemColor(context.resolveColor(R.attr.md_item_color, contentColorFallback))
        }
    }

    @UiThread
    open fun build(): MaterialDialog {
        invalidateColors()

        return MaterialDialog(this).also {
            applyKeyboardMode(it)
        }
    }

    protected fun applyKeyboardMode(dialog: MaterialDialog) {
        dialog.window?.let { win ->
            if (bottomSheet) {
                DialogKeyboardAnimator(win).start()
            }
            if (keyboardMode == KeyboardMode.HIDDEN) {
                win.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            }
        }
    }

    @UiThread
    open fun show(): MaterialDialog =
        build().also {
            it.show()
        }
}