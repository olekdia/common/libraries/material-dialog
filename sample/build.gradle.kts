plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdk = Versions.Sdk.compile
    buildToolsVersion = Versions.buildTools
    namespace = "com.olekdia.sample"

    defaultConfig {
        minSdk = Versions.Sdk.min
        targetSdk = Versions.Sdk.target
        applicationId = "com.olekdia.sample"
        versionCode = 1
        versionName = "1.0.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    testOptions.unitTests.isIncludeAndroidResources = true

    compileOptions {
        encoding = "UTF-8"
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(kotlin("stdlib", Versions.kotlin))
    implementation(Libs.olekdia.commonJvm)
    implementation(Libs.olekdia.commonAndroid)
    implementation(project(":core"))

    implementation(Libs.Androidx.annotations)
    implementation(Libs.Androidx.fragment)
    implementation(Libs.Androidx.appcompat)
    implementation(Libs.Androidx.material)

    implementation(Libs.Androidx.fragmentTesting)

    testImplementation(Libs.junit)
    testImplementation(Libs.robolectric)
    testImplementation(Libs.Androidx.testCore)
    testImplementation(Libs.Androidx.testRunner)
    testImplementation(Libs.Androidx.testRules)
}