package com.olekdia.sample

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.testing.launchFragment
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.platform.app.InstrumentationRegistry
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialogBuilder
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowLooper

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class MaterialDialog_BasicTest {

    private lateinit var context: Context

    private lateinit var dialog: MaterialDialog

    class MaterialDialogFragment : DialogFragment() {

        private var dlg: MaterialDialog? = null
        var dismissCounter: Int = 0
        var cancelCounter: Int = 0

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
        }

        override fun onPause() {
            super.onPause()

            dismissAllowingStateLoss()
        }

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
            initBuilder().build().also { dlg = it }

        @Suppress("UNCHECKED_CAST")
        private fun initBuilder(): MaterialDialogBuilder =
            requireContext()
                .let { ctx ->
                    MaterialDialogBuilder(ctx)
                        .positiveText("Just Ok")
                        .negativeText("Just Cancel")
                        .setDismissListener{
                            dismissCounter++
                        }
                        .setCancelListener{
                            cancelCounter++
                        }
                }
    }

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
    }


//--------------------------------------------------------------------------------------------------
//  Listeners
//--------------------------------------------------------------------------------------------------

//    @Test
//    fun `fragment dialog dismiss listener - called once`() {
//        launchFragment<MaterialDialogFragment>(
//            themeResId = R.style.SomeTheme
//        ).let { scenario ->
//
//            scenario.moveToState(Lifecycle.State.RESUMED)
//                .onFragment { fragment ->
//                    fragment.dismissAllowingStateLoss()
//                    ShadowLooper.runUiThreadTasks()
//                    assertEquals(1, fragment.dismissCounter)
//                }
//
//            scenario.moveToState(Lifecycle.State.DESTROYED)
//        }
//    }

    @Test
    fun `fragment dialog cancel listener - called once`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)

        var counter = 0
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .setCancelListener() {
                counter++
            }
            .build()

        dialog.cancel()
        assertEquals(1, counter)
    }

    @Test
    fun `fragment dialog dismiss, cancel listeners, dismiss action - called once`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)

        var counter = 0
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .setCancelListener() {
                counter++
                counter++ // Not triggered by design
            }
            .setDismissListener {
                counter++
            }
            .build()

        dialog.dismiss()
        assertEquals(1, counter)
    }

    @Test
    fun `fragment dialog dismiss, cancel listeners, cancel action - called once`() {
        val ctxThemed = ContextThemeWrapper(context, R.style.SomeTheme)

        var counter = 0
        dialog = MaterialDialogBuilder(ctxThemed)
            .positiveText("Just Ok")
            .negativeText("Just Cancel")
            .setCancelListener() {
                counter++
            }
            .setDismissListener {
                counter++
            }
            .build()

        dialog.cancel()
        assertEquals(2, counter)
    }
}