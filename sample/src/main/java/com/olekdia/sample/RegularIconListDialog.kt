package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallback
import com.olekdia.materialdialog.MaterialDialogBuilder

class RegularIconListDialog : BaseSampleDialog(),
    ItemsCallback {

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .items(SampleActivity.CITIES)
            .itemsIcons(SampleActivity.CITIES_ICONS)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.LIST)
            .itemsCallback(this)
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        itemView: View?,
        which: Int,
        text: CharSequence?
    ): Boolean = if (dialog!!.isItemEnabled(which)) {
        showToast(requireContext(), text!!)
        true
    } else {
        showToast(requireContext(), "Not allowed")
        false
    }

    companion object {
        const val TAG = "REGULAR_ICON_LIST_DLG"
    }
}