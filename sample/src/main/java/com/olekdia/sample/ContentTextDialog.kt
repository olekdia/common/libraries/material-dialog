package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.view.WindowManager
import com.olekdia.androidcommon.extensions.fromHtmlCompat
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialogBuilder

class ContentTextDialog : BaseSampleDialog() {

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("What's New")
            .positiveText("Ok")
            .content(fromHtmlCompat(getString(R.string.changelog_dlg_content)))
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
    }

    companion object {
        const val TAG = "CONTENT_DLG"
    }
}