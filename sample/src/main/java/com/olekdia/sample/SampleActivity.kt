package com.olekdia.sample

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import com.olekdia.androidcommon.extensions.getDrawableCompat
import com.olekdia.common.extensions.getRandom
import com.olekdia.common.extensions.getRandomColor

class SampleActivity : AppCompatActivity() {

    private var bottomSheetStyle: RadioButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)

        val citiesDrwRes = intArrayOf(
            R.drawable.city_1, R.drawable.city_2, R.drawable.city_3, R.drawable.city_4
        )
        CITIES_ICONS = Array(CITIES.size) {
            this.getDrawableCompat(citiesDrwRes[getRandom(0, citiesDrwRes.size - 1)])!!.also { drawable ->
                drawable.setTint(getRandomColor())
            }
        }

        val fm = supportFragmentManager
        val styleGroup = findViewById<RadioGroup>(R.id.dialog_style)
        styleGroup.check(R.id.bottom_sheet_style)
        bottomSheetStyle = findViewById(R.id.bottom_sheet_style)

        findViewById<View>(R.id.regular_list)
            .setOnClickListener {
                RegularListDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.regular_icon_list)
            .setOnClickListener {
                RegularIconListDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.regular_icon_grid)
            .setOnClickListener {
                RegularIconGridDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.single_choice_list)
            .setOnClickListener {
                SingleChoiceListDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.single_choice_icon_list)
            .setOnClickListener {
                SingleChoiceIconListDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.single_choice_icon_grid)
            .setOnClickListener {
                SingleChoiceIconGridDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.multi_choice_list)
            .setOnClickListener {
                MultiChoiceListDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.multi_choice_grid)
            .setOnClickListener {
                MultiChoiceGridDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.multi_choice_icon_list)
            .setOnClickListener {
                MultiChoiceIconListDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.multi_choice_icon_grid)
            .setOnClickListener {
                MultiChoiceIconGridDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.input_text)
            .setOnClickListener {
                InputTextDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.indeterminate_circular_progress)
            .setOnClickListener {
                ProgressCircularIndeterminateDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.indeterminate_horizontal_progress)
            .setOnClickListener {
                ProgressHorizontalIndeterminateDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.determinate_progress)
            .setOnClickListener {
                ProgressDeterminateDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.content_text)
            .setOnClickListener {
                ContentTextDialog()
                    .show(fm, isBottomSheetSelected)
            }
        findViewById<View>(R.id.custom_view)
            .setOnClickListener {
                CustomViewDialog()
                    .show(fm, isBottomSheetSelected)
            }
    }

    private val isBottomSheetSelected: Boolean
        get() = bottomSheetStyle!!.isChecked

    companion object {
        val CITIES = arrayOf<CharSequence>(
            "Nizhyn",
            "Kyiv",
            "New-York",
            "London",
            "Beijing",
            "Dubai",
            "Hong Kong",
            "Paris",
            "Shanghai",
            "Singapore",
            "Sydney",
            "Tokyo",
            "Los Angeles",
            "Boston",
            "Chicago",
            "San Francisco",
            "Houston",
            "Sydney",
            "San Jose",
            "Melbourne",
            "Seattle",
            "Atlanta",
            "Washington",
            "Toronto",
            "Dallas",
            "San Diego",
            "Baltimore",
            "Shenzhen",
            "Miami",
            "Austin",
            "Philadelphia",
            "Phoenix",
            "Munich",
            "Brisbane",
            "Moscow",
            "Seoul",
            "Milan",
            "San Jose",
            "Detroit",
            "Madrid",
            "Rome",
            "Qingdao",
            "Austin",
            "Pune",
            "Dayton",
            "Dhaka",
            "Frankfurt"
        )
        var CITIES_ICONS: Array<Drawable>? = null
    }
}