package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialogBuilder
import java.awt.font.NumericShaper

class ProgressDeterminateDialog : BaseSampleDialog(),
    Runnable {

    private var dialog: MaterialDialog? = null
    private val handler = Handler(Looper.getMainLooper())

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .numeralSystem(NumericShaper.EASTERN_ARABIC)
            .progress(MAX_PROGRESS, true)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
            .also {
                dialog = it
                handler.postDelayed(this, 5 * INTERVAL)
            }
    }

    override fun run() {
        dialog!!.let {
            it.incrementProgress(1)
            if (it.currentProgress < MAX_PROGRESS) {
                handler.postDelayed(this, INTERVAL)
            } else {
                it.dismiss()
            }
        }
    }

    companion object {
        const val TAG = "PROGRESS_DLG"

        private const val MAX_PROGRESS = 100
        private const val INTERVAL = 200L
    }
}