package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialogBuilder

class CustomViewDialog : BaseSampleDialog() {

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Custom view dialog")
            .customView(R.layout.custom_dialog, true)
            .positiveText("Ok")
            .keyboardMode(MaterialDialog.KeyboardMode.SHOWN) // default
            .build().also {
                initCustomView(it.customView!!)
            }
    }

    private fun initCustomView(customView: View) {
        customView.findViewById<View>(R.id.button_1).setOnClickListener {
            SingleChoiceIconListDialog()
                .show(parentFragmentManager, true)
        }
    }

    companion object {
        const val TAG = "CUSTOM_DLG"
    }
}