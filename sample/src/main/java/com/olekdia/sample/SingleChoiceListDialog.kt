package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallbackSingleChoice
import com.olekdia.materialdialog.MaterialDialogBuilder
import com.olekdia.materialdialog.isDialogItemEnabled

class SingleChoiceListDialog : BaseSampleDialog(),
    ItemsCallbackSingleChoice {

    private val disabledCities = intArrayOf(4, 5)
    private var dialog: MaterialDialog? = null

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Your city")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("Ok")
            .negativeText("Cancel")
            .content("Chose where you want to live:")
            .items(SampleActivity.CITIES, disabledCities, "[far away]")
            .itemsCallbackSingleChoice(
                savedInstanceState?.getInt(MaterialDialog.SELECTED_INDEX_KEY, 0) ?: 0,
                this
            )
            .alwaysCallSingleChoiceCallback()
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
            .also {
                dialog = it
            }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        dialog?.selectedIndex?.let {
            outState.putInt(MaterialDialog.SELECTED_INDEX_KEY, it)
        }
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        itemView: View?,
        which: Int,
        text: CharSequence?
    ): Boolean = if (dialog!!.isItemEnabled(which)) {
        showToast(requireContext(), text!!)
        true
    } else {
        showToast(requireContext(), "Not allowed")
        false
    }

    companion object {
        const val TAG = "SINGLE_LIST_DLG"
    }
}