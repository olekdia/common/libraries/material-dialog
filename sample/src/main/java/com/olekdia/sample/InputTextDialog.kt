package com.olekdia.sample

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.InputCallback
import com.olekdia.materialdialog.MaterialDialogBuilder

class InputTextDialog : BaseSampleDialog(),
    InputCallback {

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, true)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Input text")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("Ok")
            .negativeText("Cancel")
            .input("Enter some text", "", false, this)
            .inputMaxLength(10, Color.RED)
            .alwaysCallInputCallback()
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
    }

    override fun onInput(dialog: MaterialDialog?, input: CharSequence) {}

    companion object {
        const val TAG = "INPUT_DLG"
    }
}