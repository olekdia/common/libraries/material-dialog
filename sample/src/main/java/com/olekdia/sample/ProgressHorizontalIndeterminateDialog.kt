package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialogBuilder

class ProgressHorizontalIndeterminateDialog : BaseSampleDialog() {

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .progressIndeterminate(MaterialDialog.ProgressStyle.HORIZONTAL)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
    }

    companion object {
        const val TAG = "PROGRESS_HORIZONTAL_INDETERMINATE_DLG"
    }
}