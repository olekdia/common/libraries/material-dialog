package com.olekdia.sample

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.IButtonCallback

abstract class BaseSampleDialog : DialogFragment(),
    IButtonCallback {

    abstract val dialogTag: String

    fun show(fm: FragmentManager, useBottomSheetStyle: Boolean) {
        show(
            fm,
            Bundle().apply {
                putBoolean(BOTTOM_SHEET_STYLE, useBottomSheetStyle)
            }
        )
    }

    @JvmOverloads
    fun show(fm: FragmentManager, args: Bundle? = Bundle()) {
        arguments = args
        show(fm, dialogTag)
    }

    override fun onAny(dialog: MaterialDialog?) {}
    override fun onPositive(dialog: MaterialDialog?) {}
    override fun onNegative(dialog: MaterialDialog?) {}
    override fun onNeutral(dialog: MaterialDialog?) {}

    companion object {
        const val BOTTOM_SHEET_STYLE = "BOTTOM_SHEET_STYLE"
    }
}