package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallback
import com.olekdia.materialdialog.MaterialDialogBuilder

class RegularListDialog : BaseSampleDialog(),
    ItemsCallback {

    private val disabledCities = intArrayOf(4, 5)

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .items(SampleActivity.CITIES, disabledCities, "[far away]")
            .itemsCallback(this)
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        itemView: View?,
        which: Int,
        text: CharSequence?
    ): Boolean = if (dialog!!.isItemEnabled(which)) {
        showToast(requireContext(), text!!)
        true
    } else {
        showToast(requireContext(), "Not allowed")
        false
    }

    companion object {
        const val TAG = "REGULAR_LIST_DLG"
    }
}