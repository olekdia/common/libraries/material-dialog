package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallbackMultiChoice
import com.olekdia.materialdialog.MaterialDialogBuilder

class MultiChoiceGridDialog : BaseSampleDialog(),
    ItemsCallbackMultiChoice {

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Your city")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("Ok")
            .negativeText("Cancel")
            .content("Chose where you want to live:")
            .items(SampleActivity.CITIES)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .itemsCallbackMultiChoice(null, this)
            .alwaysCallMultiChoiceCallback()
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
    }

    override fun onPositive(dialog: MaterialDialog?) {}

    override fun onSelection(
        dialog: MaterialDialog?,
        which: IntArray,
        text: Array<CharSequence>?
    ): Boolean {
        showMultiSelectionToast(requireContext(), which)
        return true
    }

    companion object {
        const val TAG = "MULTI_GRID_DLG"
    }
}