package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallbackSingleChoice
import com.olekdia.materialdialog.MaterialDialogBuilder

class SingleChoiceIconGridDialog : BaseSampleDialog(),
    ItemsCallbackSingleChoice {

    private var dialog: MaterialDialog? = null

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Your city")
            .iconRes(R.mipmap.ic_launcher)
            .content("Chose where you want to live:")
            .items(SampleActivity.CITIES)
            .itemsIcons(SampleActivity.CITIES_ICONS)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .itemsCallbackSingleChoice(
                savedInstanceState?.getInt(MaterialDialog.SELECTED_INDEX_KEY, 0) ?: 0,
                this
            )
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
            .also {
                dialog = it
            }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        dialog?.selectedIndex?.let {
            outState.putInt(MaterialDialog.SELECTED_INDEX_KEY, it)
        }
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        itemView: View?,
        which: Int,
        text: CharSequence?
    ): Boolean {
        showToast(requireContext(), SampleActivity.CITIES[dialog!!.selectedIndex])
        return true
    }

    companion object {
        const val TAG = "SINGLE_GRID_DLG"
    }
}