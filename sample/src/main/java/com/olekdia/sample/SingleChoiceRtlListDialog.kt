package com.olekdia.sample

import android.app.Dialog
import android.content.Context
import android.content.ContextWrapper
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.olekdia.androidcommon.extensions.toLang
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallbackSingleChoice
import com.olekdia.materialdialog.MaterialDialogBuilder

// Not used
class SingleChoiceRtlListDialog : BaseSampleDialog(),
    ItemsCallbackSingleChoice {

    private val disabledCities = intArrayOf(4, 5)
    private var dialog: MaterialDialog? = null

    override val dialogTag: String
        get() = TAG

    override fun onAttach(context: Context) {
        super.onAttach(ContextWrapper(context).toLang("ar"))
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("مدينتك")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("حسنا")
            .negativeText("إلغاء")
            .items(CITIES, disabledCities, "[بعيدا]")
            .itemsCallbackSingleChoice(
                savedInstanceState?.getInt(MaterialDialog.SELECTED_INDEX_KEY, 0) ?: 0,
                this
            )
            .alwaysCallSingleChoiceCallback()
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
            .also {
                dialog = it
            }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        dialog?.selectedIndex?.let {
            outState.putInt(MaterialDialog.SELECTED_INDEX_KEY, it)
        }
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        itemView: View?,
        which: Int,
        text: CharSequence?
    ): Boolean = if (dialog!!.isItemEnabled(which)) {
        showToast(requireContext(), text!!)
        true
    } else {
        showToast(requireContext(), "غير مسموح")
        false
    }

    companion object {
        const val TAG = "SINGLE_RTL_LIST_DLG"
        val CITIES = arrayOf<CharSequence>(
            "نيزين",
            "كييف",
            "نيويورك",
            "لندن",
            "بكين",
            "دبي",
            "هونج كونج",
            "باريس",
            "شنغهاي",
            "سنغافورة",
            "سيدني",
            "طوكيو",
            "لوس أنجلوس",
            "بوسطن",
            "شيكاغو",
            "سان فرانسيسكو",
            "هيوستن",
            "سيدني",

            "نيزين",
            "كييف",
            "نيويورك",
            "لندن",
            "بكين",
            "دبي",
            "هونج كونج",
            "باريس",
            "شنغهاي",
            "سنغافورة",
            "سيدني",
            "طوكيو",
            "لوس أنجلوس",
            "بوسطن",
            "شيكاغو",
            "سان فرانسيسكو",
            "هيوستن",
            "سيدني",

            "ميلان",
            "سان خوسيه",
            "ديترويت",
            "مدريد",
            "روما",
            "تشينغداو",
            "أوستين",
            "بيون",
            "دايتون",
            "دكا",
            "فرانكفورت"
        )
    }
}