package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallbackMultiChoice
import com.olekdia.materialdialog.MaterialDialogBuilder

class MultiChoiceIconListDialog : BaseSampleDialog(),
    ItemsCallbackMultiChoice {

    private var dialog: MaterialDialog? = null

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Your city")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("Ok")
            .negativeText("Cancel")
            .content("Chose where you want to live:")
            .items(SampleActivity.CITIES)
            .itemsIcons(SampleActivity.CITIES_ICONS)
            .itemsCallbackMultiChoice(
                if (savedInstanceState == null) {
                    IntArray(0)
                } else {
                    savedInstanceState.getIntArray(MaterialDialog.SELECTED_INDEX_LIST_KEY)
                },
                this
            )
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.LIST)
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
            .also {
                dialog = it
            }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        dialog?.selectedIndices?.let {
            outState.putIntArray(MaterialDialog.SELECTED_INDEX_LIST_KEY, it)
        }
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        which: IntArray,
        text: Array<CharSequence>?
    ): Boolean {
        showMultiSelectionToast(requireContext(), which)
        return true
    }

    companion object {
        const val TAG = "MULTI_ICON_LIST_DLG"
    }
}