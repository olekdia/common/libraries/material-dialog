package com.olekdia.sample

import android.content.Context
import android.widget.Toast

private var toast: Toast? = null

private fun cancelToast() {
    toast?.cancel()
    toast = null
}

fun showToast(context: Context, text: CharSequence) {
    cancelToast()
    toast = Toast.makeText(context, text, Toast.LENGTH_SHORT).also {
        it.show()
    }
}

fun showMultiSelectionToast(context: Context, indices: IntArray?) {
    if (indices != null) {
        val sb = StringBuilder()
        val size = indices.size
        for (i in 0 until size) {
            sb.append(SampleActivity.CITIES[indices[i]])
                .append(", ")
        }
        showToast(context, sb)
    }
}