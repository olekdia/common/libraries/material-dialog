package com.olekdia.sample

import android.app.Dialog
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallbackMultiChoice
import com.olekdia.materialdialog.MaterialDialogBuilder
import com.olekdia.materialdialog.isDialogItemEnabled

class MultiChoiceListDialog : BaseSampleDialog(),
    ItemsCallbackMultiChoice {

    private val disabledCities = intArrayOf(
        4, 5
    )
    private var dialog: MaterialDialog? = null

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Your city")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("Ok")
            .negativeText("Cancel")
            .content("Chose where you want to live:")
            .items(SampleActivity.CITIES, disabledCities, getDisabledTail())
            .itemsCallbackMultiChoice(
                if (savedInstanceState == null) {
                    IntArray(0)
                } else {
                    savedInstanceState.getIntArray(MaterialDialog.SELECTED_INDEX_LIST_KEY)
                },
                this
            )
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.LIST)
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
            .also {
                dialog = it
            }
    }



    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        dialog?.selectedIndices?.let {
            outState.putIntArray(MaterialDialog.SELECTED_INDEX_LIST_KEY, it)
        }
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        which: IntArray,
        text: Array<CharSequence>?
    ): Boolean {
        for (i in which.indices.reversed()) {
            if (!disabledCities.isDialogItemEnabled(which[i])) {
                showToast(requireContext(), "Not allowed")
                return false
            }
        }
        showMultiSelectionToast(requireContext(), which)
        return true
    }

    companion object {
        const val TAG = "MULTI_LIST_DLG"

        fun getDisabledTail(): CharSequence =
            SpannableString("far away").also {
                it.setSpan(
                    RelativeSizeSpan(0.7f),
                    0,
                    it.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                it.setSpan(
                    StyleSpan(Typeface.ITALIC),
                    0,
                    it.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
    }
}