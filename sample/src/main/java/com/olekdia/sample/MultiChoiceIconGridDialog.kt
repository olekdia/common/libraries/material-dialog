package com.olekdia.sample

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import com.olekdia.materialdialog.MaterialDialog
import com.olekdia.materialdialog.MaterialDialog.ItemsCallbackMultiChoice
import com.olekdia.materialdialog.MaterialDialogBuilder
import com.olekdia.materialdialog.isDialogItemEnabled

class MultiChoiceIconGridDialog : BaseSampleDialog(),
    ItemsCallbackMultiChoice {

    private var dialog: MaterialDialog? = null

    private val disabledCities = intArrayOf(
        4, 5, 6, 7
    )

    override val dialogTag: String
        get() = TAG

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val useBottomSheetStyle = requireArguments()
            .getBoolean(BOTTOM_SHEET_STYLE, false)

        return MaterialDialogBuilder(requireContext())
            .bottomSheet(useBottomSheetStyle, useBottomSheetStyle)
            .title("Your city")
            .iconRes(R.mipmap.ic_launcher)
            .positiveText("Ok")
            .negativeText("Cancel")
            .content("Chose where you want to live:")
            .items(SampleActivity.CITIES, disabledCities, "[far away]")
            .itemsIcons(SampleActivity.CITIES_ICONS)
            .itemsLayoutType(MaterialDialog.ItemsLayoutType.GRID)
            .itemsCallbackMultiChoice(
                if (savedInstanceState == null) {
                    IntArray(0)
                } else {
                    savedInstanceState.getIntArray(MaterialDialog.SELECTED_INDEX_LIST_KEY)
                },
                this
            )
            .alwaysCallMultiChoiceCallback()
            .callback(this)
            .keyboardMode(MaterialDialog.KeyboardMode.HIDDEN)
            .build()
            .also {
                dialog = it
            }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        dialog?.selectedIndices?.let {
            outState.putIntArray(MaterialDialog.SELECTED_INDEX_LIST_KEY, it)
        }
    }

    override fun onPositive(dialog: MaterialDialog?) {
        showMultiSelectionToast(requireContext(), dialog!!.selectedIndices)
    }

    override fun onSelection(
        dialog: MaterialDialog?,
        which: IntArray,
        text: Array<CharSequence>?
    ): Boolean {
        for (i in which.indices.reversed()) {
            if (!disabledCities.isDialogItemEnabled(which[i])) {
                showToast(requireContext(), "Not allowed")
                return false
            }
        }
        showMultiSelectionToast(requireContext(), which)
        return true
    }

    companion object {
        const val TAG = "MULTI_GRID_DLG"
    }
}