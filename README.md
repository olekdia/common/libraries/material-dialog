# Material Dialog

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

---

# Modules

The core module is the fundamental module that you need in order to use this library. The others 
are extensions to core.

## Core

[ ![Core](https://api.bintray.com/packages/olekdia/olekdia/material-dialog%3Acore/images/download.svg?version=3.7.3) ](https://bintray.com/olekdia/olekdia/material-dialog%3Acore/3.7.3/link)


The `core` module contains everything you need to get started with the library. It contains all
core and normal-use functionality.


```gradle
dependencies {
  ...
  implementation 'com.olekdia.material-dialog:core:3.7.3'
}
```

## Flower color

[ ![Color](https://api.bintray.com/packages/olekdia/olekdia/material-dialog:flower-color/images/download.svg?version=3.0.7) ](https://bintray.com/olekdia/olekdia/material-dialog:flower-color/3.7.0/link)

```gradle
dependencies {
  ...
  implementation 'com.olekdia.material-dialog:flower-color:3.7.3'
}
```