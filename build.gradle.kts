buildscript {
    repositories {
        google()
        mavenCentral()
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath(Libs.Plugin.kotlinGradle)
        classpath(Libs.Plugin.androidGradle)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }
}